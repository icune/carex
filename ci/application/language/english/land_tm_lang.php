<?php
$lang['russkii'] = "Русский";
$lang['imja_i_familija'] = "Your name";
$lang['telefon'] = "Phone number";
$lang['registracija_'] = "National trademark registration";
$lang['kak_vas_zovut'] = "Как вас зовут?";
$lang['pakety_i_tarify_'] = "PRICES";
$lang['registracija_tovarnogo_znaka_v_nacionalnom_vedomstve_kazhdogo_vybrannogo_gosudarstva'] = "National trademark registration";
$lang['goda'] = "года";
$lang['konsultacija_'] = "Консультация ";
$lang['vash_telefon'] = "Ваш телефон";
$lang['priglashaem_k_nam'] = "Приглашаем к нам";
$lang['vy_urist'] = "Вы юрист?";
$lang['service_page_main_tm_title'] = "TRADEMARK <br> REGISTRATION";
$lang['service_page_main_tm_text'] = "Engaged in export busines? or preparing to enter new market? want to protect your brand? We will help you to protect your interests.";
$lang['service_page_main_btn'] = "Send request";
$lang['service_page_advantages_title'] = "ADVANTAGES OF OUR SERVICES";
$lang['service_page_advantages_sub-title'] = "";
$lang['service_page_advantages_adv1-title'] = "CONFIDENTIALITY";
$lang['service_page_advantages_adv1-text'] = "All your documents are stored in the Legal Space office.";
$lang['service_page_advantages_adv2-title'] = "PAYMENT UPON CONFIRMATION";
$lang['service_page_advantages_adv2-text'] = "A lawyer receives payment only after the work is done";
$lang['service_page_advantages_adv3-title'] = "INDIVIDUAL APPROACH";
$lang['service_page_advantages_adv3-text'] = "You will get an efficient service and a personal manager.";
$lang['service_page_advantages_adv4-title'] = "ON-LINE SUPPORT";
$lang['service_page_advantages_adv4-text'] = "Conduct all necessary operations and organize document workflow directly on our website";
$lang['service_page_advantages_adv5-title'] = "BEST PRICES";
$lang['service_page_advantages_adv5-text'] = "Working on-line allows us to avoid unnecessary costs of conventional law firms.";
$lang['service_page_advantages_adv6-title'] = "WE WORK ON WEEKENDS";
$lang['service_page_advantages_adv6-text'] = "Prefer to earn money, when everybody is resting? We will be next to you.";
$lang['service_page_register_title'] = "Want to register a trademark?";
$lang['service_page_register_sub_title'] = "Get first consultation free of charge!";
$lang['service_page_register_btn'] = "Send request";
$lang['service_page_tarifs_title'] = "Prices";
$lang['service_page_tarifs_sub-title'] = "";
$lang['service_page_tarifs_cost'] = "Price";
$lang['service_page_tarifs_indcleded'] = "Included";
$lang['service_page_tarifs_patent'] = "Full Search and Opinion";
$lang['service_page_tarifs_reg-tax'] = "Government fees";
$lang['service_page_tarifs_time'] = "Срок";
$lang['service_page_tarifs_month'] = "monthes";
$lang['service_page_tarifs_btn'] = "Register";
$lang['service_page_tarifs_p2_title'] = "European Community Trademark Registration";
$lang['service_page_tarifs_year'] = "years";
$lang['service_page_tarifs_p3_title'] = "International Trademark Registration";
$lang['service_page_tarifs_bottom-text'] = "*Prices are indicated for 3 classes.";
$lang['service_page_faq_title'] = "F. A. Q.";
$lang['service_page_faq_sub_title'] = "";
$lang['service_page_faq_q1_title'] = "WHAT DO I NEED TO DO TO REGISTER A TRADEMARK?";
$lang['service_page_q1_text'] = "You need to choose what will be the image of the trademark: color or black and white, and then decide in what classes of goods / services the trademark will be registered - our lawyers will help you to make the right decision.";
$lang['service_page_faq_q1_text'] = "
Protection is granted for 10 years and can be renewed endlessly                        ";
$lang['service_page_faq_q2_title'] = "WHAT DOCUMENTS WILL I GET UPON REGISTRATION?";
$lang['service_page_faq_q2_text'] = "
                            You will receive an electronic certificate confirming the registration of the trademark.
                        ";
$lang['service_page_faq_q3_title'] = " HOW CAN I PAY?";
$lang['service_page_faq_q3_text'] = "
	You pay for the trademark registration through the Legal Space website. After work is done, we transfer payment to the Legal Space associated partner. In case the trademark is not registered (although there has been no such cases), we will refund you the full amount.                            
                        ";
$lang['service_page_faq_q4_title'] = "How long protection will be effective?";
$lang['service_page_faq_q4_text'] = "
                            Protection is granted for 10 years and can be renewed endlessly
                        ";
$lang['service_page_faq_q5_title'] = "When trademark protection is granted?";
$lang['service_page_faq_q5_text'] = "
                            Protection is immediate and is granted upon the filing of application.
                        ";
$lang['main_page_offices_h2'] = "OUR OFFICES";
$lang['main_page_offices_subtitle'] = "";
$lang['main_page_map_adr1_title'] = "Legalspace — MILAN";
$lang['main_page_adr1_adress'] = "Via Friuli, 51 - 20135";
$lang['main_page_adr1_phone'] = "+39 029-475-96-84";
$lang['main_page_map_adr2_title'] = "Legalspace — Москва";
$lang['main_page_adr2_adress'] = "Пресненский вал, 15";
$lang['main_page_adr2_phone'] = "8 (800) 333-67-36";
$lang['main_page_map_adr3_title'] = "Legalspace — Никоссия";
$lang['main_page_adr3_adress'] = "Псарон, 2408";
$lang['main_page_adr3_phone'] = "+357 230-300-16";
$lang['service_page_anyquestion_title'] = "ANY QUESTIONS?";
$lang['service_page_anyquestion_sub_title'] = "Leave your phone number and we will call you soon";
$lang['service_page_anyquestion_btn'] = "CALL ME";
$lang['footer_info'] = " <b>Legal Space</b> — is an international community of more than 2 000 first-class lawyers
                                                       <br>Our service allows you to get high quality legal services on the transparent basis.";
$lang['footer_copyright'] = "© 2015 Legal Space Ltd., All rights reserved.";

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<table cellpadding="5" cellspacing="0" border="1">
		<tr>
			<th>Имя</th>
			<th>Email</th>
			<th>Телефон</th>
			<th>Linkedin</th>
			<th>Страна</th>
			<th>Скайп</th>
			<th>Специализация</th>
			<th>Фирма?</th>
			<th>Юрист?</th>
		</tr>
	<?php foreach($requests as $r): ?>
		<tr>
			<td>
				<?php echo $r['name']; ?>
			</td>
			<td>
				<?php echo $r['email']; ?>
			</td>
			<td>
				<?php echo $r['phone']; ?>
			</td>
			<td>
				<?php echo $r['linkedin']; ?>
			</td>
			<td>
				<?php echo $r['country']; ?>
			</td>
			<td>
				<?php echo $r['skype']; ?>
			</td>
			<td>
				<?php echo $r['area']; ?>
			</td>
			<td>
				<?if($r['corporate'] == 1){echo "<span style='font-weight:bold; color:green'>ДА</span>";}else{echo "<span style='font-weight:bold; color: red'>НЕТ</span>";}?>
			</td>
			<td>
				<?if($r['is_lawyer'] == 1){echo "<span style='font-weight:bold; color:green'>ДА</span>";}else{echo "<span style='font-weight:bold; color: red'>НЕТ</span>";}?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</body>
</html>
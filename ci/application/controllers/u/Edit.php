<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends CI_Controller {
	public function set(){
		$this->load->database();
		if(!User::$id)
			return;
		$updates = $_POST;
		foreach($updates as $table => $upd){
			if($table != 'user' && $table != 'user_info' && $table != 'user_info_lawyer')
				continue;
			if($table == 'user')
				$where = array('id' => User::$id);
			else
				$where = array('user_id' => User::$id);
			$this->db->update($table, $upd, $where);
		}

		if(isset($_POST['redirect']))
			header('Location: '.$_POST['redirect']);
		else
			header('Location: /in/hello');		
	}
	public function rescale(){
		$userSrc = $this->postAbsent('userSrc');		
		$p = $this->postAbsent('p');		
		$fn = Info::$rootDir . $userSrc;
		$fn_p = explode('.', $fn);
		if(count($fn_p) < 2)
			$this->err('???');
		$ext = strtolower(trim($fn_p[count($fn_p)-1]));
		if($ext == 'jpeg' || $ext == 'jpg')
			$im = imagecreatefromjpeg($fn);
		else if($ext == 'png')
			$im = imagecreatefrompng($fn);
		else if($ext == 'gif')
			$im = imagecreatefromgif($fn);
		else
			$this->err('unknown format');

		$w = imagesx($im);
		$h = imagesy($im);

		$scale = floatval($p['scale']);
		$x = floatval($p['x'])/$scale;
		$y = floatval($p['y'])/$scale;
		$w_n = 150;
		$h_n = 150;
		$im_new = imagecreatetruecolor($w_n, $h_n);
		imagecopyresampled($im_new, $im, 0, 0, $x, $y, $w_n, $h_n, $w_n/$scale, $h_n/$scale);
		$fn_p[count($fn_p)-2] = $fn_p[count($fn_p)-2] . '-c';
		$fn_new = implode('.', $fn_p);
		if($ext == 'jpeg' || $ext == 'jpg')
			$im = imagejpeg($im_new, $fn_new, 100);
		else if($ext == 'png')
			$im = imagepng($im_new, $fn_new, 0);
		else if($ext == 'gif')
			$im = imagegif($im_new, $fn_new);
		$this->ok();
	}
	public function password(){
		if(!isset($_POST['password']))
			$this->err();
		User::changePassword(User::$id, $_POST['password']);
		$this->ok();
	}
}


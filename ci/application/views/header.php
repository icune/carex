<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" />


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>CAREX</title>

    <!-- google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900|Open+Sans:400,300,600,700,800|PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="http://yastatic.net/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="/assets/css/global.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/tooltip-classic.css">
    <link href="/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="/assets/css/auto-dealer.css" rel="stylesheet">
    <link href="/assets/css/responsive.css" rel="stylesheet">
    <link href="/assets/css/skin.less" rel="stylesheet/less">
    <link href="/assets/css/transition-effect.css" rel="stylesheet">
    <script src="http://yastatic.net/jquery/2.1.4/jquery.min.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="homepage-17">
    <div class="loader-block">
        <div class="loader">Загрузка...</div>
    </div>
    <!--Wrapper Section Start Here -->
   <header id="header">

            <!--headerBox Start Here -->

            <div class="header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 top">
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="logo">
                                            
                                            <?php 

                                              if(Info::$script) echo '<a href="/"><img class="logotip" src="assets/img/logo_color_white.png" alt=""></a>';

                                             ?>

                                             
                                            
                                        <!--   <label class="top-globe hidden-xs" for="city-change"><i class="fa fa-globe"></i> </label><input type="text" value="" id="city-change" placeholder="Москва"> -->
                                            <div class="city-select hidden-sm hidden-xs">
                                                <!-- <a  href="carexp.html" class="top-expert"> <i class="fa fa-thumbs-o-up "></i> Наши эксперты</a> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6 top-cont">


                                        <ul>
                                        <li >
                                            <a href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-sign-in"></i> Войти</a>

                                        </li>
                                        <li >
                                            
                                            <a href="/register"><i class="fa fa-pencil-square-o"></i> Регистрация</a>
                                        </li>
                                        <li>
                                            <a class="top-phone" href="#">+7 499 397 01 61
</a>
                                        </li>
</ul>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







            <!--headerCntr End Here -->
        </header>
    <!--header Section End Here -->
<!-- Modal -->
<?php if(isset($_GET['already'])): ?>
    <script type="text/javascript">
        var email = '<?php echo $_GET["already"]; ?>';
        var tasked = '<?php echo (isset($_GET["task_added"]) ? 'Ваш запрос добавлен в личный кабинет' : '' ); ?>';
        $(function(){

            alert('Данный E-mail уже зарегистрирован. ' + tasked);

            $('#myModal2').modal();
            $('#myModal2 .email').val(email);
        })
    </script>
<?php endif; ?>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Вход в личный кабинет</h4>
      </div>
      <div class="modal-body">
        <div class="exp-login login">
            <input type="text" class="email" placeholder="E-mail">
            <input type="password" class="password" placeholder="Пароль">
            <button class="btn btn-default login">Войти</button>
            <a href="/register" class="btn btn-default register" >Зарегистрироваться</a>
            <!-- <a class="forget-pass" href="#">Забыли пароль?</a> -->
        </div>
        <p>
          <a class="restore-link" href="/restore">
            Востановить пароль
          </a>
        </p>
      </div>
     
        
        
      </div>
    </div>
  </div>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Expert-list</title>
	<style>
		.el{
			padding: 10px;
			border-bottom:1px solid #a0a0a0;
		}
		.el a{
			float : right;
		}
	</style>
</head>
<body>
	<h1>
		Эксперты
	</h1>
	<?php foreach($experts as $e): ?>
		<div class="el">
			<?php echo $e['name'] ?>&nbsp;
			<?php echo $e['surname'] ?>&nbsp;
			<?php echo $e['patronym'] ?>
			<a href="/imperium/expert/edit?user_id=<?php echo $e['id']; ?>">
				Перейти в профиль
			</a>
		</div>
		
	<?php endforeach; ?>
	<div class="el">
			&nbsp;
			<a href="/imperium/expert/add">
				Добавить нового
			</a>
		</div>
</body>
</html>
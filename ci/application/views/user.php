<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>User Edit</title>
	<script type="text/javascript" src="/js/jquery-2.1.4.js"></script>

	<script type="text/javascript" src="/js/net.js"></script>
	<script type="text/javascript" src="/js/user.js"></script>
	<link rel="stylesheet" href="/css/user.css">
	
	<script type="text/javascript">
		var PER_PAGE = <?php echo $PER_PAGE ?>;
		var user_dirs = <?php echo json_encode($dirs, true); ?>;
		for(var k  in user_dirs){
			var shn = shname(user_dirs[k].name, 3);
			for(var kk in user_dirs)
				if(user_dirs[kk].shname == shn){
					shn = shname(user_dirs[k].name, 4);
					break;
				}
			user_dirs[k].shname = shn;
		}
		function shname(name, count){
			return name.substr(0, count).toUpperCase();;
		}
	</script>
</head>
<body>
<table style="display:none">
	<tr class="user-row template">
		<td class="name" field="name">
			<input type="text" class="inp" />
		</td>
		<td class="surname" field="surname">
			<input type="text" class="inp" />
		</td>
		<td class="patronym" field="patronym">
			<input type="text" class="inp" />
		</td>
		<td class="email" field="email">
			<input type="text" class="inp" />
		</td>
		<td class="dir" field="dir">
			
		</td>
		<td class="create_ts">
			
		</td>
		<td class="actions">
			<span class="delete">Удалить</span>
		</td>
	</tr>
</table>
	
	<div class="header">
		<div class="center">
			Направление:
			<?php foreach($dirs as $d): ?>
				<input type="checkbox" class="filter" dir_id="<?php echo $d['id']; ?>" />
				<?php echo $d['name']; ?>
				&nbsp;&nbsp;
			<?php endforeach; ?>
		</div>
	</div>
	<div class="content">
		<div class="sorter">
			Сортировать по:
			<select id="sort">
				<option value="name">Имя</option>
				<option value="surname">Фамилия</option>
				<option value="patronym">Отчество</option>
				<option value="email">Email</option>
				<option value="create_ts">Создан</option>
			</select>
			<span class="direction asc active">По возрастанию</span>
			<span class="direction descc">По убыванию</span>
		</div>
		<table class="user-table" id="update-table" style="display:none">
			<tr class="head-row">
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Отчество</th>
				<th>Email</th>
				<th>Направление</th>
				<th>Создан</th>
				<th>Действия</th>
			</tr>
		</table>
		<div class="no-records" style="display:none">
			Нет записей
		</div>
		<div class="pager">
			
		</div>
		<button class="button" id="save" style="display:none">
		Сохранить изменения
		</button>
		<br>

		<table class="user-table" id="create-table" style="display:none">
			<tr class="head-row">
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Отчество</th>
				<th>Email</th>
				<th>Направление</th>
				<th>Создан</th>
				<th>Действия</th>
			</tr>
		</table>
		<button class="button" id="create">
			Создать
		</button>	
		<button class="button" id="create-save" style="display:none">
			Сохранить созданных
		</button>
	</div>
	<div class="cover"></div>
</body>
</html>
<?php
$lang['we_invite'] = "Приглашаем к нам";
$lang['are_you'] = "Вы юрист?";
$lang['title'] = "Legal space";
$lang['best_juri_online'] = "Best Lawyers Worldwide. Online";
$lang['bla_bla_before_form'] = "<p>Вам нужен юрист?</p>
					<p>Напишите, чем мы можем Вам помочь.</p>";
$lang['get_free_consult'] = "Получить бесплатную консультацию";
$lang['where_you'] = "Выберите страну";
$lang['desc_problem'] = "Опишите Вашу задачу";
$lang['send'] = "Send";
$lang['footer_blabla'] = "© Copyright 2015. Legal Space";
$lang['rights'] = "Все права защищены";
$lang['moscow'] = "Москва";
$lang['moscow_addr'] = "Пресненский вал, 15";
$lang['milan'] = "Милан";
$lang['milan_addr'] = "Виа Фриули, 51 - 20135";
$lang['nikosia'] = "Никосия";
$lang['nikosia_addr'] = "Псарон , 2408 ";
$lang['from_where_you'] = "Откуда вы?";
$lang['name'] = "Ваше имя";
$lang['phone'] = "Your phone";
$lang['success'] = "Спасибо за Ваш вопрос! Мы скоро с вами свяжемся";
$lang['error'] = "Произошла ошибка. Свяжитесь с нами и мы поможем: info@legalspace.world";
$lang['bla_1'] = "<p>Сервис LEGAL SPACE — это безопасная и надежная площадка, которая поможет вам решить юридическую задачу в любой точке мира on-line. Учреждаете компанию в Лондоне или регистрируете товарный знак в Италии? Получаете вид на жительство в США? Любые юридические услуги в любой точке мира прямо сейчас на нашем сайте. </p><p>Каждый юрист LEGAL SPACE проходит жесткий отбор, поэтому мы уверенны в его квалификации и профессионализме и ручаемся за качество оказанных им услуг.</p>";
$lang['bla_2'] = "<p>Все наши юристы обладают опытом работы не менее 5 лет. Каждый кандидат проходит личное собеседование перед началом работы. База юристов LEGAL SPACE пополняется только через рекомендации и личные отзывы. </p><p>Наши партнеры это лучшие представители юридической отрасли они прогрессивны и всегда находятся в центре новаций. Вместе мы меняем мир юридических услуг в лучшую сторону .</p>";
$lang['we_connect'] = "МЫ СВЯЗЫВАЕМ ВАС С ЛУЧШИМИ ЮРИСТАМИ НАПРЯМУЮ";
$lang['spoiled_phone'] = "Наша обширная географическая база позволяет решить Вашу задачу в любой стране.";
$lang['with_support'] = "При поддержке";
$lang['globe_under'] = "стран";
$lang['hat_under'] = "юристов";
$lang['case_under'] = "клиентов";
$lang['office_title'] = "Наши офисы:";
$lang['office_desc'] = "Позвоните и мы поможем:";
$lang['moscow_desc'] = "Московский офис расположен в самом центре Москвы, в 5 минутах от метро Белорусская. Паркинг для клиентов. 
";
$lang['milan_desc'] = "Миланский офис расположен в самом центре Милана, в 10 минутах от метро Lodi.
";
$lang['nikosia_desc'] = "Кипрский офис удобно расположен вблизи аэропорта.
";
$lang['find_descision'] = "Решите юридический вопрос сейчас";
$lang['find_descision_after'] = "Нужна помощь? Мы Вам позвоним";
$lang['bot_button'] = "Позвоните мне";
$lang['enter_lk'] = "Вход в личный кабинет";
$lang['password'] = "Пароль";
$lang['_main_page_h1_title'] = "Лучшие юристы мира. ONLINE    ";
$lang['_main_page_h1_subtitle'] = "<p>ВАМ НУЖЕН ЮРИСТ?</p><p>НАПИШИТЕ, ЧЕМ МЫ МОЖЕМ ВАМ ПОМОЧЬ</p>";
$lang['_main_page_btn_consult'] = "Получить бесплатную консультацию";
$lang['_main_page_trigers_title_1'] = "Найти юриста легко";
$lang['_main_page_trigers_text_1'] = "С нами работает более 2 000 юристов в более чем 150 странах мира";
$lang['_main_page_trigers_title_2'] = "Лучшие юристы";
$lang['_main_page_trigers_text_2'] = "Минимум 6 лет опыта, наличие рекомендаций и личное собеседование";
$lang['_main_page_trigers_title_3'] = "Безопасная сделка";
$lang['_main_page_trigers_text_3'] = "Юрист получит деньги только после подтверждения выполнения работы";
$lang['_main_page_best_lawyers_h2'] = "МЫ СВЯЗЫВАЕМ ВАС <br> С ЛУЧШИМИ ЮРИСТАМИ НАПРЯМУЮ";
$lang['_main_page_best_lawyers_sub-title'] = "";
$lang['_main_page_best_lawyers_info_text'] = "
                                <p>Сервис Legal Space — это безопасная и надежная площадка, которая поможет Вам решить юридическую задачу в любой точке мира on-line. </p>

<p>Вы можете получить любые юридические услуги в любой точке мира прямо сейчас на нашем сайте. Оставьте заявку и мы предоставим Вам специалиста с высочайшей квалификацией именно по Вашему вопросу. </p>

<p>Каждый юрист Legal Space проходит тщательный отбор, поэтому мы полностью уверены в его профессионализме и ручаемся за качество оказанных им услуг. </p>
                                ";
$lang['_main_page_brands_support'] = "При поддержке";
$lang['_main_page_pop_service_h2'] = "Популярные услуги";
$lang['_main_page_pop_service_subtitle'] = "Вы можете позвонить в любой из наших офисов в рабочее время и мы
                                        <br> проконсультируем вас на русском или английском языке";
$lang['_main_page_pop_service_title_1'] = "Создать
                                                                <br> компанию
                                                                <br> в России";
$lang['_main_page_pop_service_text_1'] = "Открыть свою компанию проще, чем вам кажется. Мы поможем сориентироваться в процессе, подготовить документы и начать бизнес";
$lang['_main_page_pop_service_btn'] = "Бесплатная консультация";
$lang['_main_page_pop_service_btn_more'] = "Узнать подробнее";
$lang['_main_page_pop_service_title_2'] = "Создать
                                                                <br> компанию
                                                                <br> на Кипре";
$lang['_main_page_pop_service_text_2'] = "Компания на Кипре — одно из лучших решений при необходимости выхода на международный рынок";
$lang['_main_page_pop_service_title_3'] = "Зарегистрировать
                                                                <br> товарный
                                                                <br> знак";
$lang['_main_page_pop_service_text_3'] = "На международном рынке необходимо защищать свой бренд. Мы поможем вам сделать это без лишней бюрократии";
$lang['_main_page_solution_h2'] = "мы Поможем вам <br> решить любые задачи";
$lang['_main_page_solution_subtitle'] = "";
$lang['_main_page_solution_title_startup'] = "Стартапам";
$lang['_main_page_solution_title_lawyer'] = "Юристам";
$lang['_main_page_solution_title_business'] = "Бизнесу";
$lang['_main_page_solution_text_business'] = "
                            
                                Я понимал, что мне нужен юрист для<br> сопровождения инвестиционного раунда.<br> Legal Space помог мне <br>найти одного из лучших инвестиционных юристов, <br>который решил мою задачу быстро<br> и отстоял важнейшие для меня условия

                        ";
$lang['_main_page_solution_btn_business'] = "Сократить издержки бизнеса";
$lang['_main_page_solution_business_clinet'] = "<b>Дмитрий Зарюта</b>
                                                         CEO EASY TEN";
$lang['_main_page_solution_text_lawyer'] = "
                            С Legal Space мы можем за счет <br /> внешних юристов в любой момент <br> масштабировать свой юридический <br>департамент до размера, <br>который нужен для сопровождения <br>крупной сделки или судебного<br> разбирательства
                        ";
$lang['_main_page_solution_btn_lawyer'] = "Найти юриста";
$lang['_main_page_solution_text_startup'] = "
                            У нас растущий бизнес и нет <br>времени на поиск высококлассного юриста<br> в рамках имеющегося бюджета. Нам нужен <br>юрист, который хорошо понимает <br>наш бизнес и финансовые возможности. <br>Legal Space помогли нам создать структуру<br> нашего бизнеса, без которой мы бы <br>просто не смогли существовать. <br>Спасибо большое.
                        ";
$lang['_main_page_solution_btn_startup'] = "Начать свое дело";
$lang['_main_page_solution_startup_clinet'] = "Команда
                                                        <b>Bash.today</b>";
$lang['_main_page_activity_h2'] = "Направления деятельности";
$lang['_main_page_activity_list_1'] = "Товарные знаки и интеллектуальная собственность";
$lang['_main_page_activity_list_2'] = "Недвижимость";
$lang['_main_page_activity_list_3'] = "Корпоративное право и M&amp;A";
$lang['_main_page_activity_list_4'] = "Частный капитал";
$lang['_main_page_activity_list_5'] = "Международное налоговое и корпоративное структурирование";
$lang['_main_page_activity_list_6'] = "Налоги";
$lang['_main_page_activity_list_7'] = "Инвестиции";
$lang['_main_page_activity_list_8'] = "Учреждение компаний";
$lang['_main_page_activity_list_9'] = "Инвестиционные фонды";
$lang['_main_page_activity_list_10'] = "Реструктуризации и банкротство";
$lang['_main_page_activity_list_11'] = "Трудовое право";
$lang['_main_page_activity_list_12'] = "Антимонопольное законодательство";
$lang['_main_page_offices_h2'] = "Наши офисы";
$lang['_main_page_offices_subtitle'] = "Позвоните нам и мы поможем найти решение, подходящее именно Вам.";
$lang['_main_page_map_adr1_title'] = "Legalspace — Милан";
$lang['_main_page_adr1_adress'] = "Виа Фриули, 51 - 20135";
$lang['_main_page_adr1_phone'] = "+39 029-475-96-84";
$lang['_main_page_map_adr2_title'] = "Legalspace — Москва";
$lang['_main_page_adr2_adress'] = "Пресненский вал, 15";
$lang['_main_page_adr2_phone'] = "8 (800) 333-67-36";
$lang['_main_page_map_adr3_title'] = "Legalspace — Никосия";
$lang['_main_page_adr3_adress'] = "Псарон, 2408";
$lang['_main_page_adr3_phone'] = "+357 230-300-16";
$lang['_main_page_footer_h1'] = "Начните решение юридических <br> вопросов уже сейчас";
$lang['_main_page_footer_btn'] = "ПОЗВОНИТЕ МНЕ";
$lang['footer_text'] = "       <b>Legal Space</b> — это безопасная и надежная виртуальная площадка, <br>
                             которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.
                            <p>
                                © 2015 Legal Space Ltd., Все права защищены </p>";
$lang['maria'] = "Мария Нечепа";
$lang['maria_record'] = "Директор по инвестиционной деятельности банка";
$lang['sk_title'] = "Для друзей Сколково";
$lang['sk_text'] = "Мы дарим Вам 9000 рублей на юридические услуги нашей<br> компании в честь дня рождения школы.";
$lang['sk_activate'] = "Активировать карту";
$lang['where_need'] = "В какой стране вам нужен юрист?";
$lang['russkii'] = "Русский";
$lang['blok_'] = "Блок ";
$lang['trigery_'] = "тригеры ";
$lang['kak_eto_rabotaet_'] = "How it works";
$lang['vy_sami_reshaete'] = "You decide";
$lang['hotite_li_vy_rabotat_s_konkretnym_klientom___'] = "if you wish to work with the certain client";
$lang['priglashaem_k_nam'] = "Приглашаем к нам";
$lang['vy_urist'] = "Вы юрист?";
$lang['vyberite_stranu'] = "Выберите страну";
$lang['vashe_imja'] = "Ваше имя";
$lang['vash_telefon'] = "Ваш телефон";
$lang['opishite_vashu_zadaczu'] = "Describe your legal matter";
$lang['otpravit_'] = "Send";
$lang['laywer_page_h1_title'] = "We solve any legal problem <br /> around the world.  ONLINE";
$lang['laywer_page_h1_subtitle'] = "";
$lang['laywer_page_advantage_h2'] = "Our benefits";
$lang['laywer_page_advantage_1'] = "Get access <br>to the best projects
";
$lang['laywer_page_advantage_2'] = "Grow <br> you professional network";
$lang['laywer_page_advantage_3'] = "100% <br> Payment guarantee";
$lang['laywer_page_advantage_4'] = "Improve <br> your reputation";
$lang['laywer_page_howitworks_h2'] = "How it works";
$lang['laywer_page_howitworks_item_1_title'] = "Search for new clients";
$lang['laywer_page_howitworks_item_1_text'] = "We find clients in accordance with your specialization &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$lang['laywer_page_howitworks_item_2_title'] = "Your choice";
$lang['laywer_page_howitworks_item_3_title'] = "Use escrow services";
$lang['laywer_page_howitworks_item_3_text'] = "We guarantee payment in case of successful completion of the work";
$lang['laywer_page_requirements_title'] = "Our requirements";
$lang['laywer_page_requirements_list_1'] = "University education in law";
$lang['laywer_page_requirements_list_2'] = "More than 6 years experience";
$lang['laywer_page_requirements_list_3'] = "References ";
$lang['laywer_page_requirements_list_4'] = "Personal interview";
$lang['laywer_page_requirements_btn'] = "Join our network";
$lang['footer_info'] = " <b>Legal Space</b> — is an international community of more than 2 000 first-class lawyers
                                                       <br>Our service allows you to get high quality legal services on the transparent basis.";
$lang['footer_copyright'] = "© 2015 Legal Space Ltd., All rights reserved.";

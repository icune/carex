
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title>Title</title>

    <!-- Bootstrap core CSS -->
    <link href="http://yastatic.net/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="/css/dasboard.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
    <script type="text/javascript" src="/js/jquery-2.1.4.js"></script>
   
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">CarExp Club</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="user-name"><a href="/in/profile"><i class="fa fa-user"></i> 
            <?php echo User::displayName(); ?>
            </a></li>
            <li><a href="/in/tasks"><i class="fa fa-car"></i> Заявки</a></li>
              
            <li class="lk-login"><a href="/u/logout"><i class="fa fa-sing-out"></i> Выход</a></li>
          </ul>
          
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row row-offcanvas row-offcanvas-left">
       
       
       <div class="col-xs-6 col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="/in/profile">Профиль</a></li>
            <li><a href="/in/tasks">Заявки</a></li>
            <li><a href="/in/expert">Эксперты</a></li>
            <!-- <li><a href="#">Обратная связь</a></li> -->
          </ul>
        </div>
<?php

final class Exp{
	private static $db = null;
	private static $loader;
	public static $image_c = '';
	public static function init(){
		$loader = new CI_Loader();
		self::$db = $loader->database('', true);
		self::$loader = $loader;
	}
	public static function add($expert){
		
	}
	public static function specializations(){
		$sql = "SELECT * FROM expert_specialization";
		return self::$db->query($sql)->result_array();
	}
	public static function cities(){
		$sql =  "SELECT ui.city city FROM user u JOIN user_info ui ON ui.user_id = u.id ";
		$sql .= "WHERE u.is_expert GROUP BY ui.city";
		$r =  self::$db->query($sql)->result_array();
		$_r = array();
		foreach($r as $c)
			$_r []= $c['city'];
		return $_r;
	}
	public static function directions(){
		$sql = "SELECT * FROM expert_direction";
		return self::$db->query($sql)->result_array();
	}
	public static function services($user_id){
		$sql = "SELECT * FROM expert_service WHERE user_id=$user_id";
		return self::$db->query($sql)->result_array();
	}
	public static function info($user_id){
		$sql = "SELECT * FROM user WHERE id=$user_id";
		$user = self::$db->query($sql)->result_array();	
		$user = $user[0];

		$sql = "SELECT * FROM user_info WHERE user_id=$user_id";
		$user_info = self::$db->query($sql)->result_array();	
		$user_info = $user_info[0];

		$sql = "SELECT * FROM expert WHERE user_id=$user_id";
		$expert = self::$db->query($sql)->result_array();	
		$expert = $expert[0];

		$sql = "SELECT * FROM expert_service WHERE user_id=$user_id";
		$services = self::$db->query($sql)->result_array();		

		return array(
			'user' => $user,
			'user_info' => $user_info,
			'expert' => $expert,
			'services' => $services
		);
	}
	public static function filter($filter){
		
		if(!$filter || !count($filter))
			$filter = array();
		
		$_f = array();
		$ok_f = array('ui.city', 'e.specializations');
		foreach($filter as $k => $f){
			$k = str_replace('_', '.', $k);
			if(in_array($k, $ok_f) && trim($f))
				$_f[$k] = $f;
		}
		$_f = array_merge($_f, array('u.is_expert' => 1));
		
		$sql = "SELECT * FROM user u JOIN user_info ui ON ui.user_id = u.id ";
		$sql .= "JOIN expert e ON e.user_id = u.id ";
		
		$sql .= "WHERE ";
		$and = array();
		foreach ($_f as $key => $value) {
			if($key == 'e.specializations'){
				if(intval($value))
					$and []= $key . ' LIKE "%' . intval($value) . '%"';
			}else	
				$and []= $key . '=' . self::$db->escape($value);
		}
		$sql .= implode(' AND ', $and);
		$r = self::$db->query($sql)->result_array();
		foreach ($r as $k=>$e) {
			$serv = self::$db->query("SELECT * FROM expert_service WHERE user_id=".$e['id'])->result_array();
			$r[$k]['services']	= $serv;
		}
		return $r;
	}
	public static function exists($user_id){
		$sql = "SELECT * FROM user WHERE id=$user_id";
		$user = self::$db->query($sql)->result_array();	
		if(!count($user))
			return false;
		if(!intval($user[0]['is_expert']))
			return false;
		return true;
	}
}
Exp::init();
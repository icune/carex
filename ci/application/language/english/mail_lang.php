<?php 
$lang['client_greeting'] = 'Welcome to Legal Space!';
$lang['client'] = 'Dear %name%
Welcome to Legal Space!
Access the chat with the lawyer we carefully selected to solve your legal matter.
Legal Space is an international community of more than 2 000 first-class lawyers. Our service allows you to get high quality legal services on the transparent basis.
';
$lang['lawyer_greeting'] = 'Welcome to Legal Space!';
$lang['lawyer'] = 'Dear %name%
Thank you for your interest in Legal Space!
We received your application and will contact you shortly.
Legal Space is an international community of more than 2 000 first-class lawyers. Our service allows you to get orders from the clients from all over the world.  
';
<?php 

$lang['form_name'] =  'Your name';
$lang['form_phone'] =  'Phone';
$lang['form_email'] =  'E-mail';
$lang['form_country'] =  'Country';
$lang['form_skype'] =  'Skype';
$lang['form_linkedin'] =  'LinkedIn';
$lang['form_area'] =  'Specialization';
$lang['form_i'] =  'Lawyer';
$lang['form_they'] =  'Law firm';
$lang['form_register'] =  'Register';
$lang['add'] =  'Join';
$lang['form_do_register'] =  'Register';
$lang['form_area_select'] = 'Your specialization';
$lang['form_areas'] = array(
	'Constitutional Law', 
	'Civil Law', 
	'Family Law', 
	'Real Estate', 
	'Employment', 
	'Consumer Law', 
	'Notary', 
	'Criminal Defence', 
	'Automotive', 
	'Tax Law', 
	'Customs', 
	'Intellectual Property', 
	'Accounting', 
	'Corporate Law', 
	'Capital Markets', 
	'Insurance', 
	'Immigration', 
	'Antitrust Law', 
	'International Law', 
	'Administrative Law', 
	'Conflict Of Laws', 
	'Alternative Dispute Resolution', 
	'Maritime Law', 
	'Banking Law', 
	'Litigation', 
	'Trusts',
	'Another'
);
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expert extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!User::$id || User::$email != 'a@a.a')
			header('Location: /login');
	}
	public function index(){
		$this->load->database();
		$db = $this->db;

		$sql =  'SELECT * from user u JOIN user_info ui ON ui.user_id=u.id ';
		$sql .= 'WHERE u.is_expert';
		$r = $db->query($sql)->result_array();
		//$this->load->helper('expert');
		$this->load->view('imperium/expert-list', array(
			'experts' => $r
		));
	}
	public function add(){
		$this->load->helper('expert');
		$user = array(
			'email' => 'test@test.ru',
			'phone' => '623678213687'
		);
		$user_info = array(
			'image' => '/img/ava.png',
			'name' => 'Иван',
			'surname' => 'Иванов',
			'patronym' => 'Иванович',
			'country' => 'Россия',
			'city' => 'Москва'
		);
		$expert = array(
			'is_abroad' => 0,
			'experience' => 'Опыт',
			'work_conditions' => 'Условия работы',
			'checked' => 0,
			'rating' => 5,
			'specializations' => '',
			'directions' => '',
			'departure' => 'Куда выезжает',
		);
		$services = array(
			array(
				'id' => 'new',
				'name' => 'Автомобиль на заказ (под ключ)',
				'price' => 'от 20 000 руб'
			)
		);
		$this->load->view('imperium/expert-add', array(
			'user' => $user,
			'user_info' => $user_info,
			'expert' => $expert,
			'specializations' => Exp::specializations(),
			'directions' => Exp::directions(),
			'services' => $services
		));
	}
	public function edit(){
		if(!isset($_GET['user_id'])) die('');
		$user_id = intval($_GET['user_id']);

		$this->load->helper('expert');
		$info = Exp::info($user_id);
		$this->load->view('imperium/expert-add', array_merge($info, array(
			'specializations' => Exp::specializations(),
			'directions' => Exp::directions(),
			'user_id' => $user_id
		)));
	}
	public function create(){
		$this->insert();
		header('Location: /imperium/expert/');
	}
	public function change(){
		$this->update();
		header('Location: /imperium/expert/');
	}
	public function delete(){
		$id = $this->postAbsent('user_id');
		$this->load->database();
		$db = $this->db;
		$id = intval($id);
		$db->delete('user', array('id' => $id));
		$db->delete('user_info', array('user_id' => $id));
		$db->delete('expert', array('user_id' => $id));
		$db->delete('expert_service', array('user_id' => $id));

		header('Location: /imperium/expert/');
	}
	private function manage_specializations($user_id){
		$this->load->database();
		$db = $this->db;
		$sp_id = array();
		if(isset($_POST['specializations'])){
			$sp = $_POST['specializations'];
			foreach($sp as $k=>$v){
				if($v)
					$sp_id []= $k;
			}
		};
		if(isset($_POST['new_specializations'])){
			$nsp = $_POST['new_specializations'];
			foreach($nsp as $n){
				$is = strpos($n, '[on]') !== false;
				$n = str_replace('[on]', '', $n); 
				$db->insert('expert_specialization', array('name' => $n));
				$id = $db->insert_id();
				if($is) $sp_id []= $id;
			}
		};
		$db->update('expert', array(
			'specializations' => implode(',', $sp_id)
		), array(
			'user_id' => $user_id
		));
	}
	private function manage_directions($user_id){
		$this->load->database();
		$db = $this->db;
		$sp_id = array();
		if(isset($_POST['directions'])){
			$sp = $_POST['directions'];
			foreach($sp as $k=>$v){
				if($v)
					$sp_id []= $k;
			}
		};
		if(isset($_POST['new_directions'])){
			$nsp = $_POST['new_directions'];
			foreach($nsp as $n){
				$is = strpos($n, '[on]') !== false;
				$n = str_replace('[on]', '', $n); 
				$db->insert('expert_direction', array('name' => $n));
				$id = $db->insert_id();
				if($is){
					$sp_id []= $id;
				};
			}
		}
		$db->update('expert', array(
			'directions' => implode(',', $sp_id)
		), array(
			'user_id' => $user_id
		));
	}
	private function manage_services($user_id){
		$this->load->database();
		$db = $this->db;

		$serv = $_POST['service'];
		foreach($serv as $k => $s){
			if(intval($k))
				$this->db->update('expert_service', array(
					'name' => $s['name'],
					'price' => $s['price']
				), array(
					'id' => $k
				));
			else{
				$this->db->insert('expert_service', array(
					'name' => $s['name'],
					'price' => $s['price'],
					'user_id' => $user_id
				));
			}
		}
	}
	private function insert(){
		$this->load->database();
		$db = $this->db;
		$id = User::register(array(
			'is_expert' => 1,
			'email' => $_POST['user']['email'],
			'phone' => $_POST['user']['phone']
		), true, true);
		if($id == 'already') die('E-mail существует');
		$db->insert('user_info', array_merge($_POST['user_info'], array('user_id' => $id)));
		$expert = $_POST['expert'];
		if(isset($expert['is_abroad'])) $expert['is_abroad'] = 1;
		if(isset($expert['checked'])) $expert['checked'] = 1;

		$db->insert('expert', array_merge($expert, array('user_id' => $id)));
		$this->manage_specializations($id);
		$this->manage_directions($id);
		$this->manage_services($id);
	}
	private function update(){
		$this->load->database();
		$db = $this->db;
		$id = $_POST['user_id'];
		$db->update('user', $_POST['user'], array('id' => $id));
		$db->update('user_info', $_POST['user_info'], array('user_id' => $id));
		$expert = $_POST['expert'];
		if(isset($expert['is_abroad'])) $expert['is_abroad'] = 1;
		if(isset($expert['checked'])) $expert['checked'] = 1;

		$db->update('expert', $expert, array('user_id' => $id));
		$this->manage_specializations($id);
		$this->manage_directions($id);
		$this->manage_services($id);
	}
}

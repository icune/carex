<!DOCTYPE html>
<!-- This site was created in Webflow. http://www.webflow.com-->
<!-- Last Published: Wed Sep 30 2015 21:33:57 GMT+0000 (UTC) -->
<html data-wf-site="560c19c13a2e2cbc544de9cf" data-wf-page="560c19c13a2e2cbc544de9ce">
<head>
  <meta charset="utf-8">
  <title>ls-howitworks-sfalsjfaslhfeo323</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="generator" content="Webflow">
  <link rel="stylesheet" type="text/css" href="/fland/how/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="/fland/how/css/webflow.css">
  <link rel="stylesheet" type="text/css" href="/fland/how/css/ls-howitworks-sfalsjfaslhfeo323.webflow.css">
  <script type="text/javascript" src="/js/modernizr.js"></script>
  <script type="text/javascript" src="/js/wiggy.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="/fland/how/images/favicon.ico">
  <link rel="apple-touch-icon" href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png">
  <style type="text/css">
    @media screen and (min-width: 1200px) {
      .w-container {
        max-width: 1170px;
      }
    }
  </style>
  <script src="/js/jquery-2.1.4.js"></script>
</head>
<body class="body">
  <?php $this->view('header'); ?>
  <div class="w-section hiw-section-1">
    <div class="w-container hiw-container">
      <h1 class="hiw-h1"><?php echo $_["bystro__udobno__bezo"]; ?></h1>
      <p class="hiw-p1"><?php echo $_["my_pomozhem_naiti_va"]; ?></p>
    </div>
  </div>
  <div class="w-section hiw-section-2">
    <div class="w-container">
      <h1 class="hiw-h2"><?php echo $_["s_nami_rabotaet_bole"]; ?> 2000 <?php echo $_["uristov_po_vsemu_mir"]; ?></h1>
      <p class="hiw-p2"><?php echo $_["vy_mozhete_poluczit_"]; ?></p>
    </div>
  </div>
  <div class="w-section hiw-section-3">
    <div class="w-container">
      <h1 class="hiw-h3"><?php echo $_["prosto_vyberite_stra"]; ?> <?php echo $_["v_kotoroi_vam_nuzhen"]; ?></h1>
      <p class="hiw-p3"><?php echo $_["esli_u_nas_vozniknut"]; ?> <?php echo $_["neobhodimye_dlja_vyb"]; ?> <?php echo $_["my_vam_perezvonim"]; ?></p>
    </div>
  </div>
  <div class="w-section hiw-section-3 _3s">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6 w-col-stack hiw-dev-colleft">
          <div class="hiw-videosection">
            <div class="w-embed">
              <video autoplay="" loop="" style="width:100%; height: auto; z-index: 1;">
                <source src="/video/hiw-ipadmovie.mp4" type="video/mp4">
                  <img src="/video/hiw-ipad.jpg">
              </video>
            </div>
          </div>
        </div>
        <div class="w-col w-col-6 w-col-stack hiw-dev-column">
          <h1 class="hiw-h3 h3"><?php echo $_["my_naidem_podhodjasc"]; ?> <?php echo $_["opytom_reshenija_ime"]; ?></h1>
          <p class="hiw-p3 p3"><?php echo $_["kazhdyi_urist_"]; ?>Legal Space <?php echo $_["prohodit_tsczatelnyi"]; ?>, <?php echo $_["poetomu_my_polnostu_"]; ?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="w-section hiw-section-3 _4s">
    <div class="w-container">
      <h1 class="hiw-h3 white"><?php echo $_["obsczaites_s_uristom"]; ?></h1>
      <p class="hiw-p3 white"><?php echo $_["vy_obsuzhdaete_vozni"]; ?></p>
    </div>
  </div>
  <div class="w-section hiw-section-3 _5s">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-7 w-col-stack hiw-dev-column image"><img src="/fland/how/images/hiw-iphones.jpg">
        </div>
        <div class="w-col w-col-5 w-col-stack hiw-dev-column">
          <h1 class="hiw-h3 h4"><?php echo $_["obsczenie_prohodit_b"]; ?></h1>
          <p class="hiw-p3 p4"><?php echo $_["vy_mozhete_perepisyv"]; ?></p><a class="w-button hiw-findlayer-btn" href="/?fs=1"><?php echo $_["naiti_urista"]; ?></a>
        </div>
      </div>
    </div>
  </div>
  <div class="w-section hiw-section-3 _6s">
    <div class="w-container hiw-payament-block">
      <h1 class="hiw-h3 h5"><?php echo $_["bezopasno_provodite_"]; ?></h1>
      <p class="hiw-p3 p5"><?php echo $_["dengi_s_vashego_scze"]; ?></p><img class="hiw-payment-logo" src="/fland/how/images/hiw-visa.png"><img class="hiw-payment-logo" src="/fland/how/images/hiw-paypal.png"><img class="hiw-payment-logo" src="/fland/how/images/hiw-mc.png">
    </div>
  </div>
  <div class="w-section hiw-section-logos">
    <div class="w-container">
      <h1 class="hiw-h4"><?php echo $_["my_rabotaem_pri_podd"]; ?></h1>
      <div class="hiw-logos-block"><img class="hiw-sup-logo" src="/fland/how/images/hiw-oxford.png"><img class="hiw-sup-logo" src="/fland/how/images/hiw-berkeley.png"><img class="hiw-sup-logo" src="/fland/how/images/hiw-yale.png"><img class="hiw-sup-logo" src="/fland/how/images/hiw-lse.png"><img class="hiw-sup-logo" src="/fland/how/images/hiw-skolkovo.png">
      </div>
    </div>
  </div>
  <div class="w-section footer1-section">
    <div class="w-container footer1-sontainer1">
      <div class="w-row">
        <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4"></div>
        <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4 footer1-socialblock">
          <a class="w-inline-block footer1-socialicon" href="https://www.facebook.com/Legal-Space-468707986637000/timeline/"><img class="footer1-sociallogo" src="/fland/how/images/footer-fb.png">
          </a>
          <a class="w-inline-block footer1-socialicon" href="https://www.linkedin.com/company/9340358?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9340358%2Cidx%3A1-1-1%2CtarId%3A1441991163447%2Ctas%3Alegal%20space"><img class="footer1-sociallogo" src="/fland/how/images/footer-in.png">
          </a>
        </div>
        <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4 w-clearfix footer1-forlawyerblock"><a class="w-button footer1-forlawyer-btn" href="/lawyer"><?php echo $_["ja_urist"]; ?></a>
          <div class="footer1-forlawyer-p"><?php echo $_["priglashaem_k_nam"]; ?></div>
        </div>
      </div>
    </div>
    <div class="w-container footer1-container2"></div>
  </div>
  <div class="w-section footer1-section s2">
    <div class="w-container footer1-container2"></div>
    <div class="w-container footer1-container3">
      <div class="w-row">
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p1"><?php echo $_['footer_text']; ?></p>
          <p class="mainfooter-3lane-p1"><?php echo $_['cpr']; ?></p>
        </div>
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p2"><?php echo $_['foo_1']; ?></p>
        </div>
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p2"><?php echo $_['foo_2']; ?></p>
        </div>
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p2"><?php echo $_['foo_3']; ?></p>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="/js/webflow.js"></script>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
  <?php $this->view('modal'); ?>
  <script type="text/javascript">
    window.curlang = '<?php echo $curlang;?>';
  </script>
</body>
</html>
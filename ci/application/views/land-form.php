<?php if(isset($top) || isset($lawyer)|| isset($bottom_form)): ?>
<div class="form-block">
  <div>
    
    	<div class="sel-cnt">
        <?php if (!isset($lawyer)): ?>
            <?php if(isset($bottom_form)): ?>
                <input type="text" class="wiggy white" id="phone" placeholder="<?php echo $_['phone']; ?>"/>
            <?php else: ?>
                <input type="text" class="cselect-external" value="<?php echo $_['where_you']; ?>"/>
            <?php endif; ?>
        <?php else: ?>
            
        <?php endif ?>
    		
    	</div>	
    
    <button class="btn" <?php if(isset($bottom_form)) echo 'id="phone-button"'; elseif(!isset($lawyer)) echo 'id="form-button"'; else echo 'id="lawyer-send"'; ?>>
        <?php if(!isset($lawyer)): ?>
    	   <?php echo isset($bottom_form) ? $_['bot_button'] : $_['get_free_consult']; ?>
        <?php else: ?>
            <?php echo $_['form_register']; ?>
        <?php endif; ?>
    </button>
  </div>
</div>
<?php if(isset($bottom_form) || isset($lawyer)): ?>
	<div id="phone-message"></div>
<?php endif; ?>
<?php elseif(isset($login)): ?>
<div class="form-block">
  <div>
    
        <div class="sel-cnt enter">
            <input type="text" class="wiggy white" id="email" name="email" placeholder="Email"/>
            <input type="password" class="wiggy white" id="password" name="password" placeholder="<?php echo $_['password']; ?>"/>

        </div>  
        <div class="sel-cnt restore" style="display:none">
            <input type="text" class="wiggy white" name="email" placeholder="Email"/>
        </div>  

        <div class="forget">
            <?php echo $_['forget']; ?>
        </div>  
        <center>
            <br>
			<?if(Info::$host=='legalspace.world'){?>
				<a href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=779scqmhyc7kbg&redirect_uri=http://<?php echo Info::$host; ?>/u/login/linkedin&state=987654321&scope=r_emailaddress">
                <img src="/fland/img/foot-linkedin.png" alt=""></a>
			<?}else{
				?>
				<a href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77od03o5igspcg&redirect_uri=http://<?php echo Info::$host; ?>/u/login/linkedin&state=987654321&scope=r_emailaddress">
                <img src="/fland/img/foot-linkedin.png" alt=""></a>
				<?
			}?>
            
        </center>    

    <button class="btn" id="login-button">
        <?php echo $_['send']; ?>    
    </button>
    <button class="btn" id="restore-button" style="display:none">
        <?php echo $_['restore']; ?>    
    </button>

  </div>
</div>  
<?php endif; ?>
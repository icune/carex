<?php $this->view('header'); ?>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Безопасная сделка</h4>
                </div>
                <div class="modal-body">
                    <p>Безопасная сделка - специальная услуга нашего сервиса по взаимодействию клиентов и автоэкспертов, где наш сервис выступает в роли независимой третьей стороны и гарантирует выполнение обязательств с обеих сторон. </p>

                    <p>Данная услуга позволяет оплатить услуги автоэксперта только после их выполнения (никаких авансов) и также данная услуга даёт уверенность эксперту,что его услуги будут оплачены.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Зарегистрироваться</button>

                </div>
            </div>
        </div>
    </div>



    <section class="reg-page" >
        <div class="container">
            <div class="row">
                <h2 class="reg-page-title">
                    Регистрация
                </h2>
                <div class="col-sm-6 reg-item">
                    <h4 class="reg-sub-title">
                        Я автолюбитель
                    </h4>
                    <p class="reg-descr">
                        Мне необходима помощь автоэкспертов в подборе и покупке  автомобиля или в решении других вопросов связанных с автотранспортом
                    </p>
                    
                    <div class="reg-user">
                        <div class="col-sm-8 col-sm-offset-2 input-area">
                            <label>Имя: <span class="must-input">*</span></label>
                            <input class="name contact-name " type="text" required>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2  input-area">
                            <label>Email: <span class="must-input">*</span></label>
                            <input class="email contact-name " type="text" required>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2  input-area">
                            <label>Телефон:</label>
                            <input class="phone contact-name " type="text">
                        </div>
                        <div class="col-sm-8 col-sm-offset-2 ">
                        <button type="" class="btn btn-default btn-effect">
                            Зарегистрироваться
                        </button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 reg-item">
                    <h4 class="reg-sub-title">
                        Я автоэксперт
                    </h4>
                    <p class="reg-descr">
                        Я имею профильный опыт работы в автомобильной сфере или могу с закрытыми глазами собрать и разобрать свой автомобиль
                    </p>
                    <div class="reg-expert">
                        <div class="col-sm-8 col-sm-offset-2  input-area">
                            <label>Имя: <span class="must-input">*</span></label>
                            <input class="name contact-name " type="text">
                        </div>
                        <div class="col-sm-8 col-sm-offset-2  input-area">
                            <label>Телефон: <span class="must-input">*</span></label>
                            <input class="phone contact-name " type="text">
                        </div>
                        <div class="col-sm-8 col-sm-offset-2  input-area">
                            <label>Email: </label>
                            <input class="email contact-name " type="text">
                        </div>
                        <div class="col-sm-8 col-sm-offset-2 ">
                        <button type="" class="btn btn-default btn-effect">
                            Зарегистрироваться
                        </button>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>



<?php $this->view('footer'); ?>
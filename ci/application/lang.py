# -*- coding: utf-8 -*-
import os
import re
import codecs
import sys

_dir = 'D:/_ist/wamp/www/ci/application'
files = [_dir + '/views/how.php']
# name: это строка которую транслитим
def translit(name):
   """
   Автор: LarsKort
   Дата: 16/07/2011; 1:05 GMT-4;
   Не претендую на "хорошесть" словарика. В моем случае и такой пойдет,
   вы всегда сможете добавить свои символы и даже слова. Только
   это нужно делать в обоих списках, иначе будет ошибка.
   """
   # Слоаврь с заменами
   slovar = {'а':'a','б':'b','в':'v','г':'g','д':'d','е':'e','ё':'e',
      'ж':'zh','з':'z','и':'i','й':'i','к':'k','л':'l','м':'m','н':'n',
      'о':'o','п':'p','р':'r','с':'s','т':'t','у':'u','ф':'f','х':'h',
      'ц':'c','ч':'cz','ш':'sh','щ':'scz','ъ':'','ы':'y','ь':'','э':'e',
      'ю':'u','я':'ja', 'А':'a','Б':'b','В':'v','Г':'g','Д':'d','Е':'e','Ё':'e',
      'Ж':'zh','З':'z','И':'i','Й':'i','К':'k','Л':'l','М':'m','Н':'n',
      'О':'o','П':'p','Р':'r','С':'s','Т':'t','У':'u','Ф':'Х','х':'h',
      'Ц':'c','Ч':'cz','Ш':'sh','Щ':'scz','Ъ':'','Ы':'y','Ь':'','Э':'e',
      'Ю':'u','Я':'ja',',':'','?':'',' ':'_','~':'','!':'','@':'','#':'',
      '$':'','%':'','^':'','&':'','*':'','(':'',')':'','-':'','=':'','+':'',
      ':':'',';':'','<':'','>':'','\'':'','"':'','\\':'','/':'','№':'',
      '[':'',']':'','{':'','}':'','ґ':'','ї':'', 'є':'','Ґ':'g','Ї':'i',
      'Є':'e', '.':'_'}
        
   # Циклически заменяем все буквы в строке
   for key in slovar:
      name = name.replace(key, slovar[key])
   return name[:20]	



span_rexp = '(<span\s+class=(?:\'|")lang_((?:.|\n)+?)(?:\'|")>((?:.|\n)*?)</span>)'
def vars(html):
	found = re.findall(span_rexp, html)
	r = []
	for f in found:
		r.append(f)
	return r
def replace(html, rep):
	for r in rep:
		html = html.replace(r[0], '<?php echo $_["%s"]; ?>'%r[1])
	return html
ru_rexp = '[а-яА-Я]{1}(?:[а-яА-Я .?!]|<br>|<br />|<br/>|<b>|</b>)+'
def gen_ru(html):
	found = re.findall(ru_rexp, html)
	r = []
	for f in found:
		r.append((f, translit(f), f));
	return r
def gen_lang(fn, rep):
	bn = os.path.basename(fn).split('.')[0]
	fn1 = _dir + '/language/english/land_' + bn + '_lang' + '.php';
	fn2 = _dir + '/language/russian/land_' + bn + '_lang' + '.php';
	_v = ["$lang['%s'] = '%s';" % (_r[1], _r[2]) for _r in rep]
	content = '<?php\n' + '\n'.join(_v);
	with codecs.open(fn1, 'w', 'utf-8') as f1:
		f1.write(content)
	with codecs.open(fn2, 'w', 'utf-8') as f2:
		f2.write(content)

for fn in files:
	with codecs.open(fn, 'r', 'utf-8') as f:
		html = f.read()
		spans = vars(html)
		html = replace(html, spans)
		ru = gen_ru(html)
		html = replace(html, ru)
		gen_lang(fn, ru + spans)	
		wfn = os.path.basename(fn) + '.html'
		with codecs.open(wfn + '.html', 'w', 'utf-8') as wf:
			wf.write(html)


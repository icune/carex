<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expert extends CI_Controller {
	public function index()
	{
		if(!isset($_GET['id']))
			header('Location: /experts');
		$this->load->helper('expert');
		$id = intval($_GET['id']);
		if(!Exp::exists($id))
			header('Location: /experts');
		$this->load->view('expert', array(
			'expert' => Exp::info($id),
			'specializations' => Exp::specializations()
		));
	}
}

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="/css/imperium/imperium.css">
	<link rel="stylesheet" href="/css/imperium/add-expert.css">
	<script type="text/javascript" src="/js/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="/js/net.js"></script>
	<script type="text/javascript" src="/js/Chunky.js"></script>
	<script type="text/javascript" src="/js/imperium/add-expert.js"></script>
	<link rel="stylesheet" href="/plug/guillotine/jquery.guillotine.css" />
    <script type="text/javascript" src="/plug/guillotine/jquery.guillotine.js"></script>
    <script type="text/javascript">
    	window.user_id = 0;
    	<?php if(isset($user_id) && $user_id): ?>
    		window.user_id = <?php echo $user_id ?>;

    	<?php endif; ?>
    	window.user_email = "<?php echo $user['email'] ?>";
    </script>
</head>
<body>
	<div class="content">
		<form id="main-form" action="<?php if(isset($user_id) && $user_id) echo '/imperium/expert/change'; else echo '/imperium/expert/create'; ?>" method="POST">
		<?php if(isset($user_id) && $user_id): ?>
    		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

    	<?php endif; ?>
    	<?php if(isset($user_id) && $user_id): ?>
			<h1>Редактировать эксперта</h1>
		<?php else: ?>
			<h1>Добавить эксперта</h1>	
		<?php endif; ?>
		<input type="text" name="user[email]" value="<?php echo $user['email']; ?>"> 
		<span id="busy"></span>
		<br>
		<input type="text" name="user[phone]" value="<?php echo $user['phone']; ?>"> <br>
		<br>
		<img src="<?php echo $user_info['image']; ?>" alt="" class="img" id="image"/>
		<input type="hidden" name="user_info[image]" id="image-input" value="<?php echo $user_info['image']; ?>" />
		<input type="file" id="upl">
		<br>

		<input type="checkbox" name="expert[checked]" 
		<?php if((int)($expert['checked'])) echo 'checked=checked'; ?>
		> Данные проверены

		<br>
		<input type="text" name="user_info[name]" value="<?php echo $user_info['name']; ?>"> <br>
		<input type="text" name="user_info[surname]" value="<?php echo $user_info['surname']; ?>"> <br>
		<input type="text" name="user_info[patronym]" value="<?php echo $user_info['patronym']; ?>"> <br>
		<input type="text" name="user_info[country]" value="<?php echo $user_info['country']; ?>"> <br>
		<input type="text" name="user_info[city]" value="<?php echo $user_info['city']; ?>"> <br>

		<br>
		
		<input type="checkbox" name="expert[is_abroad]" 
		<?php if((int)$expert['is_abroad']) echo 'checked=checked'; ?>
		> Выезд заграницу <br>
		<textarea name="expert[departure]" ><?php echo $expert['departure']; ?></textarea> <br>
		<textarea name="expert[experience]" ><?php echo $expert['experience']; ?></textarea> <br>
		<textarea name="expert[work_conditions]" ><?php echo $expert['work_conditions']; ?></textarea> <br>
		Рейтинг (от 1 до 5) : <input type="text" name="expert[rating]" value="<?php echo $expert['rating']; ?>">		<br>
	
		Специализации: <br>
		<div class="checkboxes" id="specializations">
			<?php foreach($specializations as $s): ?>
				<div class="spec">
					<input type="checkbox" name="specializations[<?php echo $s['id']; ?>]"
					<?php if(in_array($s['id'], explode(',', $expert['specializations']))) echo 'checked=checked'; ?>
					> 
					<span><?php echo $s['name']; ?></span>
				</div>
			<?php endforeach; ?>
		</div>
		<br>
		<input type="text" id="new-specialization" placeholder="Название специализации"/>
		<div id="add-specialization" class="btn">Добавить специализацию</div><br>
		
		Направления: <br>
		<div class="checkboxes" id="directions">
			<?php foreach($directions as $s): ?>
				<div class="spec">
					<input type="checkbox" name="directions[<?php echo $s['id']; ?>]"
					<?php if(in_array($s['id'], explode(',', $expert['directions']))) echo 'checked=checked'; ?>
					> 
					<span><?php echo $s['name']; ?></span>
				</div>
			<?php endforeach; ?>
		</div>
		<br>
		<input type="text" id="new-direction" placeholder="Название направления"/>
		<div id="add-direction" class="btn">Добавить направление</div><br><br>
		Услуги
		<div class="checkboxes" id="services">
			<?php foreach($services as $s): ?>
				<div class="serv">
					<input type="text" name="service[<?php echo $s['id']; ?>][name]" value="<?php echo $s['name'] ?>">
					<input type="text" name="service[<?php echo $s['id']; ?>][price]" value="<?php echo $s['price'] ?>">
					<div class="btn del">Удалить</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div id="add-service" class="btn">Добавить услуги</div><br><br>
		<input type="submit">
		</form>
		<?php if(isset($user_id) && $user_id): ?>
			<br><br><br>
    		<form action="/imperium/expert/delete" id="del-form" method="POST">
    			<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

    			<button style="background-color:#F28787;">Удалить</button>
    		</form>

    	<?php endif; ?>
	</div>

	<link rel="stylesheet" href="/css/imperium/crop.css">
	<div class="fluid" style="display:none">
        <div class="center">
            <div class="img-cont">
                <img src="/img/user-default.png" alt="" id="img" />
            </div>
            <div class="buts">
                <div class="but" id="plus">+</div>
                <div class="but" id="minus">-</div>    
                <div class="but ok" id="ok">OK</div>
            </div>
            
        </div>
    </div>  
</body>
</html>
<?php 
	function detect_lang(){

		if(isset($_COOKIE['language_id']))
			if($_COOKIE['language_id'] == 'russian')
				return 'russian';
			else
				return 'english';
        if(!isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            return 'english';
		if (($list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']))) {
            if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
                $lang = array_combine($list[1], $list[2]);
                foreach ($lang as $n => $v)
                    $lang[$n] = $v ? $v : 1;
                arsort($lang, SORT_NUMERIC);
            }
        } else $lang = array();

        $ak = array_keys($lang);
        if(count($ak) && ($ak[0] == 'ru-ru' || $ak[0] == 'ru'))
        	return 'russian';

        return 'english';
	}
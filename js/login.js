function bind(){
    var curlang = 'russian';
	var r = {};
    var fb = $('.box');
	function get(){
		r = {};
        fb.find('.email,.password').each(function(){
            r[$(this).prop('name')] = $(this).val();
        });
	}
	function check1(){
		var lang = (curlang == 'russian' ? {
            wrong_email: 'Неправильный E-mail',
            restore_send: 'Инструкции по изменению пароля были отправлены на указанный email-адрес',
            fill_password: 'Неправильный пароль'
        } : {
            wrong_email: 'Wrong E-mail',
			restore_send: 'Instructions how to reset your password have been sent to your email-address',
            fill_password: 'Wrong password'
        });
        var emps = ['email', 'password'];
        var regx = {
            email:{
                pre:function(txt){return txt.replace(/\s+/g, '');},
                reg:/^.+@.+\..+$/,
                msg:lang.wrong_email
            },
            password:{
                pre:function(txt){return txt.replace(/\s+/g, '');},
                reg:/^.+$/,
                msg:lang.fill_password
            }
        };

        function elemGet(name){
            return fb.find('.'+name);
        };
        for(var k in emps){
            if(!Wiggy.check(emps[k], r, emps, regx, elemGet)){
                return false;
            }
        }

        return true;
	}
	function check2(){
		Net.post('/u/login/check', r, {
			success:function(){
                window.location.href = '/u/login?email=' + r.email + 
                '&password='+r.password+
                '&redirect=/in/tasks/';
			},
			error:function(msg){
                if(msg == 'abs-email'){
                    Wiggy.blink(fb.find('.email'));
					Wiggy.modal(curlang == 'english' ? 'Error' : 'Ошибка', curlang == 'english' ? 'Unknown user' : 'Пользователь с таким email не найден');
                    return;
                }
                if(msg == 'wrong-password'){
                    Wiggy.blink(fb.find('.password'));
					Wiggy.modal(curlang == 'english' ? 'Error' : 'Ошибка', curlang == 'english' ? 'Incorrect password' : 'Не верный пароль');
                    return;
                }
				Wiggy.modal(curlang == 'english' ? 'Error' : 'Ошибка', curlang == 'english' ? 'Unknown error' : 'Неизвестная ошибка');
			}
		})
	}

	fb.find('.enter').click(function(){
		get();
		if(check1())
			check2();
	});


    fb.find('.restore').click(function(){
        fb.find('.restore').hide();
        fb.find('.enter').hide();
        fb.find('.password').hide();
        fb.find('.send-password').show();
    })
    fb.find('.send-password').click(function(){
        var eEl = fb.find('.email');
        if(!eEl.val().trim().match(/^.+@.+\..+$/)){
            Wiggy.blink(eEl);
            return;
        }

        function success(){
            fb.find('.send-password').hide();      
            Net.post('/u/login/restore', {email:eEl.val().trim()}, {
                success:function(){
                    Wiggy.modal(curlang == 'english' ? 'Восстановление' : 'Восстановление', curlang == 'english' ? 'Instructions how to reset your password have been <br/>sent to your email-address' : 'Инструкции по изменению пароля были <br/>отправлены на указанный email-адрес');
                },
                error:function(msg){
                    Wiggy.modal(curlang == 'english' ? 'Восстановление' : 'Восстановление', curlang == 'english' ? 'Error: restore password <br/>finish badly' : 'Ошибка восставноления <br/>пароля');
                }
            }); 
            
        }

        Net.post('/u/login/check', {email:eEl.val().trim(), password:'xxx'}, {
            success:function(){
                success();    
            },
            error:function(msg){
                if(msg == 'abs-email'){
                    Wiggy.modal('', lang.wrong_email);
                    return;
                }
                success();
            }
        }) 

    });
};
$(function(){
    bind();
})
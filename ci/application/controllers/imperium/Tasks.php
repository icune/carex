<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!User::$id || User::$email != 'a@a.a')
			header('Location: /login');
	}
	public function index(){
		
	}
	function task(){
		$this->load->database();

		$users  = $this->db->query("
			SELECT * FROM user u JOIN user_info ui ON ui.user_id=u.id WHERE NOT is_expert
		")->result_array();
		$this->load->helper('url');

        /* ------------------ */    
 
        $this->load->library('grocery_CRUD'); 
        $this->grocery_crud->set_table('task');
        
        $this->load->helper('expert');
        $this->grocery_crud->field_type('expert_id','dropdown',
        	Utils::concar(
        		Utils::assocKey(
        			Exp::filter(null)
        		, 'user_id')
        	, array('surname', 'name', 'patronym'))
        );
        $this->grocery_crud->field_type('status','dropdown',
            array('new' => 'Новая', 'go' => 'В процессе','done' => 'Готово'));


        $this->grocery_crud->display_as('user_id', 'E-mail пользователя');
        $this->grocery_crud->set_relation('user_id','user','email');
        // $this->grocery_crud->callback_before_insert(array($this, 'cb_insert'));
        $this->grocery_crud->add_action('Сделать кейс', '/img/add139.png', '/in/profile','ui-icon-plus', array(
        	$this, 'make_case'
        ));
		$output = $this->grocery_crud->render();
		$data = array(
			'grocery' => (array)$output,
			'users' => $users,
			'title' => 'Список заявок'
		);

		$this->load->view('imperium/groc', $data);
	}
	// function cb_insert($arr){
	// 	$arr['desc'] = '[[[' . $arr['desc'] . ']]]';
	// 	return $arr;
	// }
	function make_case($key, $row){
		return site_url('/imperium/tasks/case_make?task_id=' . $key);
	}
	function case_make(){
		if(!isset($_GET['task_id']))
			die('No task_id');

	}
}

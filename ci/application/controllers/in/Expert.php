<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Expert extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!User::$id)
			header('Location: /login');
		$this->load->helper('expert');
	}
	public function index(){
		$this->load->database();
		$db = $this->db;

		$r = Exp::filter($_GET);
		//$this->load->helper('expert');
		$this->load->view('in/expert-list', array(
			'experts' => $r,
			'cities' => Exp::cities(),
			'specializations' => Exp::specializations(),
		));
	}
	public function profile(){
		if(!isset($_GET['id'])) header('Location: /in/expert');

		$expert = Exp::info(intval($_GET['id']));
		//$this->load->helper('expert');
		$this->load->view('in/expert-profile', array(
			'e' => $expert,
			'specializations' => Exp::specializations()
		));
	}
}

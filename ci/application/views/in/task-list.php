<?php $this->view('in/header'); ?>

<div class="col-sm-9  col-md-10  main">
    <div class="row">
        <p class="pull-left btn-left-sidebar visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
        </p>
    </div>
  <div class="row">
               <h2>Заявки</h2>
               
               <table class="leads-table table table-striped">
                   <tr>
                       <th>№</th>
                       <th>Авто</th>
                       <th>От</th>
                       <th>До</th>
                       <th>Статус</th>
                       <th>Эксперт</th>
                       <th>Действия</th>
                   </tr>
                   
                   <?php $st = array(
                   		'new' => 'Новая',
                   		'go' => 'В процессе...',
                   		'done' => 'Готово',
                   ) ?>
                   <?php foreach($tasks as $t): ?>
						

						<tr>
	                       <td><?php echo $t['id']; ?></td>
	                       <td><?php echo $t['desc']; ?></td>
	                       <td><?php echo $t['from'];  ?> р.</td>
	                       <td><?php echo $t['to']; ?> р.</td>
	                       <td><?php echo $st[$t['status']]; ?></td>
	                       <td>
	                       	<?php if(!$t['expert_id']): ?>
	                       		Эксперт еще не выбран
	                       	<?php else: ?>
	                       		<a href="/in/expert/profile?id=<?php echo $t['expert_id']; ?>">
	                       			<?php echo $t['expert_surname'] ?>&nbsp;
	                       			<?php echo $t['expert_name'] ?>&nbsp;
	                       			<?php echo $t['expert_patronym'] ?>&nbsp;
	                       		</a>
	                       	<?php endif; ?>
	                       </td>
	                       <td>
	                       	<?php if($t['status'] == 'new'): ?>
	                       		<a href="/in/tasks/edit?id=<?php echo $t['id'] ?>" class="lead-canceled"><i class="fa fa-edit"></i> Изменить</a>
                           		<a href="/in/tasks/cancel?id=<?php echo $t['id'] ?>" class="lead-chanched"><i class="fa fa-close"></i> Отменить</a>
	                       	<?php elseif($t['status'] == 'done'): ?>	
	                       		<a href="order.php" class="view-auto">
	                            	<i class="fa fa-car"></i>    Посмотреть авто
	                           	</a>
	                       	<?php endif; ?>
	                       </td>
	                   </tr>
					<?php endforeach; ?>
               </table>
               
               
           </div>       
           <a href="/in/tasks/add">Добавить</a>
</div>

<?php $this->view('in/footer'); ?>
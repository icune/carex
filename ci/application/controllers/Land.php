<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Land extends CI_Controller {
	public function index()
	{
		$this->load->helper('expert');
		if(isset($_GET['experts'])){
			print_r(Exp::filter(null));
			exit();
		};
		$this->load->view('land', array(
			'experts' => Exp::filter(null)
		));
	}
	public function send(){
		
		$email = $_POST['data']['_email'];
		$this->mailWe('icune@ya.ru', $_POST['data']);
		$this->mailClient($email);

		$this->ok();
	}
	private function mailClient($email){
		if(!preg_match('!^.+@.+\..+$!', $email)) return;
		
		$msg = file_get_contents(Info::$rootDir . '/mail.html');
	
		$this->load->helper('mail');
		Mail::smtp($email, $msg);		
		
		return;
	}
	private function mailWe($email, $data){
		if(!preg_match('!^.+@.+\..+$!', $email)) return;
		
		$msg_p = array();
		foreach($data as $k=>$d)
			$msg_p []= "$k : $d";

		$msg = implode("<br>", $msg_p);
	
		$this->load->helper('mail');
		Mail::smtp($email, $msg);		
		
		return;
	}
}

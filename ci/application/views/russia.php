<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">

<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />


<head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="/favicon.png" />
    <meta charset="UTF-8" />
    <script type="text/javascript">
        window.lang = {
            "are_you": "For lawyers",
            "title": "Legal space",
            "best_juri_online": "Best lawyers worldwide. Online",
            "bla_bla_before_form": "Fill in the details and find your lawyer",
            "get_free_consult": "Get free advice",
            "where_you": "В какой стране Вам нужен юрист?",
            "desc_problem": "Describe your legal matter",
            "send": "Send",
            "footer_blabla": "\u00a9 Copyright 2015. Legal Space",
            "rights": "All rights reserved",
            "moscow": "Moscow",
            "moscow_addr": "15 Presnensky val",
            "milan": "Milan",
            "milan_addr": "Via Friuli, 51 - 20135",
            "nikosia": "Nikosia",
            "nikosia_addr": "Psaron , 2408 ",
            "from_where_you": "Where are you from?",
            "name": "Your name",
            "phone": "Телефон",
            "success": "Thank you for your request! We\u2019re on your case.",
            "error": "Something went wrong. Please contact us and we will help: info@legalspace.world",
            "services": ["Trademarks and intellectual property", "International tax and corporate structuring", "Corporate M&A", "Private equity", "Real estate", "Tax", "Investments", "Company formation ", "Investment funds", "Restructuring and insolvency", "Labour law", "Competition and antitrust", "Banking and finance", "Litigation & dispute resolution", "Private wealth and succession planning"],
            "bla_1": "<p>LEGAL SPACE is a safe and reliable virtual resource to help you resolve your legal issues online. Whether you are setting up a company in Cyprus, registering a trademark, or applying for residential rights abroad - whatever legal service you need, in whichever part of the world, you can find it right now on our website. <\/p><p>Every LEGAL SPACE lawyer passes through a highly competitive selection process, which is why we are sure of his or her qualifications and professionalism and can guarantee the quality of his or her services.<\/p>",
            "bla_2": "<p>All of our lawyers have a minimum of five years professional experience. Every candidate undergoes a personal interview before they start work, while lawyers may only join the LEGAL SPACE professional pool through recommendation.<\/p><p> Our partners are outstanding representatives of the legal profession; they are progressive and always abreast of the latest developments in the industry. Together we are changing the world of legal services for the better. <\/p>",
            "we_connect": "We connect you with the best lawyers DIRECTLY",
            "spoiled_phone": "Our comprehensive geographical database allows us to solve your issue in any country.",
            "with_support": "With support",
            "globe_under": "countries",
            "hat_under": "lawyers",
            "case_under": "clients",
            "office_title": "Our offices:",
            "office_desc": "Call us and we will help",
            "moscow_desc": "Moscow office is located in the very centre of Moscow, 5 minutes walk from the metro Belorusskaya. There is the parking for the clients.",
            "milan_desc": "Our office in Milan is located in the very centre of Milan, 10 minutes walk from the metro Lodi.\r\n",
            "nikosia_desc": "Our office in Cyprus is conveniently located near the airport.\r\n",
            "find_descision": "Find legal solutions right now",
            "find_descision_after": "Need help? We will call you very soon",
            "bot_button": "Call me",
            "enter_lk": "Enter your account",
            "password": "Password"
        };
        window.curlang = 'russian';
    </script>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/fland/css/bootstrap-grid-3.3.1.min.css" />
    <link rel="stylesheet" href="/fland/css/font.css" />
    <link rel="stylesheet" href="/fland/less/style.css" />
    <link rel="stylesheet" href="/fland/css/wiggy.css" />
    <link rel="stylesheet" href="/fland/plug/cs/css/countrySelect.css">
    <title>LegalSpace</title>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-34372260-5', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter32235704 = new Ya.Metrika({
                        id: 32235704,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) {}
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "../mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/32235704" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <style type="text/css">
        @media all and (max-width: 767px) {
            .russia{
                height: 700px !important;
            }
        }
    </style>
</head>




<?php

    $_['lang_main_page_h1_title'] = 'Решaем любые юридические  <br> Задачи по всему миру. ONLINE';
    $_['lang_main_page_h1_subtitle'] = 'Оставьте свои контакты и мы перезвоним сегодня';
    $_['lang_main_page_btn_consult'] = 'Получить бесплатную консультацию';
    
    
    $_['lang_main_page_trigers_title_1'] = 'Найти юриста легко';
    $_['lang_main_page_trigers_text_1'] = 'С нами работает более 2 000 юристов в более чем 150 странах мир';
    $_['lang_main_page_trigers_title_2'] = 'Лучшие юристы';
    $_['lang_main_page_trigers_text_2'] = 'Минимум 6 лет опыта, наличие рекомендаций и личное собеседовани';
    $_['lang_main_page_trigers_title_3'] = 'Безопасная сделка';
    $_['lang_main_page_trigers_text_3'] = 'Юрист получит деньги только после подтверждения выполнения работы';
    
    
    $_['lang_main_page_best_lawyers_h2'] = 'Найти юриста легко';
    $_['lang_main_page_best_lawyers_info_text'] = 'Зачем вам испорченный телефон, если можно говорить со специалистами без посредника';
    $_['lang_main_page_best_lawyers_sub-title'] = '<p>Мы организовываем простой поиск юристов высокого класса в реальном времени по всему миру. Вы решаете необходимые задачи без долгого ожидания и ненужной переписки, общаясь в чате сервиса Legal Space.</p>
    <p>Экономия вашего времени и денег, за счет подбора именно того юриста, который подходит для решения именно вашего вопроса. На вас работает не один специалист за огромные деньги, а целый штат со значительно меньшим бюджетом. Эта особенность делает Legal Space самым успешным в своем роде.</p>';

    $_['lang_main_page_brands_support'] = 'При поддержке';



    $_['lang_main_page_pop_service_h2'] = 'Популярные услуги';
    $_['lang_main_page_pop_service_subtitle'] = 'Вы можете позвонить в любой из наших офисов в рабочее время и мы <br> проконсультируем вас на русском или английском языке';
    $_['lang_main_page_pop_service_btn'] = 'Бесплатная консльтация';
    $_['lang_main_page_pop_service_btn_more'] = 'подробнее об услуге';

     $_['lang_main_page_pop_service_title_1'] = 'Открыть  <br> бизнес <br> в России';
     $_['lang_main_page_pop_service_text_1'] = 'Открыть свою компанию проще, чем вам кажется. Мы поможем сориентироваться в процессе, подготовить правильно документы и начать бизнес';

     $_['lang_main_page_pop_service_title_2'] = 'Открыть  <br> бизнес <br> в России';
     $_['lang_main_page_pop_service_text_2'] = 'Открыть свою компанию проще, чем вам кажется. Мы поможем сориентироваться в процессе, подготовить правильно документы и начать бизнес';

     $_['lang_main_page_pop_service_title_3'] = 'Открыть  <br> бизнес <br> в России';
     $_['lang_main_page_pop_service_text_3'] = 'Открыть свою компанию проще, чем вам кажется. Мы поможем сориентироваться в процессе, подготовить правильно документы и начать бизнес';



     $_['lang_main_page_solution_h2'] = 'мы Поможем вам <br> решить любые задачи';
     $_['lang_main_page_solution_btn'] = 'Какой-то поясняющий текст блаблабла какой-то поясняющий текст блаблабла';

    $_['lang_main_page_solution_title_startup'] = 'Стартапам';
    $_['lang_main_page_solution_text_startup'] = 'As a small business owner, I don\'t have time to <br> find excellent legal help at a price I can afford.<br>  I just want someone who understands my  <br> business and budget. Thanks, Legal Space.'; 
    $_['lang_main_page_solution_btn_startup'] = 'Начать свое дело';
    $_['lang_main_page_solution_startup_clinet'] = 'Команда <b>Bash.today</b>';



     $_['lang_main_page_solution_title_lawyer'] = 'Юристам';
     $_['lang_main_page_solution_text_lawyer'] = 'Legal Space  partners with my legal department <br> so we can find the niche help we need on an <br> ongoing basis and scale up our legal resources <br> during a major deal or litigation.';
     $_['lang_main_page_solution_btn_lawyer'] = 'Найти юриста';
     $_['lang_main_page_solution_lawyer_clinet'] = '<b>Дмитрий Зарюта</b> CEO EASY TEN';



     $_['lang_main_page_solution_title_business'] = 'Бизнесу';
     $_['lang_main_page_solution_text_business'] = 'I knew what I wanted out of my Series A, but <br> Legal Space  helped me hire a lawyer who could <br> actually get me there! ';
     $_['lang_main_page_solution_btn_business'] = 'Сократить издержки бизнеса';
     $_['lang_main_page_solution_business_clinet'] = '<b>Дмитрий Зарюта</b> CEO EASY TEN';

     

     $_['lang_main_page_activity_h2'] = 'Направления деятельности';

     $_['lang_main_page_activity_list_1'] = 'Товарные знаки и интеллектуальная собственность';
     $_['lang_main_page_activity_list_2'] = 'Недвижимость';
     $_['lang_main_page_activity_list_3'] = 'Корпоративное право и M&amp;A';
     $_['lang_main_page_activity_list_4'] = 'Частный капитал';
     $_['lang_main_page_activity_list_5'] = 'Международное налоговое и корпоративное структурирование';
     $_['lang_main_page_activity_list_6'] = 'Налоги';
     $_['lang_main_page_activity_list_7'] = 'Инвестиции';
     $_['lang_main_page_activity_list_8'] = 'Учреждение компаний';
     $_['lang_main_page_activity_list_9'] = 'Инвестиционные фонды';
     $_['lang_main_page_activity_list_10'] = 'Реструктуризации и банкротство';
     $_['lang_main_page_activity_list_11'] = 'Трудовое право';
     $_['lang_main_page_activity_list_12'] = 'Антимонопольное законодательство';


     $_['lang_main_page_offices_h2'] = 'Направления деятельности';
     $_['lang_main_page_offices_subtitle'] = 'Вы можете позвонить в любой из наших офисов в рабочее время и мы <br> проконсультируем вас на русском или английском языке';



     $_['lang_main_page_map_adr1_title'] = 'Legalspace — Милан';
     $_['lang_main_page_adr1_info'] = 'Московский офис расположен в самом центре <br> Москвы, в Москва-сити. До него довольно просто: там ходит метро. Либо на автомобиле.';
     $_['lang_main_page_adr1_adress'] = 'Виа Фриули, 51 - 20135';
     $_['lang_main_page_adr1_phone'] = '+39 029-475-96-84';

     $_['lang_main_page_map_adr2_title'] = 'Legalspace — Москва';
     $_['lang_main_page_adr2_info'] = 'Московский офис расположен в самом центре <br> Москвы, в Москва-сити. До него довольно просто: там ходит метро. Либо на автомобиле.';
     $_['lang_main_page_adr12_adress'] = 'иа Фриули, 51 - 20135';
     $_['lang_main_page_adr2_phone'] = '+39 029-475-96-84';

     $_['lang_main_page_map_adr2_title'] = 'Legalspace — Никоссия';
     $_['lang_main_page_adr2_info'] = 'Московский офис расположен в самом центре <br> Москвы, в Москва-сити. До него довольно просто: там ходит метро. Либо на автомобиле.';
     $_['lang_main_page_adr12_adress'] = 'Псарон, 2408';
     $_['lang_main_page_adr2_phone'] = '+357 230-300-16';


     $_['lang_main_page_footer_h1'] = 'Начните решение юридических <br> вопросов уже сейчас';
     $_['lang_main_page_footer_subtitle'] = 'Первая консультация бесплатна. Вы сразу поймете, что больше не будете использовать ничего другого';
     $_['lang_main_page_footer_btn'] = 'Получить бесплатную консультацию';


?>



<body>
    <header>
        <div class="block-area">

        </div>
        <div class="gradient"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="container sl">
                        <div class="select-lang">
                            
                            <!-- <ul class="lang-list">
                                <li ><a href="#">Русский</a></li>
                                <li class="active"><a href="#">English</a></li>
                            </ul> -->
                        </div>
                    </div>
                    <a href="index.html" class="logo"><img src="/fland/img/logo.png" alt="" class="img-responsive logo-head" />
                    </a>
                    <div class="text-top">
                        <h1>
             <span class="lang_main_page_h1_title">Лучшие юристы мира. ONLINE    </span>  </h1>
                        <div class="subtitle">
                     <span class="lang_main_page_h1_subtitle"><p>ВАМ НУЖЕН ЮРИСТ?</p><p>НАПИШИТЕ, ЧЕМ МЫ МОЖЕМ ВАМ ПОМОЧЬ</p></span>
                      </div>
                    </div>
                    <div class="form-block">
                        <div>

                            <div class="sel-cnt">
                                <input type="text" class="cselect-external" value="В какой стране Вам нужен юрист?" />

                            </div>

                            <button class="btn" id="form-button">
                                <span class="lang_main_page_btn_consult">Получить бесплатную консультацию</span> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
  
    <!-- Блок 2 тригеры -->
    <section class="block-trigers">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="triger-wrap"><img class="triger-icon icon-1" src="/fland/img/bl2-icon-search.png" width="71px" alt="">
                    </div>
                    <div class="triger-title"><span class="lang_main_page_trigers_title_1">Найти юриста легко</span></div>
                    <div class="triger-tex"><span class="lang_main_page_trigers_text_1">С нами работает более 2 000 юристов в более чем 150 странах мира</span></div>
                </div>
                <div class="col-sm-4">
                    <div class="triger-wrap"><img class="triger-icon icon-2" src="/fland/img/bl2-icon-best.png" width="59px" alt="">
                    </div>
                    <div class="triger-title">Лучшие юристы</div>
                    <div class="triger-tex"><span class="lang_main_page_trigers_text_2">Минимум 6 лет опыта, наличие рекомендаций и личное собеседовани</span></div>
                </div>
                <div class="col-sm-4">
                    <div class="triger-wrap"><img class="triger-icon icon-3" src="/fland/img/bl2-icon-safety.png" width="61px" alt="">
                    </div>
                    <div class="triger-title"><span class="lang_main_page_trigers_title_3">Безопасная сделка</span></div>
                    <div class="triger-tex"><span class="lang_main_page_trigers_text_3">Юрист получит деньги только после подтверждения выполнения работы</span></div>
                </div>
            </div>
        </div>
    </section>



    <section class="bl4">
        <div class="container">
            <div class="row">
                <h2 class="title">
                  <span class="lang_main_page_pop_service_h2">Стоимость услуг</span>
              </h2>
                <div class="sub-title">
                    <br>
                </div>
            </div>

        </div>
        <div class="conteiner-fluid">
            <div class="row mr0 ml0">
                <div class="col-sm-12 pl0 pr0">
                    <div class="service-wrap">
                        <div class="service-item item1 russia" style='background-image: url(/img/mos-big.jpg)'>
                            <table style="table-layout:fixed; width:100%; height:100%">
                                <tr>
                                    <td style="color:white; font-size:25px; text-align:center; width:30%;
                                    border-right:1px solid white;position:relative">
                                        <div>
                                            <span class="lang_main_page_pop_service_title_1">Создать
                                                                             компанию
                                                                             в России</span>
                                            <p style="text-align:justify; font-size:15px; width:90%">
                                                Планируете начать свой бизнес, но не уверены, что для Вас лучше: общесто с ограниченной ответственностью или статус индивидуального предпринимателя? Мы поможем найти решение, которое подходит именно Вам и зарегистрировать удобную для Вас организационно-правовую форму.
                                            </p>
                                        </div>

                                        <div id="create-company"  class="btn" style="position:absolute; bottom:20px;left:0px; width:96%">
                                            Создать компанию
                                        </div>
                                    </td>
                                    <td>    
                                        <table style="width:60%; margin:30px;color:white; font-size:22px;">
                                            <tr>
                                                <td style="vertical-align:top">Регистрация компании</td>
                                                <td style="vertical-align:top">20 000 рублей</td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:top">Открытие расчетного счета</td>
                                                <td style="vertical-align:top">12 000 рублей в месяц</td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:top">Бухгалтерское сопровождение</td>
                                                <td style="vertical-align:top">15 000 рублей в месяц</td>
                                            </tr>
                                            <tr>
                                                <td>Юридическое сопровождение</td>
                                                <td>18 000 рублей в месяц</td>
                                            </tr>
                                            <tr>
                                                <td>Подготовка корпоративного договора</td>
                                                <td>70 000 рублей</td>
                                            </tr>
                                            <tr>
                                                <td>Создание мотивационной схемы</td>
                                                <td>200 000 рублей</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>







   <section class="offices">
       <div class="container">
           <div class="row">
                <h2 class="title"><span class="lang_main_page_offices_h2">Наши офисы по всему миру</span></h2>
                <div class="sub-title"><span class="lang_main_page_offices_subtitle">Вы можете позвонить в любой из наших офисов в рабочее время и мы <br> проконсультируем вас на русском или английском языке</span></div>
            </div>
       </div>
   </section>
   
    <section class="bl6 custom-map">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 ">
                    <div class="map-mark pos1">
                        <div class="map-title">
                            <span class="lang_main_page_map_adr1_title">Legalspace — Милан</span>
                        </div>
                        <div class="text">
                            <span class="lang_main_page_adr1_info">Московский офис расположен в самом центре <br> Москвы, в Москва-сити. До него довольно просто: там ходит метро. Либо на автомобиле.</span>
                        </div>
                        <div class="info address"><span class="lang_main_page_adr1_adress">Виа Фриули, 51 - 20135</span></div>
                        <div class="info phone"><span class="lang_main_page_adr1_phone">+39 029-475-96-84</span></div>
                    </div>
                </div>
                <div class="col-sm-12 ">
                    <div class="map-mark pos2">
                        <div class="map-title">
                            <span class="lang_main_page_map_adr2_title">Legalspace — Москва</span>
                        </div>
                        <div class="text">
                            <span class="lang_main_page_adr2_info">Московский офис расположен в самом центре <br> Москвы, в Москва-сити. До него довольно просто: там ходит метро. Либо на автомобиле.</span>
                        </div>
                        <div class="info address"><span class="lang_main_page_adr2_adress">Пресненский вал, 15</span></div>
                        <div class="info phone"><span class="lang_main_page_adr2_phone">8 (800) 333-67-36</span></div>
                    </div>
                </div>
                <div class="col-sm-12 ">
                    <div class="map-mark pos3">
                        <div class="map-title">
                            <span class="lang_main_page_map_adr3_title">Legalspace — Никоссия</span>
                        </div>
                        <div class="text">
                            <span class="lang_main_page_map_adr3_info">Московский офис расположен в самом центре <br> Москвы, в Москва-сити. До него довольно просто: там ходит метро. Либо на автомобиле.</span>
                        </div>
                        <div class="info address"><span class="lang_main_page_adr3_adress">Псарон, 2408</span></div>
                        <div class="info phone"><span class="lang_main_page_adr3_phone">+357 230-300-16</span></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
    <section class="section-bot-form">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="title-text">
                        <div class="h1"><span class="lang_main_page_footer_h1">Начните решение юридических <br> вопросов уже сейчас</span></div>
                        <p><span class="lang_main_page_footer_subtitle">
                                                    Первая консультация бесплатна. Вы сразу поймете, что больше не будете использовать ничего другого</span> </p>
                    </div>
                     <div class="form-block">
                        <div>

                            <div class="sel-cnt">
                                <input type="text" id="phone" class="wiggy white" placeholder="Ваш телефон" />

                            </div>

                            <button class="btn" id="phone-button">
                                <span class="lang_main_page_footer_btn">Получить бесплатную консультацию</span> </button>
                        </div>
                    </div>
                    <div id="phone-message"></div>
                </div>
            </div>
        </div>
        
    </section>
    <footer>
        <div class="brand-bank-block">
            <div class="container">
                <div class="col-sm-6 col-sm-offset-3">
                    <a><img src="/fland/img/icon-bank-1.png" alt="" class="img-responsive" />
                    </a>
                    <a><img src="/fland/img/icon-bank-2.png" alt="" class="img-responsive" />
                    </a>
                    <a><img src="/fland/img/icon-bank-3.png" alt="" class="img-responsive" />
                    </a>
                </div>
            </div>
        </div>
        <div class="footer-bot">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="copyright-notation">
                             <b>Legal Space</b> — это безопасная и надежная виртуальная площадка, <br>
                             которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.
                            <p>
                                © 2015 Legal Space Ltd., Все права защищены </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="soc-info">
                           
                           
                            <div class="offer">Приглашаем к нам</div> <a href="/lawyer" class="custom-btn">Вы юрист?</a>                        
                            <i class="foot-icons fb"></i>
                            <i class="foot-icons twitter"></i>
                            <i class="foot-icons google"></i>
                            <i class="foot-icons linkedin"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--[if lt IE 9]>
  <script src="js/es5-shim.min.js"></script>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/html5shiv-printshiv.min.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
    <script src="js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="plug/cs/js/countrySelect.js"></script>
    <script src="/js/navigation.js"></script>
    <script src="/js/common.js"></script>
    <script src="/js/wiggy.js"></script>
    <script src="/js/net.js"></script>
    <script src="/js/jquery.vide.js"></script>
    <script src="/js/video.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        var images = {
            "colors": {
                "ALGIERS_1.png": {
                    "color": "rgb(130, 127, 122)",
                    "label": "ALGIERS"
                },
                "AMSTERDAM_1.png": {
                    "color": "rgb(91, 81, 75)",
                    "label": "AMSTERDAM"
                },
                "BEIJING_1.png": {
                    "color": "rgb(59, 50, 45)",
                    "label": "BEIJING"
                },
                "BERLIN_1.png": {
                    "color": "rgb(87, 78, 74)",
                    "label": "BERLIN"
                },
                "BUENOS AIRES_1.png": {
                    "color": "rgb(156, 158, 151)",
                    "label": "BUENOS AIRES"
                },
                "CANBERRA_1.png": {
                    "color": "rgb(110, 96, 96)",
                    "label": "CANBERRA"
                },
                "COPENHAGEN_1.png": {
                    "color": "rgb(102, 102, 102)",
                    "label": "COPENHAGEN"
                },
                "DUBAI_1.png": {
                    "color": "rgb(70, 70, 71)",
                    "label": "DUBAI"
                },
                "GENEVA_1.png": {
                    "color": "rgb(167, 163, 164)",
                    "label": "GENEVA"
                },
                "GEORGETOWN_1.png": {
                    "color": "rgb(150, 157, 161)",
                    "label": "GEORGETOWN"
                },
                "HONG KONG_1.png": {
                    "color": "rgb(73, 77, 80)",
                    "label": "HONG KONG"
                },
                "LONDON_1.png": {
                    "color": "rgb(47, 42, 42)",
                    "label": "LONDON"
                },
                "MADRID_1.png": {
                    "color": "rgb(169, 163, 158)",
                    "label": "MADRID"
                },
                "MILANO_1.png": {
                    "color": "rgb(166, 157, 155)",
                    "label": "MILANO"
                },
                "MOSCOW_1.png": {
                    "color": "rgb(171, 165, 165)",
                    "label": "MOSCOW"
                },
                "NEW DELHI_1.png": {
                    "color": "rgb(124, 138, 137)",
                    "label": "NEW DELHI"
                },
                "NEW-YORK_1.png": {
                    "color": "rgb(56, 46, 38)",
                    "label": "NEW-YORK"
                },
                "NICOSIA_1.png": {
                    "color": "rgb(224, 216, 213)",
                    "label": "NICOSIA"
                },
                "OTTAWA_1.png": {
                    "color": "rgb(165, 163, 164)",
                    "label": "OTTAWA"
                },
                "PARIS_1.png": {
                    "color": "rgb(107, 106, 101)",
                    "label": "PARIS"
                },
                "RIO DE JANEIRO_1.png": {
                    "color": "rgb(139, 126, 110)",
                    "label": "RIO DE JANEIRO"
                },
                "SEOUL_1.png": {
                    "color": "rgb(145, 137, 133)",
                    "label": "SEOUL"
                },
                "SINGAPORE_1.png": {
                    "color": "rgb(143, 135, 135)",
                    "label": "SINGAPORE"
                },
                "TOKIO_1.png": {
                    "color": "rgb(66, 61, 64)",
                    "label": "TOKIO"
                },
                "VIENNA_1.png": {
                    "color": "rgb(187, 181, 180)",
                    "label": "VIENNA"
                },
                "VIENTIANE_1.png": {
                    "color": "rgb(139, 136, 135)",
                    "label": "VIENTIANE"
                },
                "WARSAW_1.png": {
                    "color": "rgb(60, 56, 55)",
                    "label": "WARSAW"
                },
                "WASHINGTON_1.png": {
                    "color": "rgb(212, 206, 205)",
                    "label": "WASHINGTON"
                },
                "WELLINGTON_1.png": {
                    "color": "rgb(83, 78, 77)",
                    "label": "WELLINGTON"
                },
                "ZURICH_1.png": {
                    "color": "rgb(148, 137, 138)",
                    "label": "ZURICH"
                }
            },
            "sizes": {
                "ALGIERS_1.png": "size\/175+++ALGIERS_1.png",
                "AMSTERDAM_1.png": "size\/175+++AMSTERDAM_1.png",
                "BEIJING_1.png": "size\/175+++BEIJING_1.png",
                "BERLIN_1.png": "size\/175+++BERLIN_1.png",
                "BUENOS AIRES_1.png": "size\/175+++BUENOS AIRES_1.png",
                "CANBERRA_1.png": "size\/175+++CANBERRA_1.png",
                "COPENHAGEN_1.png": "size\/175+++COPENHAGEN_1.png",
                "DUBAI_1.png": "size\/175+++DUBAI_1.png",
                "GENEVA_1.png": "size\/175+++GENEVA_1.png",
                "GEORGETOWN_1.png": "size\/175+++GEORGETOWN_1.png",
                "HONG KONG_1.png": "size\/175+++HONG KONG_1.png",
                "LONDON_1.png": "size\/175+++LONDON_1.png",
                "MADRID_1.png": "size\/175+++MADRID_1.png",
                "MILANO_1.png": "size\/175+++MILANO_1.png",
                "MOSCOW_1.png": "size\/175+++MOSCOW_1.png",
                "NEW DELHI_1.png": "size\/175+++NEW DELHI_1.png",
                "NEW-YORK_1.png": "size\/175+++NEW-YORK_1.png",
                "NICOSIA_1.png": "size\/175+++NICOSIA_1.png",
                "OTTAWA_1.png": "size\/175+++OTTAWA_1.png",
                "PARIS_1.png": "size\/175+++PARIS_1.png",
                "RIO DE JANEIRO_1.png": "size\/175+++RIO DE JANEIRO_1.png",
                "SEOUL_1.png": "size\/175+++SEOUL_1.png",
                "SINGAPORE_1.png": "size\/175+++SINGAPORE_1.png",
                "TOKIO_1.png": "size\/175+++TOKIO_1.png",
                "VIENNA_1.png": "size\/175+++VIENNA_1.png",
                "VIENTIANE_1.png": "size\/175+++VIENTIANE_1.png",
                "WARSAW_1.png": "size\/175+++WARSAW_1.png",
                "WASHINGTON_1.png": "size\/175+++WASHINGTON_1.png",
                "WELLINGTON_1.png": "size\/175+++WELLINGTON_1.png",
                "ZURICH_1.png": "size\/175+++ZURICH_1.png"
            }
        };
    </script>
    <div class="popup" style="display:none">
        <input type="text" class="cselect-internal" val="В какой стране Вам нужен юрист?" name="country" />
        <input type="text" name="name" class="wiggy white" placeholder="Ваше имя" />
        <input type="text" name="email" class="wiggy white" placeholder="Email" />
        <input type="text" name="phone" class="wiggy white" placeholder="Телефон" value="" />
        <textarea class="wiggy white" name="message" placeholder="Опишите вашу задачу"></textarea>
        <div class="btn" style="width:100%">
            Send </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $('#create-company').click(function(){
                $('.cselect-external').countrySelect("selectCountry", 'ru');
                $('body').animate({
                    scrollTop:$('.sel-cnt').offset().top-100
                }, 1000);
            })
        })
    </script>
<?php $this->view('jivosite'); ?>
</body>





</html>
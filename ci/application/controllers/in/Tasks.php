<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!User::$id)
			header('Location: /login');
		$this->load->helper('task');

	}
	public function index(){
		
		$this->load->view('in/task-list', array(
			'tasks' => Task::getList(User::$id)
		));
	}
	public function edit(){
		
		$id = $_GET['id'];
		$task = Task::get($id);

		if(!$task)
			header('Location: /in/tasks');
		$this->load->view('in/task-form', array(
			'task' => $task,
			'task_id' => $id
		));
	}
	public function cancel(){
		
		$id = $_GET['id'];
		$task = Task::cancel($id);

		header('Location: /in/tasks');
		
	}
	public function add(){
		
		
		$task = array(
			'desc' => '',
			'from' => 50000,
			'to' => 1000000,
			'status' => 'new'
		);
		$this->load->view('in/task-form', array(
			'task' => $task
		));
	}
	public function save(){
		if(!isset($_POST['task_id'])) header('Location: /in/tasks');
		if(!isset($_POST['task'])) header('Location: /in/tasks');
		
		$task = $_POST['task'];
		Task::update($_POST['task_id'], $task);
		header('Location: /in/tasks/edit?id=' . $_POST['task_id']);
	}
	public function insert(){
		if(!isset($_POST['task'])) header('Location: /in/tasks');
		Task::add($_POST['task'], User::$id);
		header('Location: /in/tasks/');	
	}

}

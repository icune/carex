<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!User::$id)
			header('Location: /login');
	}
	public function index(){
		$this->load->view('in/profile', array());
	}
	public function update(){
		$this->load->database();
		$updates = $_POST;
		foreach($updates as $table => $upd){
			if($table != 'user' && $table != 'user_info')
				continue;
			if($table == 'user')
				$where = array('id' => User::$id);
			else
				$where = array('user_id' => User::$id);
			$this->db->update($table, $upd, $where);
		}

		if(isset($_POST['redirect']))
			header('Location: '.$_POST['redirect']);
		else
			header('Location: /in/profile');	
	}

}

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Переименуй меня</title>
	<script src="/js/jquery-2.1.4.js"></script>
	<script type="text/javascript">
		window.changes = 0;
		window.file = 0;
		$(function(){
			$('#sel').change(function(){
				if($(this).val() == 'no') return;
				if(changes)
					if(confirm('Ты что-то изменил. Точно сохранить не хочешь?')){
						load($(this).val(), render);
						file  = $(this).val();
					}
					else;
				else{
					load($(this).val(), render);
					file  = $(this).val();
				}
			});
			$('#save').click(function(){
				if(!changes){
					alert('Ничего не изменено');
					return;
				}
				save(file, grab(), function(r){
					if(r.status != 'ok'){
						alert('Произошла ошибка, жмакни кпоку еще раз');
						return;
					}
					changes = 0;
					load(file, render);
					$('#sel').removeAttr('disabled');
				})
			});
		});
	</script>
	<style type="text/css">
		.tar{
			width: 100%;
			height: 100%;
		}	
		.tar.changed{
			background-color: #7DD4A0;
		}
		.all table{
			width: 100%;
			table-layout: fixed;
		}
		.all table tr td:first-child{
			width: 200px;
		}
	</style>
</head>
<body>
	<select name="" id="sel">
		<option value="no">
			Не выбрано
		</option>
		<option value="land_cyprus_lang.php">
			Кипр
		</option>
		<option value="land_land_lang.php">
			Главный
		</option>
		<option value="land_lawyer_lang.php">
			АДВОКАТ!
		</option>
		<option value="land_rus_lang.php">
			Россия	
		</option>
		<option value="land_tm_lang.php">
			Марка
		</option>
		<option value="land_how_lang.php">
			Как?
		</option>
	</select>
	<button id="save">Сохранить</button>
	<br><br>
	Напоминаю синтаксис: <b>Лошадь </b> скачет = &lt;b&gt;Лошадь &lt;b&gt; скачет<br>
	Опа<br>опа-па = Опа&lt;br&gt;опа-па ИЛИ &lt;p&gt;Опа&lt;/p&gt;&lt;p&gt;опа-па&lt;/p&gt;
	<br><br>

	<div class="all">
		
	</div>
	<script type="text/javascript">
		function load(file, done){
			$.post('/admin/rus/load', {file:file}, function(r){
				done(r.msg);
			}, 'json');
		}
		function save(file, content, done){
			$.post('/admin/rus/save', {file:file, content:content}, function(r){
				done(r);
			}, 'json');
		}
		function render(els){
			var all = $('.all');
			all.children().remove();
			var table = $('<table>').appendTo(all);
			for(var k in els){
				var tr = $('<tr>').appendTo(table);
				var td = $('<td>').appendTo(tr);
				var area = $('<textarea class="tar">').appendTo(td);
				area.attr('key', k);
				area.text(els[k]);
				area.data('was', els[k]);
				area.keyup(function(){
					if($(this).val() != $(this).data('was')){
						$(this).addClass('changed');
						changes = true;
						$('#sel').attr('disabled', 'disabled');
					}
					else{
						$(this).removeClass('changed');
					}
				})
			}
		}
		function grab(){
			var r = {};
			$('textarea').each(function(){
				r[$(this).attr('key')] = $(this).val();
			})
			return r;
		}
	</script>
</body>
</html>
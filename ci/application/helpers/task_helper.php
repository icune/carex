<?php

final class Task{
	private static $db = null;
	private static $loader;
	public static $image_c = '';
	public static function init(){
		$loader = new CI_Loader();
		$loader->helper('chat');
		self::$db = $loader->database('', true);
		self::$loader = $loader;
	}
	public static function add($task, $user_id){
		if(!$user_id) throw new Exception('No user_id');
		if(!$task) $task = array();
		if(!isset($task['desc'])) $task['desc'] = '';
		if(!isset($task['from'])) $task['from'] = '100000';
		if(!isset($task['to'])) $task['to'] = '1000000';
		$task['user_id'] = $user_id;
		self::$db->insert('task', $task);
		return self::$db->insert_id();
	}
	public static function update($task_id, $task){
		if(!$task_id) throw new Exception('No task_id');
		self::$db->update('task', $task, array(
			'id' => $task_id
		));
	}
	public static function getList($user_id){
		$sql = "SELECT *, uie.name expert_name, 
		uie.surname expert_surname, uie.patronym expert_patronym FROM task t ";
		$sql .= "JOIN user_info ui ON ui.user_id = t.user_id ";
		$sql .= "LEFT OUTER JOIN user_info uie ON uie.user_id = t.expert_id ";
		$sql .= " WHERE t.user_id=" . self::$db->escape($user_id);

		$r = self::$db->query($sql)->result_array();
		return $r;
	}
	public static function get($task_id){
		$sql = "SELECT * FROM task WHERE id=" . self::$db->escape($task_id);
		$r = self::$db->query($sql)->result_array();
		if(!count($r))
			return null;
		return $r[0];
	}
	public static function cancel($task_id){
		$sql = "DELETE FROM task WHERE id=" . self::$db->escape($task_id);
		self::$db->query($sql);
	}
}
Task::init();
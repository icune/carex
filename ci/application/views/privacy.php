<center>
<div style="width:80%; text-align:left">
<h1>
Privacy Policy
</h1>


<b>
What personal information do we collect from the people that visit our blog, website or app?
</b> <br> <br>
When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, credit card information or other details to help you with your experience.
<br> <br><b>
When do we collect information?
</b> <br> <br>
We collect information from you when you register on our site, fill out a form or enter information on our site.
<br> <br><b>
How do we use your information?
</b> <br> <br>
We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
      • To quickly process your transactions.
<br> <br><b>
Third Party Disclosure
</b> <br> <br>
We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide you with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property, or safety. 

However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.
<br> <br><b>
Third party links
</b> <br> <br>
Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.

Changes to this Policy. We may update this privacy policy to reflect changes to our information practices. If we make any material changes a notice will be posted on this page along with the updated Privacy Policy prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.

<br> <br><b>
Contacting Us
</b> <br> <br>
If there are any questions regarding this privacy policy, you may contact us using the information below.
Legalspace.world
50/2 Aviamotornaya St., Apt. 56, Moscow 111024
m.ovsyanikov@legalspace.world
<br>
Last Edited on 2015-07-09

</div>
</center>
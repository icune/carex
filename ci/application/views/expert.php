<?php $this->view('header'); ?>

  




   
    <section class="exper-profile">
        <div class="container">
         
                <div class="row exper-main-info">
                
                        <div class="col-md-4 col-md-offset-1 col-sm-6">
                            <div class="expert-photo" style="background: url(<?php echo $expert['user_info']['image']; ?>) no-repeat center;background-size: cover;"></div>
                            <div class="exp-rate-box">
                        <div class="exp-rate-title">Рейтинг:</div> 
                        <ul class="exp-rate">
	                        <?php 
                                for($i = 0; $i < 5; $i++){
                                    if(intval($expert['expert']['rating']) > $i)
                                        echo '<li><i class="fa fa-star"></i></li>';
                                    else
                                        echo '<li><i class="fa fa-star fa-star-o"></i></li>';
                                }
                             ?>
	                    </ul>
                       
                    </div>
                            
                        </div>
                        
                        <div class="col-md-6 col-md-offset-1 col-sm-6">
                           <p class="pull-left btn-left-sidebar visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
          </p>
                            <h3>
                                <?php echo $expert['user_info']['name']; ?> <?php echo $expert['user_info']['surname']; ?>
                            </h3>
                            <div class="row exp-header">
               
                            <div class="exp-spec">
                                
                                <p>
                                    <b style="color:black">Специализация</b>: 
                                    <?php 
                                        $sp = array();
                                        $sp_i = explode(',', $expert['expert']['specializations']);
                                        foreach($sp_i as $i)
                                            foreach($specializations as $spec)
                                                if($spec['id'] == $i)
                                                    $sp []= $spec['name'];
                                        echo implode(', ', $sp);
                                    ?>
                                </p>
                                <p>
                                <?php if($expert['expert']['checked']): ?>
                                    Данные проверены <i class="fa fa-check"></i>
                                <?php endif; ?>
                                </p>

                            </div>
                             <div class="exp-adress">
                            <b>Расположение:</b> <?php echo $expert['user_info']['city']; ?>
                        </div>
                        <div class="exp-condition"><b>Готов к выезду:</b>
                       <?php echo $expert['expert']['departure']; ?> </div>
                        </div>
                         <a href="#" class="btn btn-default btn-effect" data-toggle="modal" data-target="#myModal-quest"> Связаться </a>
                    </div>
                
                
            </div>
            
            
            
            <div class="row exp-work-conds">
                
                <div class="col-sm-6 ">
                    <h4 class="exp-block-title">
                        Условия работы:
                    </h4>
                   <div class="work-cond">
                        <?php echo $expert['expert']['work_conditions']; ?>
                    </div>
                </div>
                
                <div class="col-sm-6">
                   <h4 class="exp-block-title">Опыт</h4>
                    <div class="skill">
                        <?php echo $expert['expert']['experience']; ?>
                    </div>
                </div>
                
            </div>
            <div class="row exp-service-cost">
                <div class="col-sm-12">
                    <h4 class="exp-block-title">
                        Cтоимость услуг
                    </h4>
                </div>
                
                <table class="table table-striped exp-cost-table">
            <?php foreach($expert['services'] as $serv): ?>
                <tr>
                    <td><?php echo $serv['name']; ?></td>
                    <td><?php echo $serv['price']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
            </div>
            
            
            
        </div>
        
        
    </section>

      <!-- Безопасная сделка-->
    <div class="modal fade" id="myModal-safe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Безопасная сделка</h4>
                </div>
                <div class="modal-body">
                    <p>Безопасная сделка - специальная услуга нашего сервиса по взаимодействию клиентов и автоэкспертов, где наш сервис выступает в роли независимой третьей стороны и гарантирует выполнение обязательств с обеих сторон. </p>

                    <p>Данная услуга позволяет оплатить услуги автоэксперта только после их выполнения (никаких авансов) и также данная услуга даёт уверенность эксперту,что его услуги будут оплачены.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Зарегистрироваться</button>

                </div>
            </div>
        </div>
    </div>
      
       <!-- Вопрос эксперту -->
<div class="modal fade" id="myModal-quest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Задайте вопрос эксперту </h4>
        
      </div>
      <div class="modal-body">
        <div class="exp-login exp-question" >
            <input class="exp-name" type="hidden" value="<?php echo $expert['user_info']['name']; ?> <?php echo $expert['user_info']['surname']; ?> <?php echo  $_GET['id']; ?>">
            <input class="name" type="text" placeholder="Имя *" required>
            <input class="phone" type="text" placeholder="Телефон *" required>
            <input class="email" type="text" placeholder="Email">
            <textarea class="meassage"  cols="30" rows="5" placeholder="Вопрос"></textarea>
            <button class="btn btn-default send-q">Отправить</button>
            
        </div>
      </div>
     
        
        
      </div>
    </div>
  </div>    
       
<?php $this->view('footer'); ?>
<?php 

final class Mail{
	private static $lang = null, $db = null, $loader = null;
	public static function init(){
		$loader = new CI_Loader();
		self::$lang = $loader->language('mail');
		self::$lang = get_instance()->lang->language;
		$loader->helper('smtp');

		self::$db = $loader->database('', true);
		self::$loader = $loader;
	}
	public static function smtp($email, $message){
		if(!preg_match('!^.+@.+\..+$!', $email)) throw new Exception('wrong email: ' . $email);

		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		try {
		  $mail->Host       = 'smtp.mail.ru';
		  $mail->SMTPDebug  = 0;
		  $mail->setLanguage('ru');
		  $mail->CharSet  = 'UTF-8';
		  $mail->SMTPAuth   = true;
		  $mail->Port       = 25;
		  $mail->Username   = 'grand-auto-expert@mail.ru';
		  $mail->Password   = 'gae5gae+';
		  // $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
		  $mail->AddAddress($email);                //кому письмо
		  $mail->SetFrom('grand-auto-expert@mail.ru', 'Ваш Авто Эксперт'); //от кого (желательно указывать свой реальный e-mail на используемом SMTP сервере
		  // $mail->AddReplyTo($__smtp['addreply'], $__smtp['username']);
		  $mail->Subject = htmlspecialchars('Carex');
		  // $mail->AddEmbeddedImage( rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/img/logo-round-black-mail.png', 'lsp_image');
		  $mail->MsgHTML($message);
		  
		  $mail->Send();
		  
		} catch (phpmailerException $e) {
		  echo $e->errorMessage();
		} catch (Exception $e) {
		  echo $e->getMessage();
		}

		return true;
	}
	public static function send($email, $what, $p = array()){
		$letter = self::what($what);
		$short_description = $letter['short_description'];
		$text = $letter['text'];
		foreach($p as $k => $v)
			$text = str_replace("%$k%", $v, $text);
		$html = file_get_contents(Info::$rootDir . '/mail.html');
		foreach($p as $k => $v)
			$html = str_replace("%$k%", $v, $html);
		$html = str_replace("%text%", $text, $html);
		$html = str_replace("%short_description%", $short_description, $html);
		self::smtp($email, $html);
	}
	private static function what($what){
		$email = self::$db->query("SELECT * FROM email WHERE what='$what'")->result_array();
		if(!count($email))
			throw new Exception("No what '$what'");
		return $email[0];
	}
}
Mail::init();
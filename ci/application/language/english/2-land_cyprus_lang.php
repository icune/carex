<?php
$lang['russkii'] = "Русский";
$lang['imja_i_familija'] = "Имя и фамилия";
$lang['telefon'] = "Телефон";
$lang['konsultacija_'] = "Консультация ";
$lang['vash_telefon'] = "Ваш телефон";
$lang['otzyvy_'] = "Отзывы ";
$lang['kalkuljator_'] = "Калькулятор ";
$lang['kak_vas_zovut'] = "Как вас зовут?";
$lang['priglashaem_k_nam'] = "Приглашаем к нам";
$lang['vy_urist'] = "Вы юрист?";
$lang['evro'] = "евро";
$lang['sozdanie_kompanii'] = "Создание компании";
$lang['nominalnyi_direktor'] = "Номинальный директор";
$lang['god'] = "год";
$lang['nominalnyi_akcioner'] = "Номинальный акционер";
$lang['audit'] = "Аудит";
$lang['buhuczet'] = "Бухучет";
$lang['service_page_main_title'] = "УЧРЕЖДЕНИЕ кипрской КОМПАНИИ";
$lang['service_page_main_text'] = "
                          Вы занимаетесь строительным бизнесом или реализуете IT проект? Всегда находитесь в РФ или постоянно путешествуете? Пытаетесь защитить активы или структурировать отношения с партнерами? Хотите избежать двойного налогообложения? Мы подберем решение, которое нужно именно Вам.

                        ";
$lang['service_page_main_btn'] = "Отправить запрос";
$lang['service_page_advantages_title'] = "ПРЕИМУЩЕСТВА НАШИХ УСЛУГ";
$lang['service_page_advantages_sub-title'] = "Планируете приобрести компанию на Кипре?";
$lang['service_page_advantages_adv1-title'] = "Конфиденциальность";
$lang['service_page_advantages_adv1-text'] = "Все Ваши документы хранятся в офисе Legal Space на Кипре";
$lang['service_page_advantages_adv2-title'] = "ОПЛАТА ПО РЕЗУЛЬТАТУ";
$lang['service_page_advantages_adv2-text'] = "Вы осуществляете денежный перевод только когда работа выполнена в полном объеме";
$lang['service_page_advantages_adv3-title'] = "ИНДИВИДУАЛЬНЫЙ ПОДХОД";
$lang['service_page_advantages_adv3-text'] = "Заказывая услуги у нас, вы получаете безупречный сервис, плюс — персонального менеджереа";
$lang['service_page_advantages_adv4-title'] = "ON-LINE СОПРОВОЖДЕНИЕ";
$lang['service_page_advantages_adv4-text'] = "Совершайте все необходимые операции и ведите документооборот прямо на нашем сайте";
$lang['service_page_advantages_adv5-title'] = "ЛУЧШИЕ ЦЕНЫ";
$lang['service_page_advantages_adv5-text'] = "Работа on-line позволяет нам избежать крупных издержек обычных юридических компании";
$lang['service_page_advantages_adv6-title'] = "МЫ РАБОТАЕМ ПО ВЫХОДНЫМ";
$lang['service_page_advantages_adv6-text'] = "Предпочитаете зарабатывать, пока конкуренты отдыхают? Значит мы с Вами за одно!";
$lang['service_page_consult_title'] = "Нужна консультация?";
$lang['service_page_consult_sub-title'] = "Мы перезвоним Вам в течение 5 минут";
$lang['service_page_consult_btn'] = "отправить запрос";
$lang['service_testimanl1_name'] = "ВАСИЛИЙ ВОРОПАЕВ";
$lang['service_testimanl1_company'] = "CEO RUBRAIN";
$lang['service_testimanl1_review'] = "\"Нашей компании нужно было в срочном порядке решить проблему со счетом в американском банке. Благодаря LegalSpace, мы быстро нашли юриста в Нью Йорке и подготовили необходимые документы. Спасибо!\"";
$lang['service_page_calc_title'] = "Регистрация ооо на Кипре";
$lang['service_page_calc_sub_title'] = " ";
$lang['service_page_calc_final'] = "ИТОГО:";
$lang['service_page_calc_ready'] = "Документы будут готовы <br> через <b class=\"over-days\">15 дней</b>";
$lang['service_page_calc_week_day1'] = "ПН";
$lang['service_page_calc_week_day2'] = "ВТ";
$lang['service_page_calc_week_day3'] = "СР";
$lang['service_page_calc_week_day4'] = "ЧТ";
$lang['service_page_calc_week_day5'] = "ПТ";
$lang['service_page_calc_week_day6'] = "СБ";
$lang['service_page_calc_week_day7'] = "ВС";
$lang['service_page_calc_fina_btn'] = "Зарегистрировать компанию";
$lang['service_page_register_title'] = "Хотите зарегистрировать компанию? ";
$lang['service_page_register_sub_title'] = "Получите первую консультацию абсолютно бесплатно";
$lang['service_page_register_btn'] = "Отправить запрос";
$lang['service_page_faq_title'] = "F.A.Q.";
$lang['service_page_faq_sub_title'] = "Ответы на популярные вопросы";
$lang['service_page_faq_q1_title'] = "Какие документы я получу при создании компании?";
$lang['service_page_q1_text'] = "От Вас требуются всего несколько документов (внутренний 
                        и заграничный паспорт, письмо от Вашего банка и квитанцию
                        об оплате ЖКХ). Дальше действовать будем мы.";
$lang['service_page_faq_q1_text'] = "Вам достаточно позвонить или написать сообщение персональному менеджеру. Он подскажет Вам, какие действия и документы требуются для осуществления той или иной операции необходимой для Вашего бизнеса.";
$lang['service_page_faq_q5_text1'] = "Вам будут переданы следующие документы*: ";
$lang['service_page_faq_q5_list1'] = "- меморандум и устав компании;";
$lang['service_page_faq_q5_list2'] = "- копия реестра акционеров; ";
$lang['service_page_faq_q5_list3'] = "- сертификат акций, подтверждающий владение акциями компании; ";
$lang['service_page_faq_q5_list4'] = "- свидетельство об учреждении компании;";
$lang['service_page_faq_q5_list5'] = "- копия реестра директоров.";
$lang['service_page_faq_q5_text2'] = "*перечень может меняться в зависимости от Вашей ситуации и пожеланий.";
$lang['main_page_offices_h2'] = "Наши офисы по всему миру";
$lang['main_page_offices_subtitle'] = "Вы можете позвонить в любой из наших офисов в рабочее время и мы <br> проконсультируем вас на русском или английском языке";
$lang['main_page_map_adr1_title'] = "Legalspace — Милан";
$lang['main_page_adr1_info'] = "
                            

                            ";
$lang['main_page_adr1_adress'] = "Виа Фриули, 51 - 20135";
$lang['main_page_adr1_phone'] = "+39 029-475-96-84";
$lang['main_page_map_adr2_title'] = "Legalspace — Москва";
$lang['main_page_adr2_adress'] = "Пресненский вал, 15";
$lang['main_page_adr2_phone'] = "8 (800) 333-67-36";
$lang['main_page_map_adr3_title'] = "Legalspace — Никоссия";
$lang['main_page_adr3_adress'] = "Псарон, 2408";
$lang['main_page_adr3_phone'] = "+357 230-300-16";
$lang['service_page_anyquestion_title'] = "Остались вопросы?";
$lang['service_page_anyquestion_sub_title'] = "Оставьте свой номер и мы Вам перезвоним";
$lang['service_page_anyquestion_btn'] = "ПЕРЕЗВОНИТЕ МНЕ";
$lang['footer_info'] = " <b>Legal Space</b> — это безопасная и надежная виртуальная площадка,
                                                       <br> которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.";
$lang['footer_copyright'] = "© 2015 Legal Space Ltd., All rights reserved";

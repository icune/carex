<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />


<head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="favicon.png" />
    <meta charset="UTF-8" />
    <script type="text/javascript">
        window.lang = {
            "are_you": "For lawyers",
            "title": "Legal space",
            "best_juri_online": "Best lawyers worldwide. Online",
            "bla_bla_before_form": "Fill in the details and find your lawyer",
            "get_free_consult": "Get free advice",
            "where_you": "Choose a country",
            "desc_problem": "Describe your legal matter",
            "send": "Send",
            "footer_blabla": "\u00a9 Copyright 2015. Legal Space",
            "rights": "All rights reserved",
            "moscow": "Moscow",
            "moscow_addr": "15 Presnensky val",
            "milan": "Milan",
            "milan_addr": "Via Friuli, 51 - 20135",
            "nikosia": "Nikosia",
            "nikosia_addr": "Psaron , 2408 ",
            "from_where_you": "Where are you from?",
            "name": "Your name",
            "phone": "Contact phone number",
            "success": "Thank you for your request! We\u2019re on your case.",
            "error": "Something went wrong. Please contact us and we will help: info@legalspace.world",
            "services": ["Trademarks and intellectual property", "International tax and corporate structuring", "Corporate M&A", "Private equity", "Real estate", "Tax", "Investments", "Company formation ", "Investment funds", "Restructuring and insolvency", "Labour law", "Competition and antitrust", "Banking and finance", "Litigation & dispute resolution", "Private wealth and succession planning"],
            "bla_1": "<p>LEGAL SPACE is a safe and reliable virtual resource to help you resolve your legal issues online. Whether you are setting up a company in Cyprus, registering a trademark, or applying for residential rights abroad - whatever legal service you need, in whichever part of the world, you can find it right now on our website. <\/p><p>Every LEGAL SPACE lawyer passes through a highly competitive selection process, which is why we are sure of his or her qualifications and professionalism and can guarantee the quality of his or her services.<\/p>",
            "bla_2": "<p>All of our lawyers have a minimum of five years professional experience. Every candidate undergoes a personal interview before they start work, while lawyers may only join the LEGAL SPACE professional pool through recommendation.<\/p><p> Our partners are outstanding representatives of the legal profession; they are progressive and always abreast of the latest developments in the industry. Together we are changing the world of legal services for the better. <\/p>",
            "we_connect": "We connect you with the best lawyers DIRECTLY",
            "spoiled_phone": "Our comprehensive geographical database allows us to solve your issue in any country.",
            "with_support": "With support",
            "globe_under": "countries",
            "hat_under": "lawyers",
            "case_under": "clients",
            "office_title": "Our offices:",
            "office_desc": "Call us and we will help",
            "moscow_desc": "Moscow office is located in the very centre of Moscow, 5 minutes walk from the metro Belorusskaya. There is the parking for the clients.",
            "milan_desc": "Our office in Milan is located in the very centre of Milan, 10 minutes walk from the metro Lodi.\r\n",
            "nikosia_desc": "Our office in Cyprus is conveniently located near the airport.\r\n",
            "find_descision": "Find legal solutions right now",
            "find_descision_after": "Need help? We will call you very soon",
            "bot_button": "Call me",
            "enter_lk": "Enter your account",
            "password": "Password"
        };
        window.curlang = '<?php echo $curlang; ?>';
        window.lawyer = true;
    </script>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/fland/css/bootstrap-grid-3.3.1.min.css" />
    <link rel="stylesheet" href="/fland/css/font.css" />
    <link rel="stylesheet" href="/fland/less/style.css" />
    <link rel="stylesheet" href="/css/mark.css" />
    <link rel="stylesheet" href="/fland/css/wiggy.css" />
    <link rel="stylesheet" href="/fland/plug/cs/css/countrySelect.css">
    <title>LegalSpace</title>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-34372260-5', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter32235704 = new Ya.Metrika({
                        id: 32235704,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) {}
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "../mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/32235704" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <script src="/js/jquery-2.1.4.js"></script>
	<script src="/js/facebook.js"></script>
</head>


<body>
<div id="eba"></div>
<link rel="stylesheet" href="/css/modal-lp.css">
<div class="modal wiggy"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display : none">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content land-modal">
        <div class="modal-content land-modal">
         <div class="modal-title">
          <span class="text"></span>
          <div class="modal-close" data-dismiss="modal" aria-label="Close"></div>
         </div>

          <div class="modal-data">
          
          </div>
    </div>    
     
    </div>
  </div>
</div>

    <?php $this->view('header'); ?>
    <header>
        <a href="/login" style="
            position: absolute;
            color: white;
            left: 10px;
            top: 14px;
            z-index: 200;
        ">
        <?php if($curlang == 'english'): ?>
            Login
        <?php else: ?>
            Вход
        <?php endif; ?>
        </a>
        <div class="block-area">

        </div>
        <div class="gradient"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="container sl">
                        <div class="select-lang">
                            
                            <ul class="lang-list">
                                <li 
                                <?php $href = '/lang/ru/'; if($curlang == 'russian'){ echo 'class="active"'; $href = '';}; ?>
                                ><a href="<?php echo $href; ?>">Русский</a></li>
                                <li 
                                <?php $href = '/lang/en/'; if($curlang == 'english'){ echo 'class="active"'; $href = '';}; ?>
                                ><a href="<?php echo $href; ?>">English</a></li>
                            </ul>
                        </div>
                    </div>
                    <a href="/" class="logo"><img src="/fland/img/logo.png" alt="" class="img-responsive logo-head" />
                    </a>
                    <div class="text-top">
                        <h1>
             <?php echo $_["laywer_page_h1_title"]; ?>      </h1>
                        <span class="subtitle">
                     <?php echo $_["laywer_page_h1_subtitle"]; ?>
                      </span>
                    </div>
                    <?php $this->view('land-form', array(
                      'lawyer' => true
                    )); ?>
                </div>
            </div>
        </div>
    </header>


    <!-- <?php echo $_["blok_"]; ?>2 <?php echo $_["trigery_"]; ?>-->
    <section class="block-trigers page-lawyer">
        <div class="container">
           <h2 class="title">
               <?php echo $_["laywer_page_advantage_h2"]; ?>
           </h2>
            <div class="row">
                <div class="col-sm-3">
                    <div class="triger-wrap"><img class="triger-icon icon-1" src="/fland/img/bl2-law-goods1.png" width="84px" alt="">
                    </div>
                    
                    <div class="triger-tex"><?php echo $_["laywer_page_advantage_1"]; ?></div>
                </div>
                <div class="col-sm-3">
                    <div class="triger-wrap"><img class="triger-icon icon-2" src="/fland/img/bl2-law-goods2.png" width="67px" alt="">
                    </div>
                   
                    <div class="triger-tex"> <?php echo $_["laywer_page_advantage_2"]; ?></div>
                </div>
                <div class="col-sm-3">
                    <div class="triger-wrap"><img class="triger-icon icon-3" src="/fland/img/bl2-law-goods3.png" width="82px" alt="">
                    </div>
                    
                    <div class="triger-tex"><?php echo $_["laywer_page_advantage_3"]; ?>   </div>
                </div>
                <div class="col-sm-3">
                    <div class="triger-wrap"><img class="triger-icon icon-3" src="/fland/img/bl2-law-goods4.png" width="62px" alt="">
                    </div>
                    
                    <div class="triger-tex"><?php echo $_["laywer_page_advantage_4"]; ?> </div>
                </div>
            </div>
        </div>
    </section>


    <!-- <?php echo $_["kak_eto_rabotaet_"]; ?>-->
    <section class="howitworks">
        <div class="container">
            <div class="row">
                <h2 class="title">
                    <?php echo $_["laywer_page_howitworks_h2"]; ?>
                </h2>
                
                <div class="col-sm-4">
                    <div class="item">
                        <div class="img-wrap">    
                            <img src="/fland/img/page-lawyer/howit-icon1.png" width="83px" alt="" class="icon-howiw">
                        </div>
                        <div class="item-title"><?php echo $_["laywer_page_howitworks_item_1_title"]; ?></div>
                        <div class="item-text"><?php echo $_["laywer_page_howitworks_item_1_text"]; ?></div>
                        <div class="item-step">1</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <div class="img-wrap">    
                            <img src="/fland/img/page-lawyer/howit-icon2.png" width="67px" alt="" class="icon-howiw">
                        </div>
                        <div class="item-title"><?php echo $_["laywer_page_howitworks_item_2_title"]; ?></div>
                        <div class="item-text"><?php echo $_["vy_sami_reshaete"]; ?>, <?php echo $_["hotite_li_vy_rabotat_s_konkretnym_klientom___"]; ?></div>
                        <div class="item-step">2</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <div class="img-wrap">    
                            <img src="/fland/img/page-lawyer/howit-icon3.png" width="61px" alt="" class="icon-howiw">
                        </div>
                        <div class="item-title"><?php echo $_["laywer_page_howitworks_item_3_title"]; ?></div>
                        <div class="item-text"><?php echo $_["laywer_page_howitworks_item_3_text"]; ?> </div>
                        <div class="item-step">3</div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

   


    <section class="require">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="text-title"><?php echo $_["laywer_page_requirements_title"]; ?></div>
                    <div class="text">
                    <ul class="req-list">
                        <li><?php echo $_["laywer_page_requirements_list_1"]; ?> </li>
                        <li><?php echo $_["laywer_page_requirements_list_2"]; ?></li>
                        <li><?php echo $_["laywer_page_requirements_list_3"]; ?></li>
                        
                        <li><?php echo $_["laywer_page_requirements_list_4"]; ?></li>
                    </ul>
                    </div>
                    <a  class="custom-btn" onclick="$('body').animate({scrollTop:0}, 1000);$('#shit').modal();" style="cursor:pointer">
                        <?php echo $_["laywer_page_requirements_btn"]; ?>
                    </a>
                </div>
                <div class="col-sm-5 col-sm-offset-1">
                    
                </div>
            </div>
        </div>
    </section>
  
  
   
    <footer class="page-lawyer">
       
        <div class="footer-bot ">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="copyright-notation">
                             <?php echo $_["footer_info"]; ?>
                            <p>
                                <?php echo $_["footer_copyright"]; ?> </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="soc-info">
                           
                           
                            <div class="offer" style="opacity:0"><?php echo $_["priglashaem_k_nam"]; ?></div> <a style="opacity:0" href="#" class="custom-btn"><?php echo $_["vy_urist"]; ?></a>                        
                            <a target="_blank" href="https://www.facebook.com/Legal-Space-468707986637000/timeline/"><i class="foot-icons fb"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/company/9340358?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9340358%2Cidx%3A1-1-1%2CtarId%3A1441991163447%2Ctas%3Alegal%20space"><i class="foot-icons linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--[if lt IE 9]>
  <script src="js/es5-shim.min.js"></script>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/html5shiv-printshiv.min.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
    
    <script type="text/javascript" src="plug/cs/js/countrySelect.js"></script>
    <script src="/js/navigation.js"></script>
    <script src="/js/common.js"></script>
    <script src="/js/wiggy.js"></script>
    <script src="/js/net.js"></script>
    <script src="/js/lawyer.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        var images = <?php echo json_encode($files, true); ?>
    </script>
    <div class="popup" style="display:none">
        <input type="text" class="cselect-internal" val="<?php echo $_["vyberite_stranu"]; ?>" name="country" />
        <input type="text" name="name" class="wiggy white" placeholder="<?php echo $_["vashe_imja"]; ?>" />
        <input type="text" name="email" class="wiggy white" placeholder="Email" />
        <input type="text" name="phone" class="wiggy white" placeholder="<?php echo $_["vash_telefon"]; ?>" value="" />
        <textarea class="wiggy white" name="message" placeholder="<?php echo $_["opishite_vashu_zadaczu"]; ?>"></textarea>
        <div class="btn" style="width:100%">
            <?php echo $_["otpravit_"]; ?></div>
    </div>

<link rel="stylesheet" href="/css/cab/bootstrap.css">    
<link rel="stylesheet" href="/css/modal-lp.css">
<link rel="stylesheet" href="/css/modal-lp.css">
<script src="/js/cab/bootstrap.min.js"></script>
<div class="modal" id="shit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display : none">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content land-modal">
        <div class="modal-content land-modal">
        <div class="modal-title">
         <?php echo $_['form_register']; ?>
         <div class="modal-close" data-dismiss="modal" aria-label="Close"></div>
        </div>
		<div class="modal-title soc_block">
			<div class="soa_auth">
				<?if(Info::$host=='legalspace.world'){?>
					<a class="soc_auth_ref" href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77sd4z4o6hq27d&redirect_uri=http://<?=Info::$host?>/u/login/linkedin/1&state=987654321&scope=r_emailaddress">Регистрация через Linkedin</a>
				<?}
				else{
					?>
					<a class="soc_auth_ref" href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77od03o5igspcg&redirect_uri=http://<?=Info::$host?>/u/login/linkedin/1&state=987654321&scope=r_emailaddress">Регистрация через Linkedin</a>
					<?
				}?>
				<?/*<a class="soc_auth_ref" href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77sir03afdziyx&redirect_uri=http://dev.legalspace.world/u/login/linkedin/1&state=987654321&scope=r_emailaddress">Регистрация через Linkedin</a>*/?>
				<a class="soc_auth_ref soc_auth_ref2" href="#">Регистрация через email</a>
				<?/*<a href="#" id="fb-account"><img src="/img/soc_fb.png" alt="" /></a>*/?>
			</div>
		</div>
          <div class="modal-data email_reg_block" style="display:none;">
              <form action="/lawyer/reg" id="shit-form">

                  <input class="round-top usr_f_name" type="text" name="user[name]" placeholder="<?php echo $_['form_name']; ?>">
                  <input type="text" name="user[phone]" placeholder="<?php echo $_['form_phone']; ?>">
                  <input type="text" class="usr_f_email" name="user[email]" placeholder="<?php echo $_['form_email']; ?>">
                  <input type="text" name="user_info[country]" placeholder="<?php echo $_['form_country']; ?>">
                  <input type="text" name="user_info[skype]" placeholder="<?php echo $_['form_skype']; ?>">
                  <input type="text" name="user_info_lawyer[linkedin]" placeholder="<?php echo $_['form_linkedin']; ?>">
                  <input type="hidden" name="user_info_lawyer[area]" id="area" value="" />
                  <div class="dropdown">
                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="area-show">
                            <?php echo $_['form_area_select']; ?>
                        </span>
                        <div class="def" style="display:none">
                            <?php echo $_['form_area_select']; ?>
                        </div>
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <?
						$pref='_ru';
						if($curlang == 'english')
							$pref='_en';
						foreach($specs as $ar): ?>
                            <li> 
                                <table style="width:100%; table-layout:fixed">
                                    <tr>
                                        <td>
											<input id="ch<?=$ar['id']?>" type="checkbox" name="user_specialisation[]" value="<?=$ar['id']?>" class="d_sel">
                                            <label for="ch<?=$ar['id']?>" class="ololo"><?=$ar['title'.$pref]; ?></label>
                                        </td>
                                    </tr>
                                </table>                               
                                
                            </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>
                  <div class="input-group shit radio_lbl_wrap" style="height:50px; margin-top:10px;">
                      <input class="round-bottom" id="type_corp0" type="radio" name="user_info_lawyer[corporate]" value="0" checked /><label class="radio_lbl" for="type_corp0"><?php echo $_['form_i']; ?></label><input class="round-bottom" id="type_corp1" type="radio" name="user_info_lawyer[corporate]" value="1"/><label class="radio_lbl" for="type_corp1"><?php echo $_['form_they']; ?></label>
                  </div>
                  <?/*<div class="input-group shit round-bottom">
                  </div>*/?>
                  

              </form>
              <button class="modal-btn" id="reg-form">
                  <span class="">
                      <?php echo $_['form_do_register']; ?>
                  </span>
              </button>
              
          </div>
     
    </div>    
     
    </div>
  </div>
</div>


<div class="modal" id="follow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display : none">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content land-modal">
        <div class="modal-content land-modal">
        <div class="modal-title">
         <?php echo $_['form_register']; ?>
         <div class="modal-close" data-dismiss="modal" aria-label="Close"></div>
        </div>
		<div style="padding-top:15px;">
			<?if($curlang == 'english'){
				?>
				<b>THANKS FRO REGISTER</b><br/>Leave contacts, so that we can contact you
				<?
			}
			else{
				?><b>СПАСИБО ЗА РЕГИСТРАЦИЮ</b><br/>Оставьте дополнительные контакты, чтобы мы могли связаться с Вами<?
			}?>
		</div>
        <div class="modal-data email_reg_block">
              <form action="/lawyer/add_contacts_to_lawyer" id="law-form">

                  <input type="text" name="user[phone]" placeholder="<?php echo $_['form_phone']; ?>">
                  <input type="text" name="user_info[skype]" placeholder="<?php echo $_['form_skype']; ?>">
              </form>
              <button class="modal-btn" id="join_form">
                  <span class="">
                      <?php echo $_['add']; ?>
                  </span>
              </button>
              
          </div>
     
    </div>    
     
    </div>
  </div>
</div>
<?
if(isset($_GET['scs_reg_linkedin']) && $this->session->userdata('user_id')){
	?>
	<script>
		$('#follow').modal();
		window.history.pushState(null, null, '/lawyer');
	</script>
	<?
}
?>

<?php $this->view('jivosite'); ?>
</body>
</html>
<!--<![endif]-->
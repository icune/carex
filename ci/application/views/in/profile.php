<?php $this->view('in/header'); ?>
<script type="text/javascript" src="/js/net.js"></script>
<script type="text/javascript" src="/js/Chunky.js"></script>
<script type="text/javascript" src="/plug/guillotine/jquery.guillotine.js"></script>
<div class="col-sm-9  col-md-10  main">
    <div class="row">
        <p class="pull-left btn-left-sidebar visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
        </p>
    </div>
    <form action="/in/profile/update" method="POST">
    <div class="row user-main-info">

        <div class="col-md-4 col-md-offset-1 col-sm-6">
            <div class="expert-photo" id="image" style="background: url(<?php echo User::$image_c; ?>) no-repeat center;background-size: cover;"></div>
        <input type="hidden" name="user_info[image]" value="<?php echo User::$user_info['image']; ?>">

        </div>

        <div class="col-md-6 col-md-offset-1 col-sm-6">
            <p class="pull-left btn-left-sidebar visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
            </p>

            <table class="user-data">
                <tr>
                    <td>Имя</td>
                    <td>
                        <input type="text" name="user_info[name]" value="<?php echo User::$user_info['name']; ?>">
                    </td>
                </tr>
                <tr>
                    <td>Телефон</td>
                    <td>
                        <input type="text" name="user[phone]" value="<?php echo User::$user['phone']; ?>">
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        <input type="text" name="user[email]" value="<?php echo User::$user['email']; ?>">
                    </td>
                </tr>
                <tr>
                    <td>Местоположение</td>
                    <td>
                        <input type="text" name="user_info[city]" value="<?php echo User::$user_info['city']; ?>">
                    </td>
                </tr>
            </table>
        </div>


    </div>
    
    <div class="row">
        <div class="col-md-4 col-md-offset-1 col-sm-6">
            
            <div class="btn btn-default" style="position:relative; cursor:pointer;">
                Загрузить фотографию
                <input id="upl" type="file" style="position:absolute; opacity:0.001; left:0; top:0; width:100%; height:100%">
            </div>

        </div>
        <div class="col-md-6 col-md-offset-1 col-sm-6">
            <button class="btn btn-default">Сохранить</button>
            <div class="btn btn-default" onclick="$('#restoreModal').modal()">Восстановить пароль</div>
        </div>
    </div>
    </form>
    
</div>

<link rel="stylesheet" href="/plug/guillotine/jquery.guillotine.css" />
<script type="text/javascript">
    $(function(){
    var userSrc = null;
    Chunky({
        fileElem:$('#upl'),
        specialDir:'ava/',
        callbacks:{
            done:function(fn){  
                $('#image').css('background-image', 'url('+fn+')');
                $('[name="user_info[image]"]').val(fn);
                var __img = $("<img/>")
                    .attr("src", fn)
                    .load(function() {
                        var real_width = this.width;
                        var real_height = this.height;
                        $('#img').load(function(){
                            userSrc = fn;
                            $('#img').guillotine({
                                width:150,
                                height:150
                            });
                            for(var i = 0; i < 20; i++)
                                $('#img').guillotine('zoomOut');
                        }).attr('src', fn);        

                        $('.fluid').show();
                        __img.remove();
                    });

                
                // Net.post('/u/edit/set', {
                //  user:{image:fn}
                // });
            }
        }
    })

    $('#plus').click(function(){
        for(var i = 0; i < 2; i++)
            $('#img').guillotine('zoomIn');
    });
    $('#minus').click(function(){
        for(var i = 0; i < 2; i++)
            $('#img').guillotine('zoomOut');
    });

    $('#ok').click(function(){
        var data = $('#img').guillotine('getData');
        Net.post('/u/edit/rescale', {userSrc:userSrc, p:data}, {
            success:function(){
                var cFn = userSrc.split('.');
                cFn[cFn.length-2] = cFn[cFn.length-2] + '-c';
                cFn = cFn.join('.');
                $('#image').css('background-image', 'url('+cFn+')');
                $('.fluid').hide();
                
            },
            error:function(){
                alert('error');
                $('.fluid').hide();
            }
        })
    })
})
</script>
<link rel="stylesheet" href="/css/imperium/crop.css">
<div class="fluid" style="display:none">
    <div class="center">
        <div class="img-cont">
            <img src="/img/ava.png" alt="" id="img" />
        </div>
        <div class="buts">
            <div class="but" id="plus">+</div>
            <div class="but" id="minus">-</div>    
            <div class="but ok" id="ok">OK</div>
        </div>
        
    </div>
</div>
<style type="text/css">
    .wrong{
        border: 1px solid red;
    }
    #msg{
        text-align: center;
        color:red;
    }
</style>

<div class="modal fade" id="restoreModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Введите новый пароль</h4>
      </div>
      <div class="modal-body">
        <div class="form-group" id="msg">
        
      </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Введите пароль</label>
    <input type="password" class="form-control" id="restore-password" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Введите пароль повторно</label>
    <input type="password" class="form-control" id="restore-password-again" placeholder="">
  </div>
 

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" id="restore-submit" class="btn btn-primary">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function(){
        function reset(e, wait){
            if(wait)
                setTimeout(_x, 2000);
            else
                _x();
            function _x(){
                e.removeClass('wrong');
                $('#msg').text('');
            }
        }
        $('#restore-submit').click(function(){
            var p = $('#restore-password');
            var p2 = $('#restore-password-again');
            p.keyup(function(){
                reset(p);
            });
            p2.keyup(function(){
                reset(p2);
            });
            if(!p.val().trim()){
                p.addClass('wrong');
                $('#msg').text('Введите пароль');
                reset(p, true);
                return;
            };
            if(!p2.val().trim()){
                p2.addClass('wrong');
                $('#msg').text('Введите пароль повторно');
                reset(p, true);
                return;
            };
            if(p2.val() != p.val()){
                p.addClass('wrong');
                p2.addClass('wrong');
                $('#msg').text('Пароли не совпадают');
                reset(p, true);
                reset(p2, true);
                return;
            };
            Net.post('/u/edit/password', {password:p.val()}, {
                success : function(){
                    alert('Пароль успешно изменен!');
                    $('#restoreModal').modal('hide');
                },error : function(){
                    alert('Ошибка');
                }
            })

        });
    })
    <?php if(isset($_GET['restore'])): ?>
        $(function(){
            $('#restoreModal').modal();
        })
    <?php endif; ?>
</script>
<?php $this->view('in/footer'); ?>
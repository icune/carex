<?php $this->view('header'); ?>


    <!--slider Section Start Here -->
    <section id="slider">
        <div class="container-fluid">
            <div class="row">

                <!--slider Start Here -->
                <div class="banner">
                    <div class="main-bg">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="main-logo">
                                        <a href="#"><img src="/assets/img/logo.png" alt="">
                                        </a>
                                    </div>
                                    <span class="main-sub-title">
                          первый сервис по поиску <br>
                          автомобильных экспертов
                      </span>
                                </div>
                            </div>
                            <div class="row">
                                <h1 class="main-title">
                      Купить подержанный автомобиль теперь
                      <div class="sub-title">
                      <span class="tooltip1 tooltip1-effect-1"><span class="tooltip1-item">выгодно</span><span class="tooltip1-content clearfix"><span class="tooltip1-text">
                          С нашим сервисом вы съекономите не только ваши деньги, но и ваше время!
                      </span></span></span>, <span class="tooltip1 tooltip1-effect-1"><span class="tooltip1-item">надежно</span><span class="tooltip1-content clearfix"><span class="tooltip1-text">
                          Все автоэксперты проверены и являются нашими надежными партенрами
                      </span></span></span>, <span class="tooltip1 tooltip1-effect-1"><span class="tooltip1-item">безопасно</span><span class="tooltip1-content clearfix"><span class="tooltip1-text">
                          Сервис безопасная сделка позволит вам оплатить услуги уже после покупки автомобиля
                      </span></span></span>



                  </div>
                  </h1>



                            </div>
                        </div>
                    </div>
                    <div class="resto-info">
                        <div class="container">

                            <div class="tabcontent main-form">
                                <div class="mid tab-block1 clearfix">
                                    
                                    <div class="col-sm-12 form-field">
                                        <div class="row">
                                            <div class="text-center">
                                                <div class="wanttobuy">Я хочу купить</div>
                                            </div>
                                            
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <input class="form brand" type="text" placeholder="Укажите марку">
                                                <input class="form model" type="text" placeholder="Укажите модель">
                                            </div>
                                            <div class="col-sm-4 ">
                                                <input class="form year" type="text" placeholder="Год выпуска">
                                                <input class="form kpp" type="text" placeholder="КПП">
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="col-sm-6 col-sm-offset-3 form-field">
                                        <div class="price-meter-wrap"> <span class="meter-label">Или просто укажите ценовой диапозон </span>
                                            <div class="range-wrap"> <span id="amount1"> </span> <span id="amount"> </span>
                                                <div id="slider-meter"> </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="col-sm-12 form-field">
                                        <div class="row">
                                            <div class="text-center">
                                                <div class="wanttobuy">Ваши контакты</div>
                                            </div>
                                            
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <input class="form _email" type="text" placeholder="Ваш E-mail">
                                            </div>
                                            <div class="col-sm-4">
                                                <input class="form phone" type="text" placeholder="Ваш телефон">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                            <div class="col-xs-8 col-xs-offset-2">
                                                <button class="btn btn-default btn-effect" id="make-request" style="margin:0 auto;"> Сделать запрос эксперту <i> <img src="/assets/img/arrow-btn.png" alt="" /> </i> </button>
                                            </div>
                                            <div class="col-sm-12 bottom-title">
                                                Только проверенные специалисты
                                            </div>
                                        </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!--slider End Here -->

            </div>
        </div>
    </section>
    <!--slider Section End Here -->

    <!--    icon-box  with quote   -->

    <section class="car-dealership">
        <div class="container">
            <section class="section-wrap">
                <h3 class="block-title" align="center">Как это работает</h3>
                <div class="how-it-works">
                    <ul class="row">
                        <li class="col-sm-4">
                            <figure class="works">
                                <img src="/assets/img/1.svg" alt="">
                                <figcaption></figcaption>
                            </figure>
                            

                            <span>Вы заполняете форму и делаете запрос нашим экспертам</span>

                        </li>
                        <li class="col-sm-4">
                            <figure class="works">
                                <img src="/assets/img/2.svg" alt="">
                                <figcaption></figcaption>
                            </figure>
                            <span>В течение 24 часов мы отправим вам на почту 6 автомобилей с указанными вами параметрами</span>

                        </li>
                        <li class="col-sm-4">
                            <figure class="works">
                                <img src="/assets/img/3.svg" alt="">
                                <figcaption></figcaption>
                            </figure>
                            <span>Если вам понравятся выбранные автомобили, вы можете связаться с автоэкспертом и обсудить остальные детали</span>

                        </li>

                    </ul>

                    <div class="how-it-works-bottom">
                        <i class="fa fa-cog "></i> Чтобы обезопасить и Вас и эксперта от разногласий в оплате услуги,
                        <br> мы предлагаем вам воспользоваться нашим сервисом <a href="#" data-toggle="modal" data-target="#myModal-safe">безопасная сделка</a>

                    </div>

                </div>
            </section>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="myModal-safe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Безопасная сделка</h4>
                </div>
                <div class="modal-body">
                    <p>Безопасная сделка - специальная услуга нашего сервиса по взаимодействию клиентов и автоэкспертов, где наш сервис выступает в роли независимой третьей стороны и гарантирует выполнение обязательств с обеих сторон. </p>

                    <p>Данная услуга позволяет оплатить услуги автоэксперта только после их выполнения (никаких авансов) и также данная услуга даёт уверенность эксперту,что его услуги будут оплачены.</p>
                </div>
                <div class="modal-footer">
                   

                </div>
            </div>
        </div>
    </div>



    <!--   Recent listing        -->

    <section class="auto-blog-video">
        <div class="container">
            <div class="car-listing">
                <h2 class="h3" align="center"> Наши лучшие эксперты </h2>
                



                
                <div class="owl-carousel" id="video-blog-list">

            <?php 

                    foreach ($experts as $key => $value) {
                        if ($key == 1 or $key == 2 or $key == 3  ) 
                            { ?>
                                    <div class="slide-wrap">
                                        <article class="auto-blog ">


                                            <figure class="video-wrap">
                                                <div class="expert-photo" style="background: url( <?php echo $value["image"]; ?> ) no-repeat center;background-size: cover;">
                                                </div>

                                            </figure>
                                            <div class="img-description">
                                                <h3><a href="#"><?php echo $value["name"]; ?> <?php echo $value["surname"]; ?>,  <?php echo $value["city"]; ?></a> <i class="fa fa-check"></i></h3>
                                                 <ul class="exp-rate">
                                                    <?php 
                                                        for($i = 0; $i < 5; $i++){
                                                            if(intval($value['rating']) > $i)
                                                                echo '<li><i class="fa fa-star"></i></li>';
                                                            else
                                                                echo '<li><i class="fa fa-star fa-star-o"></i></li>';
                                                        }
                                                     ?>
                                                </ul>
                                                <p> 
                                                    <?php echo $value["work_conditions"]; ?>
                                                </p>
                                                
                                                <ul class="social-share clearfix">

                                                    <li>
                                                        <!-- <a href="#">Посмотерть отзывы <i class="fa fa-comments"></i> 255 </a>-->
                                                    </li>
                                                </ul>

                                                <a href="/expert?id=<?php echo $value["id"]; ?>" class="btn btn-default btn-effect"> Подробнее <i> <img src="/assets/img/arrow-btn.png" alt=""> </i> </a> 
                                            </div>
                                        </article>
                                    </div>
                        <?php
                                

                            }
                        
                        
                    }

                 ?>
                
                  
                </div>
                <div class="row">
                    <div class="col-sm-12"><a href="/experts" class="btn btn-default show-all-exp">
                                            Посмотреть всех экспертов
                                        </a></div>
                </div>
            </div>
        </div>
    </section>



    <!--  Уже более 100 довольных клиентов -->
    <section class="car-sale review-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 ">
                    <h2 class="h3" align="center">Уже более 100 довольных клиентов </h2>

                    <article class="col-sm-4">
                        <div class="article-wrap">
                            <div class="row review-head">

                                <div class="col-sm-12">
                                    <div class="expert-photo pull-left" style="background: url(/upl/ava-expert/Sutugin-c.jpg) no-repeat center;background-size: cover;">
                                    </div>
                                    <div class="img-description">
                                        <h4><a href="#">Сергей</a></h4>
                                        <p> Подбор, автомобиля </p>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <p class="col-sm-12">
                                    Спасибо большое Сергею за отличную бэху. Очень доволен, достаточно быстро нашли! Автомобиль вообще в порядке!
                                </p>

                            </div>
                            <div class="row">
                                <p class="col-sm-8 col-sm-offset-4">
                                    <b>Максим, Москва, BMW 3</b>
                                    <span class="date"> 20 октября 2015 </span>
                                </p>
                            </div>


                        </div>
                    </article>

                    <article class="col-sm-4">
                        <div class="article-wrap">
                            <div class="row review-head">

                                <div class="col-sm-12">
                                    <div class="expert-photo pull-left" style="background: url(/upl/ava-expert/1231-c.jpg) no-repeat center;background-size: cover;">
                                    </div>
                                    <div class="img-description">
                                        <h4><a href="#">Олег</a></h4>
                                        <p> Подбор, автомобиля </p>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <p class="col-sm-12">
                                    Олег просто профессионал своего дела! Посмотрели с ним три машины, сразу видит все дефекты! В итоги купли отличны лансер! Все четко, спасибо.
                                </p>

                            </div>
                            <div class="row">
                                <p class="col-sm-8 col-sm-offset-4">
                                    <b>Денис, Москва, Mitsubishi Lanser X</b>
                                    <span class="date"> 20 июня 2015 </span>
                                </p>
                            </div>


                        </div>
                    </article>

                    <article class="col-sm-4">
                        <div class="article-wrap">
                            <div class="row review-head">

                                <div class="col-sm-12">
                                    <div class="expert-photo pull-left" style="background: url(/upl/ava-expert/9F4XNg2uTmI-c.jpg) no-repeat center;background-size: cover;">
                                    </div>
                                    <div class="img-description">
                                        <h4><a href="#">Астафьев Андрей</a></h4>
                                        <p> Подбор, автомобиля </p>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <p class="col-sm-12">
                                   Андрей спасибо! Мой Aveo просто милашка, почти как новый! Едет отлично, всё нравится!
                                </p>

                            </div>
                            <div class="row">
                                <p class="col-sm-8 col-sm-offset-4">
                                    <b>Валерия, Ногинск, Chevrolet Aveo</b>
                                    <span class="date"> 15 октября 2015 </span>
                                </p>
                            </div>


                        </div>
                    </article>


                </div>

            </div>
        </div>
    </section>
    <!--  Car Sale -->



<?php $this->view('footer'); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {
	public function index(){
		$this->load->database();
		$promo = $this->postAbsent('promo');
		if(User::activatePromo($promo))
			$this->ok();
		else
			$this->err();
	}
}

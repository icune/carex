$(function(){
	$('.exp-login.login .email').keyup(function(){
		$('.restore-link').attr('href', '/restore?email=' + $(this).val())
	})
	$('.exp-login.login .login').click(function(){
		var e = $(this).closest('.exp-login').find('.email');
		var p = $(this).closest('.exp-login').find('.password');
		if(!e.val().trim()){
			alert('Введите E-mail');
			return;
		};
		if(!p.val().trim()){
			alert('Введите пароль');
			return;
		}
		var r = {
			email : e.val(),
			password : p.val()
		};
		Net.post('/u/login/check', r, {
			success:function(){
                window.location.href = '/u/login?email=' + r.email + 
                '&password='+r.password+
                '&redirect=/in/tasks/';
			},
			error:function(msg){
                if(msg == 'abs-email'){
                    alert('Неправильный E-mail');
                    return;
                }
                if(msg == 'wrong-password'){
                	alert('Неправильный пароль');
                    return;
                }
				alert('Ошибка');
			}
		})
	})
	$('.exp-login.exp-reg .reg').click(function(){
		var n = $(this).closest('.exp-login').find('.name');
		var p = $(this).closest('.exp-login').find('.phone');
		var e = $(this).closest('.exp-login').find('.email');
		var r = {
			name:n.val(),
			phone:p.val(),
			email:e.val()
		};

		if(!p.val()){
			alert('Заполните поле Телефон');
			return;
		}
		if(!n.val()){
			alert('Заполните поле Имя');
			return;
		}

		Net.post('/register/expert', {info:r}, {
			success : function(){
				$('.exp-login.exp-reg').parent().text('Спасибо за Вашу заявку!');
			},error : function(){
				$('.exp-login.exp-reg').parent().text('Ошибкаю');
			}
		})
	});
	$('.reg-user .btn').click(function(){
		var n = $(this).closest('.reg-user').find('.name');
		var p = $(this).closest('.reg-user').find('.phone');
		var e = $(this).closest('.reg-user').find('.email');
		var r = {
			name:n.val(),
			phone:p.val(),
			email:e.val(),
			password:'123'
		};
		if(!r.email.match(/^.+@.+\..+$/)){
			alert('Неправильный E-mail');
			return;
		}
		if(!n.val()){
			alert('Заполните поле Имя');
			return;
		}
		Net.post('/u/register/check', r, {
			success:function(){
                window.location.href = '/u/register?email=' + r.email + 
                '&password='+r.password + '&name='+r.name+'&phone='+r.phone;
			},
			error:function(msg){
                if(msg == 'email-busy'){
                    alert('E-mail занят');
                    return;
                }
				alert('Ошибка');
			}
		})
	});


	$('.reg-expert .btn').click(function(){
		

		var n = $(this).closest('.reg-expert').find('.name');
		var p = $(this).closest('.reg-expert').find('.phone');
		var e = $(this).closest('.reg-expert').find('.email');
		var r = {
			name:n.val(),
			phone:p.val(),
			email:e.val()
		};
		if(!p.val()){
			alert('Заполните поле Телефон');
			return;
		}
		if(!n.val()){
			alert('Заполните поле Имя');
			return;
		}


		Net.post('/register/expert', {info:r}, {
			success : function(){
				$('.reg-expert').html('<h4 class="text-center">Спасибо за Вашу заявку!</h4>');
				$('.reg-user').text('');
			},error : function(){
				$('.reg-expert').text('Ошибка');
				$('.reg-user').text('');
			}
		})
	});


	//Задать вопрос эксперту
	$('.exp-question .send-q').click(function(){
		var n = $(this).closest('.exp-question').find('.name');
		var p = $(this).closest('.exp-question').find('.phone');
		var e = $(this).closest('.exp-question').find('.email');
		var q = $(this).closest('.exp-question').find('.meassage');
		var s = $(this).closest('.exp-question').find('.exp-name');
		var r = {
			name:n.val(),
			phone:p.val(),
			email:e.val(),
			message:q.val(),
			expert:s.val()
		};
		if(!p.val()){
			alert('Заполните поле Телефон');
			return;
		}
		if(!n.val()){
			alert('Заполните поле Имя');
			return;
		}
		Net.post('/register/link', {info:r}, {
			success : function(){
				$('.exp-question').text('Спасибо за Вашу заявку!');
				$('.reg-user').text('');
			},error : function(){
				$('.exp-question').text('Ошибка');
				$('.reg-user').text('');
			}
		})
	});


	//Восстановить пароль
	$('.restore-pass .btn').click(function(){
		var n = $(this).closest('.restore-pass').find('.email');
		
		
		var r = {
			goal: "Восстановление пароля",
			email:n.val()
			
		};
		Net.post('/u/login/restore', r, {
			success : function(){
				$('.restore-pass').text('В ближайшее время на ваш email будет выслана инструкция по восстановлению');
				$('.reg-user').text('');
			},error : function(){
				$('.restore-pass').text('Ошибка');
				$('.reg-user').text('');
			}
		})
	});


	$('#make-request').click(function(){
		var task = {
			from : 100000,
			to : 500000,
			desc : $('.form.brand').val() + ' ' + $('.form.model').val() + ' ' + $('.form.year').val() + ' ' + $('.form.kpp').val()
		};
		var email = $('.form._email').val();
		var phone = $('.form.phone').val();
		if(!email.match(/^.+@.+\..+$/)){
			alert('Неккоректный E-mail');
			return;
		}
		var req = {
			task:task,
			email:email,
			phone:phone
		}
		window.location.href = '/u/register?' + $.param(req);
		
	})
})
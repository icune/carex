<?php
$lang['we_invite'] = "";
$lang['are_you'] = "For lawyers";
$lang['title'] = "Legal space";
$lang['first_consult'] = "First consultation is free.";
$lang['best_juri_online'] = "Best lawyers worldwide. Online";
$lang['bla_bla_before_form'] = "FILL IN THE DETAILS AND FIND YOUR LAWYER";
$lang['get_free_consult'] = "Get free advice";
$lang['where_you'] = "Where do you need a lawyer?";
$lang['desc_problem'] = "Describe your legal matter";
$lang['send'] = "Send";
$lang['footer_blabla'] = "© Copyright 2015. Legal Space";
$lang['rights'] = "All rights reserved";
$lang['moscow'] = "Moscow";
$lang['moscow_addr'] = "15 Presnensky val";
$lang['milan'] = "Milan";
$lang['milan_addr'] = "Via Friuli, 51 - 20135";
$lang['nikosia'] = "Nikosia";
$lang['nikosia_addr'] = "Psaron , 2408 ";
$lang['from_where_you'] = "Where are you from?";
$lang['name'] = "Your name";
$lang['phone'] = "Contact phone number";
$lang['success'] = "Thank you for your request! We’re on your case.";
$lang['error'] = "Something went wrong. Please contact us and we will help: info@legalspace.world";
$lang['bla_1'] = "<p>Сервис LEGAL SPACE — это безопасная и надежная площадка, которая поможет вам решить юридическую задачу в любой точке мира on-line. Учреждаете компанию в Лондоне или регистрируете товарный знак в Италии? Получаете вид на жительство в США? Любые юридические услуги в любой точке мира прямо сейчас на нашем сайте. </p><p>Каждый юрист LEGAL SPACE проходит жесткий отбор, поэтому мы уверенны в его квалификации и профессионализме и ручаемся за качество оказанных им услуг.</p>";
$lang['bla_2'] = "<p>Все наши юристы обладают опытом работы не менее 5 лет. Каждый кандидат проходит личное собеседование перед началом работы. База юристов LEGAL SPACE пополняется только через рекомендации и личные отзывы. </p><p>Наши партнеры это лучшие представители юридической отрасли они прогрессивны и всегда находятся в центре новаций. Вместе мы меняем мир юридических услуг в лучшую сторону .</p>";
$lang['we_connect'] = "МЫ СВЯЗЫВАЕМ ВАС С ЛУЧШИМИ ЮРИСТАМИ НАПРЯМУЮ";
$lang['spoiled_phone'] = "Наша обширная географическая база позволяет решить Вашу задачу в любой стране.";
$lang['with_support'] = "При поддержке";
$lang['globe_under'] = "стран";
$lang['hat_under'] = "юристов";
$lang['case_under'] = "клиентов";
$lang['office_title'] = "Наши офисы:";
$lang['office_desc'] = "Позвоните и мы поможем:";
$lang['moscow_desc'] = "Московский офис расположен в самом центре Москвы, в 5 минутах от метро Белорусская. Паркинг для клиентов. 
";
$lang['milan_desc'] = "Миланский офис расположен в самом центре Милана, в 10 минутах от метро Lodi.
";
$lang['nikosia_desc'] = "Кипрский офис удобно расположен вблизи аэропорта.
";
$lang['find_descision'] = "Решите юридический вопрос сейчас";
$lang['find_descision_after'] = "Нужна помощь? Мы Вам позвоним";
$lang['bot_button'] = "Позвоните мне";
$lang['enter_lk'] = "Вход в личный кабинет";
$lang['password'] = "Пароль";
$lang['_main_page_h1_title'] = "Best lawyers worldwide. Online";
$lang['_main_page_h1_subtitle'] = "FILL IN THE DETAILS AND FIND YOUR LAWYER";
$lang['_main_page_btn_consult'] = "GET free advice";
$lang['_main_page_trigers_title_1'] = "FIND LAWYERS EASILY";
$lang['_main_page_trigers_text_1'] = "More than 2 000 lawyers from more than 150 countries";
$lang['_main_page_trigers_title_2'] = "BEST LAWYERS";
$lang['_main_page_trigers_text_2'] = "6 years experience, references and passed personal interview ";
$lang['_main_page_trigers_title_3'] = "SECURED TRANSACTION";
$lang['_main_page_trigers_text_3'] = "The lawyer receives payment only after the work is done.";
$lang['_main_page_best_lawyers_h2'] = "WE CONNECT YOU <br> WITH THE BEST LAWYERS DIRECTLY";
$lang['_main_page_best_lawyers_sub-title'] = "Legal Space is a safe and reliable virtual resource to help you resolve your legal issues online. You can get any legal service anywhere in the world right now on our website. Leave the request and we will provide you with the expert with the highest professional qualifications. Find legal solutions 
right now";
$lang['foo_1'] = "Trademarks and intellectual property
            <br>Real estate
            <br>Corporate M&amp;A
            <br>Private equity";;
$lang['foo_2'] = "International tax and corporate structuring<br/>Tax<br/>Investments<br/>Company formation<br/>Investment funds";
$lang['foo_3'] = "Investment funds<br/>Restructuring and insolvency<br/>Labour law<br/>Competition and antitrust";
$lang['work_support_title'] = "We are working with the support of";
$lang['with_'] = "from";
$lang['rub'] = "r";
$lang['days'] = "days";
$lang['_main_page_best_lawyers_info_text'] = "
                                <p>Legal Space is a safe and reliable virtual resource to help you resolve your legal issues online. </p>

<p>You can get any legal service anywhere in the world right now on our website. Leave the request and we will provide you with the expert with the highest professional qualifications. </p>

<p>Every Legal Space lawyer passes through a highly competitive selection process, which is why we are sure of his or her qualifications and professionalism and can guarantee the quality of his or her services. </p>
                                ";
$lang['_main_page_brands_support'] = "При поддержке";
$lang['_main_page_pop_service_h2'] = "MOST POPULAR SERVICES";
$lang['_main_page_pop_service_subtitle'] = "Вы можете позвонить в любой из наших офисов в рабочее время и мы
                                        <br> проконсультируем вас на русском или английском языке";
$lang['_main_page_pop_service_title_1'] = "Start
                                                                <br> Business
                                                                <br> in Russia";
$lang['_main_page_pop_service_text_1'] = "Start your business is easier than you think. We will help you navigate the process and prepare the documents to start a business";
$lang['_main_page_pop_service_btn'] = "FREE ADVICE";
$lang['_main_page_pop_service_btn_more'] = "Узнать подробнее";
$lang['_main_page_pop_service_title_2'] = "Create
                                                                <br> Company
                                                                <br> in Cyprus";
$lang['_main_page_pop_service_text_2'] = "Cyprus company - one of the best solutions when you need access to international markets";
$lang['_main_page_pop_service_title_3'] = "Register
                                                                <br> Trademark";
$lang['_main_page_pop_service_text_3'] = "You need to protect your brand on international market. We will help you to do it without any red tape";
$lang['_main_page_solution_h2'] = "We help";
$lang['_main_page_solution_subtitle'] = "";
$lang['_main_page_solution_title_startup'] = "START-UP";
$lang['_main_page_solution_title_lawyer'] = "LAWYERS";
$lang['_main_page_solution_title_business'] = "BUSINESS";
$lang['_main_page_solution_text_business'] = "
                            
                                I knew what I wanted out of my Series A, <br> but Legal space helped me hire a lawyer<br> who could actually get me there!";
$lang['_main_page_solution_btn_business'] = "EXPAND YOUR BUSINESS";
$lang['_main_page_solution_business_clinet'] = "<b>Dmitry Zaruta</b>
                                                         CEO EASY TEN";
$lang['_main_page_solution_text_lawyer'] = "
                            Legal Space partners with my legal department  <br />so we can find the niche help<br>  we need on an ongoing basis  <br> and scale up our legal resources <br>during a major deal or litigation.
                        ";
$lang['_main_page_solution_btn_lawyer'] = "FIND THE RIGHT EXPERT";
$lang['_main_page_solution_text_startup'] = "
                            As a small business owner, <br>I don't have time to find <br> excellent legal help at a price I can afford. <br>I just want someone <br>who understands my business and budget.<br>Thanks, Legal Space.
                        ";
$lang['_main_page_solution_btn_startup'] = "START YOUR BUSINESS";
$lang['_main_page_solution_startup_clinet'] = "Bash.today
                                                        <b>Team</b>";
$lang['_main_page_activity_h2'] = "";
$lang['_main_page_activity_list_1'] = "Trademarks and intellectual property";
$lang['_main_page_activity_list_2'] = "Real estate";
$lang['_main_page_activity_list_3'] = "Corporate M&amp;A";
$lang['_main_page_activity_list_4'] = "Private equity";
$lang['_main_page_activity_list_5'] = "International tax and corporate structuring";
$lang['_main_page_activity_list_6'] = "Tax";
$lang['_main_page_activity_list_7'] = "Investments";
$lang['_main_page_activity_list_8'] = "Company formation";
$lang['_main_page_activity_list_9'] = "Investment funds";
$lang['_main_page_activity_list_10'] = "Restructuring and insolvency";
$lang['_main_page_activity_list_11'] = "Labour law";
$lang['_main_page_activity_list_12'] = "Competition and antitrust";
$lang['_main_page_offices_h2'] = "Our offices:";
$lang['_main_page_offices_subtitle'] = "Call us and we will help find solution that suits you well";
$lang['_main_page_map_adr1_title'] = "Russia";
$lang['city2'] = "Milan";
$lang['city1'] = "Moscow";
$lang['city3'] = "Nicosia";
$lang['_main_page_adr1_adress'] = "Via Friuli, 51 - 20135";
$lang['_main_page_adr1_phone'] = "+39 029-475-96-84";
$lang['_main_page_map_adr2_title'] = "Italy";
$lang['_main_page_adr2_adress'] = "15 Presnensky val";
$lang['_main_page_adr2_phone'] = "8 (800) 333-67-36";
$lang['_main_page_map_adr3_title'] = "Cyprus";
$lang['_main_page_adr3_adress'] = "Psaron , 2408";
$lang['_main_page_adr3_phone'] = "+357 230-300-16";
$lang['_main_page_footer_h1'] = "Find legal solutions  <br> right now";
$lang['_main_page_footer_btn'] = "Call me";
$lang['footer_text'] = "       <b>Legal Space</b> — is an international community of more than 2 000 first-class lawyers.  <br>
                             Our service allows you to get high quality legal services on the transparent basis.";
$lang['cpr']='© 2015 Legal Space Ltd., All rights reserved.';
$lang['maria'] = "Мария Нечепа";
$lang['maria_record'] = "Директор по инвестиционной деятельности банка";
$lang['sk_title'] = "FOR SKOLKOVO FRIENDS";
$lang['sk_text'] = "We give you 9,000 rubles for legal services of our company<br> in honor of the birthday of the school";
$lang['sk_activate'] = "Activate your card";
$lang['where_need'] = "В какой стране вам нужен юрист?";
$lang['russkii'] = "Русский";
$lang['v_kakoi_strane_vam_nuzhen_urist'] = "In which country do you need a lawyer?";
$lang['our_benefits'] = "Our benefits";
$lang['nomer_karty'] = "Номер карты";
$lang['imja_i_familija'] = "Your name";
$lang['telefon'] = "Phone number";
$lang['blok_'] = "Блок ";
$lang['trigery_'] = "тригеры ";
$lang['luczshie_uristy_'] = "Лучшие юристы ";
$lang['logotipy_'] = "Логотипы ";
$lang['vash_telefon'] = "Contact phone number";
$lang['opishite_vashu_zadaczu'] = "Describe your legal matter";
$lang['main_page_map_adr3_info'] = "
                              
                            ";

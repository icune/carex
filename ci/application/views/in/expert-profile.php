<?php $this->view('in/header'); ?>

<div class="col-sm-9  col-md-10  main">

    <!-- Профиль эксперта  -->
    <div class="row exper-main-info">
        <div class="row">
            <p class="pull-left btn-left-sidebar visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
            </p>
        </div>
        <div class="col-md-4 col-md-offset-1 col-sm-6">
            <div class="expert-photo" style="background: url(<?php echo $e['user_info']['image']; ?>) no-repeat center;background-size: cover;"></div>
            <div class="exp-rate-box">
                <div class="exp-rate-title">Рейтинг:</div>
                <ul class="exp-rate">
                    <?php 
                        for($i = 0; $i < 5; $i++){
                            if(intval($e['expert']['rating']) > $i)
                                echo '<li><i class="fa fa-star"></i></li>';
                            else
                                echo '<li><i class="fa fa-star fa-star-o"></i></li>';
                        }
                     ?>
                </ul>

            </div>

        </div>

        <div class="col-md-6 col-md-offset-1 col-sm-6">
            <p class="pull-left btn-left-sidebar visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
            </p>
            <h3 style="margin-left: -15px;">
                                Суругин Серегй Иванович
                            </h3>
            <div class="row exp-header">

                <div class="exp-spec">
                    
                    <p>
                        <b style="color:black">Специализация</b>: 
                        <?php 
                            $sp = array();
                            $sp_i = explode(',', $e['expert']['specializations']);
                            foreach($sp_i as $i)
                                foreach($specializations as $spec)
                                    if($spec['id'] == $i)
                                        $sp []= $spec['name'];
                            echo implode(', ', $sp);
                        ?>
                    </p>
                    <p>
                    <?php if($e['expert']['checked']): ?>
                        Данные проверены <i class="fa fa-check"></i>
                    <?php endif; ?>
                    </p>

                </div>
                <div class="exp-adress">
                    <b>Расположение:</b> 
                    <?php echo $e['user_info']['city']; ?>
                </div>
                <div class="exp-condition">
                    <b>Готов к выезду:</b> 
                    <?php echo $e['expert']['departure']; ?>
                </div>
            </div>
            <a href="#" class="btn btn-default btn-effect"> Связаться </a>
        </div>


    </div>

    <div class="row exp-work-conds">

        <div class="col-sm-6 ">
            <h4 class="exp-block-title">
                        Условия работы:
                    </h4>
            <div class="work-cond">
                <?php echo $e['expert']['work_conditions']; ?>
            </div>
        </div>

        <div class="col-sm-6">
            <h4 class="exp-block-title">Опыт</h4>
            <div class="skill">
                <?php echo $e['expert']['experience']; ?>
            </div>
            
        </div>

    </div>
    <div class="row exp-service-cost">
        <div class="col-sm-12">
            <h4 class="exp-block-title">
                        Cтоимость услуг
                    </h4>
        </div>

        <table class="table table-striped exp-cost-table">
            <?php foreach($e['services'] as $serv): ?>
                <tr>
                    <td><?php echo $serv['name']; ?></td>
                    <td><?php echo $serv['price']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>



</div>

<?php $this->view('in/footer'); ?>
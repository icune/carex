function Chunky(opt){


    if(!opt) opt = {};
    if(!opt.callbacks) opt.callbacks = {};

    if(!$('.loading').size())
      $('body').append($(loadingHtml()));

    opt.fileElem.on('change', function(e){
      var file = e.target.files[0];
      if(file.name.match(/[а-яА-Я]+/)){
        alert('Не должно быть русских букв');
        return;
      }
      DownloadFile(file);
    });



    function DownloadFile(file){
      var okFileName = '';
      FileNameChecker(file.name, {
        success:function(fileName){
          okFileName = fileName;
          var upl = new ChunkedUploader(file, $.extend({fileName:fileName}, cbs));
          upl.start();
          $('.loading').removeClass('hidden').find('.in').css('width', 0);
          $('.loading .file-name').text(fileName);
          window.onbeforeunload = function() {
            return "Загрузка идет. Уходить никак нельзя!";
          }
        },
        error:function(){}
      })

      var cbs = {
        onProgressUpdate:function(percent){
          $('.loading .in').css('width', percent*100 + '%');
        },
        onFinish:function(data){
          window.onbeforeunload = null;
          $('.loading').addClass('hidden');
          if(opt.callbacks.done instanceof Function) opt.callbacks.done(data.client_filename)
        }
      };
    }

    function FileNameChecker(fileName, callbacks){
      var data = 'file_name=' + fileName;
      if(opt.specialDir)
        data += '&special_dir='+opt.specialDir;
      $.ajax({
        url: '/upload/chunky',
        type: 'post',   
        dataType: 'json',
        data: data,
        cache: false,
        processData: false,   
        success: function(json) {
          if(callbacks.success instanceof Function)
            callbacks.success(json.file_name);
        },      
        error: function() {
          if(callbacks.error instanceof Function)
            callbacks.error();
        }
      });
    }


    function ChunkedUploader(file, options) {
        if (!this instanceof ChunkedUploader) {
            return new ChunkedUploader(file, options);
        }
     
        this.file = file;
     
        this.options = $.extend({
            url: '/upload/chunky'
        }, options);
     
        this.file_size = this.file.size;
        this.chunk_size = (1024 * 1024); 
        this.range_start = 0;
        this.range_end = this.chunk_size;
     
        if ('mozSlice' in this.file) {
            this.slice_method = 'mozSlice';
        }
        else if ('webkitSlice' in this.file) {
            this.slice_method = 'webkitSlice';
        }
        else {
            this.slice_method = 'slice';
        }
     
        this.upload_request = new XMLHttpRequest();
        this.upload_request.onload = this._onChunkComplete.bind(this);
    }
     
    ChunkedUploader.prototype = {
     
    // Internal Methods __________________________________________________
     
        _upload: function() {
            var self = this,
                chunk;
     
            // Slight timeout needed here (File read / AJAX readystate conflict?)
            setTimeout(function() {
                // Prevent range overflow
                if (self.range_end > self.file_size) {
                    self.range_end = self.file_size;
                }
     
                chunk = self.file[self.slice_method](self.range_start, self.range_end);
     
                self.upload_request.open('PUT', self.options.url, true);
                // self.upload_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                self.upload_request.overrideMimeType('application/octet-stream');
                
                self.upload_request.setRequestHeader('Content-Range', self.range_start + '-' + self.range_end + '/' + self.file_size);
                self.upload_request.setRequestHeader('File-Name', encodeURI(self.options.fileName));
                if(opt.specialDir)
                  self.upload_request.setRequestHeader('Special-Dir', opt.specialDir);
                self.upload_request.send(chunk);

            }, 20);
        },
     
    // Event Handlers ____________________________________________________
     
        _onChunkComplete: function(data) {
          var rJson = JSON.parse(data.target.response);
            // If the end range is already the same size as our file, we
            // can assume that our last chunk has been processed and exit
            // out of the function.
            var percent = (this.range_end + 0.0)/this.file_size;
            if(this.options.onProgressUpdate instanceof Function)
              this.options.onProgressUpdate(percent);

            if (this.range_end === this.file_size) {
                this._onUploadComplete(rJson);
                return;
            }
            
            // Update our ranges
            this.range_start = this.range_end;
            this.range_end = this.range_start + this.chunk_size;
     
            // Continue as long as we aren't paused
            if (!this.is_paused) {
                this._upload();
            }
        },
        _onUploadComplete: function(data){
          if(this.options.onFinish instanceof Function)
              this.options.onFinish(data);
        },
     
    // Public Methods ____________________________________________________
     
        start: function() {
            this._upload();
        },
     
        pause: function() {
            this.is_paused = true;
        },
     
        resume: function() {
            this.is_paused = false;
            this._upload();
        }
    };

    function loadingHtml(){return '<style type="text/css">\
      .loading.hidden{\
        display: none;\
      }\
      .loading .substrate{\
        position: fixed;\
        width:100%;\
        height:100%;\
        left:0;\
        top:0;\
        background-color: rgba(0, 0, 0, 0.6);\
        z-index:100;\
      }\
      .loading .band-base{\
        width:90%;\
        padding: 20px 0 20px 0;\
        overflow:hidden;\
        white-space:nowrap;\
        font-size: 20px;\
      }\
      .loading .band{\
        position: fixed;\
        left:50%;\
        top:50%;\
        transform:translate(-50%, -50%);\
        background-color: white;\
      }\
      .loading .band .in{\
       background-color: rgb(60, 177, 58);\
       color: white;\
       position: absolute;\
       left:0;\
       top:0;\
      }\
      .loading .file-name{\
        margin-left:20px;\
      }\
    </style>\
    <div class="loading hidden">\
      <div class="substrate">\
        <div class="band band-base">\
          <span class="file-name">Colo terrorita</span>\
          <div class="in band-base">\
            <span class="file-name">Colo terrorita</span>\
          </div>\
        </div>\
      </div>\
    </div>\
    '};
}
 
<?php
$lang['we_invite'] = "Приглашаем к нам";
$lang['are_you'] = "Вы юрист?";
$lang['title'] = "Legal space";
$lang['best_juri_online'] = "Best Lawyers Worldwide. Online";
$lang['bla_bla_before_form'] = "<p>Вам нужен юрист?</p>
					<p>Напишите, чем мы можем Вам помочь.</p>";
$lang['get_free_consult'] = "Получить бесплатную консультацию";
$lang['where_you'] = "Выберите страну";
$lang['desc_problem'] = "Опишите Вашу задачу";
$lang['send'] = "Отправить";
$lang['footer_blabla'] = "© Copyright 2015. Legal Space";
$lang['rights'] = "Все права защищены";
$lang['moscow'] = "Москва";
$lang['moscow_addr'] = "Пресненский вал, 15";
$lang['milan'] = "Милан";
$lang['milan_addr'] = "Виа Фриули, 51 - 20135";
$lang['nikosia'] = "Никосия";
$lang['nikosia_addr'] = "Псарон , 2408 ";
$lang['from_where_you'] = "Откуда вы?";
$lang['name'] = "Ваше имя";
$lang['phone'] = "Телефон";
$lang['success'] = "Спасибо за Ваш вопрос! Мы скоро с вами свяжемся";
$lang['error'] = "Произошла ошибка. Свяжитесь с нами и мы поможем: info@legalspace.world";
$lang['bla_1'] = "<p>Сервис LEGAL SPACE — это безопасная и надежная площадка, которая поможет вам решить юридическую задачу в любой точк%D";

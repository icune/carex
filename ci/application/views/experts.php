<?php $this->view('header'); ?>
  










  <section class="auto-blog-video">
            <div class="container">
                <div class="car-listing exp-listing">
                    <h2 class="h3" align="center"> Наши лучшие эксперты </h2>
                    
                   
 	<div class="row expert-listing">
        <div class="col-sm-12"><h3>
                            Поиск экспертов
                        </h3>
                <form class="form-inline" action="">
                    <div class="form-group">
                        <select class="sel" name="ui.city">
        					<option value="">Город</option>
        					<?php foreach($cities as $c): ?>
        						<option 
        							value="<?php echo $c; ?>"
        							<?php if(isset($_GET['ui_city']) && $c == $_GET['ui_city']) echo 'selected'; ?>
        						><?php echo $c; ?></option>
        					<?php endforeach; ?>
        				</select>
                    </div>
                    <div class="form-group">
                        <select class="sel" name="e.specializations">
        					<option value="">Специализация</option>
        					<?php foreach($specializations as $s): ?>
        						<option 
        							value="<?php echo $s['id']; ?>"
        							<?php if(isset($_GET['e_specializations']) && $s['id'] == $_GET['e_specializations']) echo 'selected'; ?>
        						><?php echo $s['name']; ?></option>
        					<?php endforeach; ?>
        				</select>
                    </div>
                    <button type="submit" class="btn btn-success">Найти эксперта</button>
                </form></div>
    </div>


		 <div class="row expert-list">
		<?php foreach($experts as $e): ?>

			
			<div class="col-sm-4 slide-wrap ">
            	<article class="auto-blog expert-item">


	                <figure class="video-wrap">
	                    <div class="expert-photo" style="background: url(<?php echo $e['image']; ?>) no-repeat center;background-size: cover;">
	                    </div>

	                </figure>
	                <div class="img-description">
	                    <h3><a href="/in/expert/profile?id=<?php echo $e['user_id']; ?>"><?php echo $e['name'] ?>, <?php echo $e['city'] ?></a> 
	                    <?php if($e['checked']): ?>
	                    	<i class="fa fa-check"></i>
	                	<?php endif; ?>
	                    </h3>
	                    <p style="height:40px;"> 
	                    	<?php 
		                    	// $sv = array();
		                    	// $inx = 0;
		                    	// foreach($e['services'] as $s){
		                    	// 	if($inx > 2) break;
		                    	// 	$sv []=  $s['name']; 
		                    	// 	$inx++;
		                    	// }
		                    	// echo implode(', ', $sv);
	                    		$sp = array();
	                    		$sp_i = explode(',', $e['specializations']);
	                    		foreach($sp_i as $i)
	                    			foreach($specializations as $spec)
	                    				if($spec['id'] == $i)
	                    					$sp []= $spec['name'];
	                    		echo implode(', ', $sp);

	                    	?>
	                        <br> 
	                    </p> 

	                    <ul class="exp-rate">
	                        <?php 
		                    	for($i = 0; $i < 5; $i++){
		                    		if(intval($e['rating']) > $i)
		                    			echo '<li><i class="fa fa-star"></i></li>';
		                    		else
		                    			echo '<li><i class="fa fa-star fa-star-o"></i></li>';
		                    	}
		                     ?>
	                    </ul>
	                    

	                    <a href="/expert?id=<?php echo $e['user_id']; ?>" class="btn btn-default "> Подробнее  </a>
	                </div>
	            </article>
	        </div>
		<?php endforeach; ?>
	</div>

                  
               
                
                </div>
            </div>
        </section>





<section class="car-banner parallax" style="background-position: 50% -76.6px;">
    <div class="container">
      <div class="">
        <h2 class="h1"> Ищешь тачку? </h2>
        <span class="sub-heading">Проверенная база <br> более 10 000 подержанных автомобилей от наших экспертов</span> 
        
        <a href="#" class="btn btn-effect btn-default" data-toggle="modal" data-target="#myModal-quest"> Запрос эксперту <i> <img src="../assets/img/arrow-btn.png" alt=""> </i> </a> </div>
    </div>
  </section>



 <!-- Вопрос эксперту -->
<div class="modal fade" id="myModal-quest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Задайте вопрос эксперту </h4>
        
      </div>
      <div class="modal-body">
        <div class="exp-login exp-question" >
            <input class="exp-name" type="hidden" value="Вопрос со страницы всех экспертов">
            <input class="name" type="text" placeholder="Имя *" required>
            <input class="phone" type="text" placeholder="Телефон *" required>
            <input class="email" type="text" placeholder="Email">
            <textarea class="meassage"  cols="30" rows="5" placeholder="Вопрос"></textarea>
            <button class="btn btn-default send-q">Отправить</button>
            
        </div>
      </div>
     
        
        
      </div>
    </div>
  </div>    


<?php $this->view('footer'); ?>
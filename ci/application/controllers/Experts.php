<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Experts extends CI_Controller {
	public function index()
	{
		$this->load->helper('expert');
		$this->load->view('experts', array(
			'experts' => Exp::filter($_GET),
			'cities' => Exp::cities(),
			'specializations' => Exp::specializations(),
		));
	}
}

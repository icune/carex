<?php
$lang['russkii'] = "Русский";
$lang['imja_i_familija'] = "Имя и фамилия";
$lang['telefon'] = "Телефон";
$lang['registracija_'] = "Регистрация компании";
$lang['kak_vas_zovut'] = "Как вас зовут?";
$lang['kalkuljator_'] = "Калькулятор ";
$lang['konsultacija_'] = "Консультация ";
$lang['vash_telefon'] = "Ваш телефон";
$lang['priglashaem_k_nam'] = "Приглашаем к нам";
$lang['vy_urist'] = "Вы юрист?";
$lang['rub'] = "руб.";
$lang['registracija_kompanii'] = "Регистрация компании";
$lang['otkrytie_rasczetnogo_sczeta'] = "Открытие расчетного счета";
$lang['0'] = "руб.";
$lang['mesjac'] = "месяц";
$lang['buhgalterskoe_soprovozhdenie'] = "Бухгалтерское сопровождение";
$lang['uridiczeskoe_soprovozhdenie'] = "Юридическое сопровождение";
$lang['podgotovka_korporativnogo_dogovora'] = "Подготовка корпоративного договора";
$lang['sozdanie_motivacionnoi_shemy'] = "Создание мотивационной схемы";
$lang['service_page_main_rus_title'] = "Регистрация <br> бизнеса в России";
$lang['service_page_main_rus_text'] = "
                        Планируете начать свой бизнес, но не уверены, что для Вас лучше: общесто с ограниченной ответственностью или статус индивидуального предпринимателя? Мы поможем найти решение, которое подходит именно Вам и зарегистрировать удобную для Вас организационно-правовую форму
                        ";
$lang['service_page_main_btn'] = "Отправить запрос";
$lang['service_page_advantages_title'] = "ПРЕИМУЩЕСТВА НАШИХ УСЛУГ";
$lang['service_page_advantages_sub-title'] = "";
$lang['service_page_advantages_adv1-title'] = "Конфиденциальность";
$lang['service_page_advantages_adv1-text'] = "Все Ваши документы хранятся в защищенном офисе Legal Space.";
$lang['service_page_advantages_adv2-title'] = "ОПЛАТА ПО РЕЗУЛЬТАТУ";
$lang['service_page_advantages_adv2-text'] = "Вы осуществляете денежный перевод только когда работа выполнена в полном объеме";
$lang['service_page_advantages_adv3-title'] = "ИНДИВИДУАЛЬНЫЙ ПОДХОД";
$lang['service_page_advantages_adv3-text'] = "Заказывая услуги у нас, Вы получаете безупречный сервис, плюс — персонального менеджера";
$lang['service_page_advantages_adv4-title'] = "ON-LINE СОПРОВОЖДЕНИЕ";
$lang['service_page_advantages_adv4-text'] = "Совершайте все необходимые операции и ведите документооборот прямо на нашем сайте";
$lang['service_page_advantages_adv5-title'] = "ЛУЧШИЕ ЦЕНЫ";
$lang['service_page_advantages_adv5-text'] = "Работа on-line позволяет нам избежать крупных издержек обычных юридических компании";
$lang['service_page_advantages_adv6-title'] = "МЫ РАБОТАЕМ ПО ВЫХОДНЫМ";
$lang['service_page_advantages_adv6-text'] = "Предпочитаете зарабатывать, пока конкуренты отдыхают? Значит мы с Вами за одно!";
$lang['service_page_register_title'] = "Хотите зарегистрировать компанию? ";
$lang['service_page_register_sub_title'] = "Получите первую консультацию абсолютно бесплатно";
$lang['service_page_register_btn'] = "Отправить запрос";
$lang['service_page_calc_rus_title'] = "Регистрация ооо в Москве";
$lang['service_page_calc_sub_title'] = "";
$lang['service_page_calc_final'] = "ИТОГО:";
$lang['service_page_calc_ready'] = "Документы будут готовы <br> через <b class=\"over-days\">15 дней</b>";
$lang['service_page_calc_week_day1'] = "ПН";
$lang['service_page_calc_week_day2'] = "ВТ";
$lang['service_page_calc_week_day3'] = "СР";
$lang['service_page_calc_week_day4'] = "ЧТ";
$lang['service_page_calc_week_day5'] = "ПТ";
$lang['service_page_calc_week_day6'] = "СБ";
$lang['service_page_calc_week_day7'] = "ВС";
$lang['service_page_calc_fina_btn'] = "Зарегистрировать компанию";
$lang['service_page_faq_title'] = "F.A.Q.";
$lang['service_page_faq_sub_title'] = "";
$lang['service_page_faq_q1_title'] = "ЧТО МНЕ НУЖНО СДЕЛАТЬ, ЧТОБЫ ЗАРЕГИСТРИРОВАТЬ КОМПАНИЮ?";
$lang['service_page_q1_text'] = "
                          От Вас требуется заполнить небольшую форму и передать нам всего несколько документов. Дальше действовать будем мы.
                        ";
$lang['service_page_faq_q2_title'] = "КАК ДОЛГО ЗАНИМАЕТ ПРОЦЕСС РЕГИСТРАЦИИ КОМПАНИИ?";
$lang['service_page_faq_q2_text'] = "
                        Срок подготовки документов зависит от того, какие опции Вы выбрали. Срок регистрации в налоговой составляет 5 рабочих дней.
                        ";
$lang['service_page_faq_q3_title'] = " КАК ПРОИЗВОДИТСЯ ОПЛАТА УСЛУГ?";
$lang['service_page_faq_q3_text'] = "
                        Вы оплачиваете компанию через сайт Legal Space. Если Компания Вам не передана (хотя таких случаев еще не было), то мы возвращаем Вам всю сумму полностью.

                        ";
$lang['main_page_offices_h2'] = "Контакты";
$lang['main_page_offices_subtitle'] = "
                  Приглашаем Вас в наш офис в центре Москвы
                ";
$lang['main_page_map_adr2_title'] = "Legalspace — Москва";
$lang['main_page_adr2_adress'] = "Пресненский вал, 15";
$lang['main_page_adr2_phone'] = "8 (800) 333-67-36";
$lang['service_page_anyquestion_title'] = "Остались вопросы?";
$lang['service_page_anyquestion_sub_title'] = "Оставьте свой номер и мы Вам перезвоним";
$lang['service_page_anyquestion_btn'] = "отправить запрос";
$lang['footer_info'] = " <b>Legal Space</b> — это безопасная и надежная виртуальная площадка,
                                                       <br> которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.";
$lang['footer_copyright'] = "© 2015 Legal Space Ltd., Все права защищены";

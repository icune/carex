<?php
$lang['bystro._udobno._bezopasno'] = 'Быстро. Удобно. Безопасно';
$lang['my_pomozhem_naiti_vam_urista_za_granicei'] = 'Мы поможем найти Вам юриста за границей';
$lang['s_nami_rabotaet_bolee_'] = 'С нами работает более ';
$lang['uristov_po_vsemu_miru'] = 'юристов по всему миру';
$lang['vy_mozhete_poluczit_lubye_uridiczeskie_uslugi_v_luboi_toczke_mira_prjamo_seiczas_na_nashem_saite._ostavte_zajavku_i_my_predostavim_vam_specialista_s_vysoczaishei_kvalifikaciei_imenno_po_vashemu_voprosu.'] = 'Вы можете получить любые юридические услуги в любой точке мира прямо сейчас на нашем сайте. Оставьте заявку и мы предоставим Вам специалиста с высочайшей квалификацией именно по Вашему вопросу.';
$lang['prosto_vyberite_stranu'] = 'Просто выберите страну';
$lang['v_kotoroi_vam_nuzhen_urist_i_opishite_vashu_zadaczu'] = 'в которой Вам нужен юрист и опишите Вашу задачу';
$lang['esli_u_nas_vozniknut_dopolnitelnye_voprosy'] = 'Если у нас возникнут дополнительные вопросы';
$lang['neobhodimye_dlja_vybora_specialista'] = 'необходимые для выбора специалиста';
$lang['my_vam_perezvonim'] = 'мы Вам перезвоним';
$lang['my_naidem_podhodjasczego_urista_'] = 'Мы найдем подходящего юриста ';
$lang['opytom_reshenija_imenno_vashei_zadaczi'] = 'опытом решения именно Вашей задачи';
$lang['kazhdyi_urist_'] = 'Каждый юрист ';
$lang['prohodit_tsczatelnyi_otbor'] = 'проходит тщательный отбор';
$lang['poetomu_my_polnostu_uvereny_v_ego_professionalizme_i_ruczaemsja_za_kaczestvo_okazannyh_im_uslug.'] = 'поэтому мы полностью уверены в его профессионализме и ручаемся за качество оказанных им услуг.';
$lang['obsczaites_s_uristombrprjamo_v_czate_'] = 'Общайтесь с юристом<br>прямо в чате ';
$lang['vy_obsuzhdaete_voznikausczie_voprosy_i_utverzhdaete_zadaczi_i_stoimost'] = 'Вы обсуждаете возникающие вопросы и утверждаете задачи и стоимость';
$lang['obsczenie_prohodit_brv_lubom_udobnombr_dlja_vas_formate'] = 'Общение проходит <br>в любом удобном<br> для Вас формате';
$lang['vy_mozhete_perepisyvatsja_s_uristom_v_nashem_czate'] = 'Вы можете переписываться с юристом в нашем чате';
$lang['pozvonit_emu_iz_czata'] = 'позвонить ему из чата';
$lang['libo_sdelat_video'] = 'либо сделать видео';
$lang['zvonok'] = 'звонок';
$lang['naiti_urista'] = 'Найти юриста';
$lang['bezopasno_provodite_platezhi_czerez_nash_sait'] = 'Безопасно проводите платежи через наш сайт';
$lang['dengi_s_vashego_sczeta_budut_spisany_tolko_posle_podtverzhdenija_vami_uspeshnogo_vypolnenenija_zadaczi_uristom.'] = 'Деньги с Вашего счета будут списаны только после подтверждения Вами успешного выполненения задачи юристом.';
$lang['my_rabotaem_pri_podderzhke'] = 'Мы работаем при поддержке';
$lang['ja_urist'] = 'Я юрист';
$lang['priglashaem_k_nam'] = 'Приглашаем к нам';
$lang['eto_bezopasnaja_i_nadezhnaja_virtualnaja_plosczadka'] = 'это безопасная и надежная виртуальная площадка';
$lang['kotoraja_pomozhet_vam_reshit_lubuu_uridiczeskuu_zadaczu_v_kratczaishie_sroki.'] = 'которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.';
$lang['vse_prava_zasczisczeny'] = 'Все права защищены';
$lang['tovarnye_znaki_i_intellektualnaja_sobstvennost'] = 'Товарные знаки и интеллектуальная собственность';
$lang['nedvizhimost'] = 'Недвижимость';
$lang['korporativnoe_pravo_i_'] = 'Корпоративное право и ';
$lang['czastnyi_kapital'] = 'Частный капитал';
$lang['mezhdunarodnoe_nalogovoe_i_korporativnoe_strukturirovanie'] = 'Международное налоговое и корпоративное структурирование';
$lang['nalogi'] = 'Налоги';
$lang['investicii'] = 'Инвестиции';
$lang['uczrezhdenie_kompanii'] = 'Учреждение компаний';
$lang['investicionnye_fondy'] = 'Инвестиционные фонды';
$lang['restrukturizacii_i_bankrotstvo'] = 'Реструктуризации и банкротство';
$lang['trudovoe_pravo'] = 'Трудовое право';
$lang['antimonopolnoe_zakonodatelstvo'] = 'Антимонопольное законодательство';
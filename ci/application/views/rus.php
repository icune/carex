<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">

</html>
<!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />


<head profile="http://www.w3.org/2005/10/profile">
    <link rel="icon" type="image/png" href="favicon.png" />
    <meta charset="UTF-8" />
    <script type="text/javascript">
        window.lang = <?php echo json_encode($_, true); ?>;
        window.curlang = '<?php echo $curlang; ?>';
        window.page_from = '{page_rus}';
    </script>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/fland/css/bootstrap-grid-3.3.1.min.css" />
    <link rel="stylesheet" href="/fland/css/font.css" />
    <link rel="stylesheet" href="/fland/less/style-serv.css" />
    <link rel="stylesheet" href="/fland/css/wiggy.css" />
    <link rel="stylesheet" href="/plug/cs/css/countrySelect.css">
    <script src="/js/jquery-2.1.4.js"></script>
    <title>LegalSpace</title>
    <?php $this->view('analytics'); ?>
    <!-- /Yandex.Metrika counter -->
</head>

<body>
<?php $this->view('header'); ?>
<a href="/login" style="
            position: absolute;
            color: white;
            left: 10px;
            top: 14px;
            z-index: 200;
        ">
        <?php if($curlang == 'english'): ?>
            Login
        <?php else: ?>
            Вход
        <?php endif; ?>
        </a>

    <section class="bl-top-form rus">

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <div class="logo">
                      <a href="/">
                        <img src="/fland/img/service/logo.png" alt="">
                      </a>
                    </div>
                </div>

                <div class="select-lang">
                            
                            <ul class="lang-list">
                                <li 
                                <?php $href = '/lang/ru/'; if($curlang == 'russian'){ echo 'class="active"'; $href = '';}; ?>
                                ><a href="<?php echo $href; ?>">Русский</a></li>
                                <li 
                                <?php $href = '/lang/en/'; if($curlang == 'english'){ echo 'class="active"'; $href = '';}; ?>
                                ><a href="<?php echo $href; ?>">English</a></li>
                            </ul>
                        </div>

            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="top-form-title">
                        <?php echo $_["service_page_main_rus_title"]; ?>
                    </div>
                    <div class="text">
                        <?php echo $_["service_page_main_rus_text"]; ?>
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-1">
                    <form id="top-form" class="top-form">
                        <input class="form-inp" name="FnameLname" type="text" placeholder="<?php echo $_["imja_i_familija"]; ?>">
                        <input class="form-inp" name="Phone" type="text" placeholder="<?php echo $_["telefon"]; ?>">
                        <input class="form-inp" name="Email" type="text" placeholder="E-mail">
                        <button class="custom-btn">
                            <?php echo $_["service_page_main_btn"]; ?>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="service-page advantages">
        <div class="container">
            <div class="row">
                <h2 class="title">
                    <?php echo $_["service_page_advantages_title"]; ?>
                </h2>
                <div class="sub-title">
                    <?php echo $_["service_page_advantages_sub-title"]; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 adv-bg1">
                    <div class="adv ">
                        <div class="adv-title"><?php echo $_["service_page_advantages_adv1-title"]; ?></div>
                        <?php echo $_["service_page_advantages_adv1-text"]; ?></div>
                </div>
                <div class="col-sm-6 adv-bg2">
                    <div class="adv ">
                        <div class="adv-title"><?php echo $_["service_page_advantages_adv2-title"]; ?></div>
                        <?php echo $_["service_page_advantages_adv2-text"]; ?></div>
                </div>
                <div class="col-sm-6 adv-bg3">
                    <div class="adv ">
                        <div class="adv-title"><?php echo $_["service_page_advantages_adv3-title"]; ?></div>
                        <?php echo $_["service_page_advantages_adv3-text"]; ?></div>
                </div>
                <div class="col-sm-6 adv-bg4">
                    <div class="adv ">
                        <div class="adv-title"><?php echo $_["service_page_advantages_adv4-title"]; ?></div>
                        <?php echo $_["service_page_advantages_adv4-text"]; ?></div>
                </div>
                <div class="col-sm-6 adv-bg5">
                    <div class="adv ">
                        <div class="adv-title"><?php echo $_["service_page_advantages_adv5-title"]; ?></div>
                        <?php echo $_["service_page_advantages_adv5-text"]; ?></div>
                </div>
                <div class="col-sm-6 adv-bg6">
                    <div class="adv ">
                        <div class="adv-title"><?php echo $_["service_page_advantages_adv6-title"]; ?></div>
                        <?php echo $_["service_page_advantages_adv6-text"]; ?></div>
                </div>
            </div>
        </div>

    </section>
    
    

    <!-- <?php echo $_["registracija_"]; ?>-->
    <section class="service-page-register rus">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title">
                        <?php echo $_["service_page_register_title"]; ?>
                    </h2>
                    <div class="sub-title">
                        <?php echo $_["service_page_register_sub_title"]; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <form id="middle-form">
                    <div class="col-sm-4"> <input name="name" type="text" class="form-inp" placeholder="<?php echo $_["kak_vas_zovut"]; ?>"> </div>
                    <div class="col-sm-4"> <input name="email" type="text" class="form-inp" placeholder="E-mail"> </div>
                    <div class="col-sm-4">
                        <button class="custom-btn"><?php echo $_["service_page_register_btn"]; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    
   
    <!-- <?php echo $_["kalkuljator_"]; ?>-->
    
    <section class="service-calc">
        <div class="container">
            <div class="row">
                <h2 class="title">
                    <?php echo $_["service_page_calc_rus_title"]; ?>
                </h2>
                <div class="sub-title">
                   <?php echo $_["service_page_calc_sub_title"]; ?>
                </div>
            </div>
            <div class="row">
                <form class="calc">
                    <div class="main-calc">

                          <div class="calc-elems">
                            
                          </div>

                            <div class="calc-box final">
                                <div class="final-sum">
                                    <?php echo $_["service_page_calc_final"]; ?> <i class="fr"> 0$</i>
                                </div>
                            </div>
                            
                            <div class="calc-box calendar">
                               
                               <div class="calendar-title">
                                   <?php echo $_["service_page_calc_ready"]; ?>
                               </div>
                               
                              
                                   <table class="month">
                                      <thead>
                                       <tr>
                                           <th><?php echo $_["service_page_calc_week_day1"]; ?></th>
                                           <th><?php echo $_["service_page_calc_week_day2"]; ?></th>
                                           <th><?php echo $_["service_page_calc_week_day3"]; ?></th>
                                           <th><?php echo $_["service_page_calc_week_day4"]; ?></th>
                                           <th><?php echo $_["service_page_calc_week_day5"]; ?></th>
                                           <th><?php echo $_["service_page_calc_week_day6"]; ?></th>
                                           <th><?php echo $_["service_page_calc_week_day7"]; ?></th>
                                       </tr>
                                       </thead>
                                       <tbody class="month-days">
                                       <tr>
                                           <td></td>
                                           <td></td>
                                           <td></td>
                                           <td></td>
                                           <td></td>
                                           <td></td>
                                           <td></td>
                                       </tr>
                                       <tr>
                                           <td class="another-mounth">28</td>
                                           <td class="another-mounth">29</td>
                                           <td class="another-mounth">30</td>
                                           <td>31</td>
                                           <td >1</td>
                                           <td>2</td>
                                           <td>3</td>
                                       </tr>
                                       <tr>
                                           <td>4</td>
                                           <td>5</td>
                                           <td>6</td>
                                           <td>7</td>
                                           <td>8</td>
                                           <td>9</td>
                                           <td>10</td>
                                       </tr>
                                       <tr>
                                           <td>11</td>
                                           <td>12</td>
                                           <td>13</td>
                                           <td>14</td>
                                           <td>15</td>
                                           <td>16</td>
                                           <td class="active">17</td>
                                       </tr>
                                       <tr>
                                           <td>18</td>
                                           <td>19</td>
                                           <td>20</td>
                                           <td>21</td>
                                           <td>22</td>
                                           <td>23</td>
                                           <td>24</td>
                                       </tr>
                                       <tr>
                                           <td>25</td>
                                           <td>26</td>
                                           <td>27</td>
                                           <td>28</td>
                                           <td>29</td>
                                           <td>30</td>
                                           <td>31</td>
                                       </tr>
                                       </tbody>
                                   </table>
                               
                               
                               
                            </div>
                            <div class="calc-box final-btn">
                                <button class="custom-btn">
                                    <?php echo $_["service_page_calc_fina_btn"]; ?>
                                </button>
                            </div>
                            
                    </div>
                </form>
            </div>
        </div>
    </section>
    

    <!-- FAQ -->
    <section class="service-faq">
        <div class="container">
            <div class="row">
                <h2 class="title"><?php echo $_["service_page_faq_title"]; ?></h2>
                <div class="sub-title"><?php echo $_["service_page_faq_sub_title"]; ?></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="question">
                        <div class="q-title">
                            <div>
                                <?php echo $_["service_page_faq_q1_title"]; ?>
                            </div>
                        </div>
                        <?php echo $_["service_page_q1_text"]; ?>

                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="question">
                        <div class="q-title">
                            <div>
                                <?php echo $_["service_page_faq_q2_title"]; ?>
                            </div>
                        </div>
                        <?php echo $_["service_page_faq_q2_text"]; ?>

                    </div>
                </div>
            </div>
            
            
            <div class="row">
               
                <div class="col-sm-6">
                    <div class="question">
                        <div class="q-title">
                            <div>
                               <?php echo $_["service_page_faq_q3_title"]; ?>
                            </div>
                        </div>
                        <?php echo $_["service_page_faq_q3_text"]; ?>

                    </div>
                    
                    
                    
                    
                </div>
                
                
                
                
            </div>
            
        </div>
    </section>
    
    
    
    




     <!-- <?php echo $_["konsultacija_"]; ?>-->
    <section class="service-page-konsult">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="title">
                        <?php echo $_["service_page_anyquestion_title"]; ?>
                    </h2>
                    <div class="sub-title">
                        <?php echo $_["service_page_anyquestion_sub_title"]; ?>
                    </div>
                </div>
                <div class="col-sm-6 form-blok">
                    <form class="konsult-form" action="">
                        <input class="kons-phone" type="text" placeholder="<?php echo $_["vash_telefon"]; ?>" value="">
                        <button class="custom-btn" type="submit">
                            <?php echo $_["service_page_anyquestion_btn"]; ?>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
   <footer>
        <div class="brand-bank-block">
            <div class="container">
                <div class="col-sm-6 col-sm-offset-3">
                    <a><img src="/fland/img/icon-bank-1.png" alt="" class="img-responsive" />
                    </a>
                    <a><img src="/fland/img/icon-bank-2.png" alt="" class="img-responsive" />
                    </a>
                    <a><img src="/fland/img/icon-bank-3.png" alt="" class="img-responsive" />
                    </a>
                </div>
            </div>
        </div>
        <div class="footer-bot">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="copyright-notation">
                           <?php echo $_["footer_info"]; ?>
                            <p>
                                <?php echo $_["footer_copyright"]; ?> </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="soc-info">


                                                        <div class="offer"><?php echo $_["priglashaem_k_nam"]; ?></div> <a href="/lawyer" class="custom-btn"><?php echo $_["vy_urist"]; ?></a>                        
                            <a target="_blank" href="https://www.facebook.com/Legal-Space-468707986637000/timeline/"><i class="foot-icons fb"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/company/9340358?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9340358%2Cidx%3A1-1-1%2CtarId%3A1441991163447%2Ctas%3Alegal%20space"><i class="foot-icons linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--[if lt IE 9]>
  <script src="js/es5-shim.min.js"></script>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/html5shiv-printshiv.min.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
    <script type="text/javascript">
        var images = [];
    </script>
    
    <script type="text/javascript" src="/plug/cs/js/countrySelect.js"></script>
    <script src="/js/navigation.js"></script>
    <script src="/js/jquery.vide.js"></script>
    <script src="/js/common.js"></script>
    <script src="/fland/js/script.js"></script>
    <script src="/js/wiggy.js"></script>
    <script src="/js/video.js"></script>
    <script src="/js/net.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <div class="popup" style="display:none">
        <input type="text" class="cselect-internal" val="Choose a country" name="country" />
        <input type="text" name="name" class="wiggy white" placeholder="Your name" />
        <input type="text" name="email" class="wiggy white" placeholder="Email" />
        <input type="text" name="phone" class="wiggy white" placeholder="Contact phone number" value="" />
        <textarea class="wiggy white" name="message" placeholder="Describe your legal matter"></textarea>
        <div class="btn" style="width:100%">
            Send </div>
    </div>
    <script type="text/javascript">
      $(function(){
        Calculator({currency:'<?php echo $_["rub"]; ?>', services:[
          {
            name:'<?php echo $_["registracija_"]; ?>',
            price: '20 000',
            hours:7*30
          },
          {
            name:'<?php echo $_["otkrytie_rasczetnogo_sczeta"]; ?>',
            price: '12 000 <?php echo $_["rub"]; ?>',
            cur: true,
            hours:12*12
          },
          {
            name:'<?php echo $_["buhgalterskoe_soprovozhdenie"]; ?>',
            price: '15 000 <?php echo $_["rub"]; ?>/<?php echo $_["mesjac"]; ?>',
            cur: true,
            hours:0*30
          },
          {
            name:'<?php echo $_["uridiczeskoe_soprovozhdenie"]; ?>',
            price: '18 000 <?php echo $_["rub"]; ?>/<?php echo $_["mesjac"]; ?>',
            cur: true,
            hours:0*30
          },
          {
            name:'<?php echo $_["podgotovka_korporativnogo_dogovora"]; ?>',
            price: '70 000',
            hours:24*10
          },
          {
            name:'<?php echo $_["sozdanie_motivacionnoi_shemy"]; ?>',
            price: '200 000',
            hours:24*15
          }
        ]});
      })
    </script>
<?php $this->view('jivosite'); ?>
<?php $this->view('modal', array('ninc' => true)); ?>
</body>
</html>
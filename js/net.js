window.Net = {
	post: function(url, data, cb){
		if(!data) data = {};
		if(!cb) cb = {};
		$.ajax({
			url: url,
			type: 'post',		
			dataType: 'json',
			data: data,
			success:function(r){
				if(r.status == 'ok'){		
					if(cb.success instanceof Function) cb.success(r.msg);
				}else{
					if(cb.error instanceof Function) cb.error(r.msg)
				}
			},
			error:function(r){if(cb.error instanceof Function) cb.error(r.msg)}
		});
	}
};
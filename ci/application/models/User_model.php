<?php 

class User_model extends CI_model{
	/*
		$user - user with required fields set in $fields
		!!THROWS
	*/
	public function insert($user){
		$fields = array('name', 'surname', 'patronym', 'email');
		$_user = array();
		foreach($fields as $f){
			if(!isset($user[$f]))
				throw new Exception($f . ' absent');
			if($f != 'dir') 
				$_user[$f] = $user[$f];
		}

		if(!isset($user['dir'])){
			$user['dir'] = array();
		}
		
		$this->db->insert('user', $_user);
		$user_id = $this->db->insert_id();
		if(count($user['dir'])){
			$dir_batch = array_map(function($dir_id) use($user_id){
				return array(
					'user_id' => $user_id,
					'dir_id' => $dir_id
				);
			}, $user['dir']);

			$this->db->insert_batch('user_dir', $dir_batch);
		};
	}
	/*
		$users - array of users, where set changed fields. Required param - id
		!!THROWS
	*/
	public function update($users){
		foreach($users as $u)
			if(!isset($u['id']))
				throw new Exception("one of user hasnt id");

		$ids = array_map(function($u){
			return $u['id'];
		}, $users);

		$dir_sql = 'SELECT user_id, GROUP_CONCAT(dir_id) dir_id FROM user_dir WHERE ';
		$dir_sql .= implode(' OR ', $ids). ' ';
		$dir_sql .= 'GROUP BY user_id';
		$dir_raw = $this->db->query($dir_sql)->result_array();
		$dir_check = array();
		foreach($dir_raw as $dr)
			$dir_check[$dr['user_id']] = explode(',', $dr['dir_id']);

		foreach($users as $u){
			$u_dir = array();
			if(isset($u['dir'])){
				$u_dir = $u['dir'];
				unset($u['dir']);
			}
			$user_id = $u['id'];
			unset($u['id']);

			if(count(array_keys($u)))
				$this->db->update('user', $u, array('id'=>$user_id));
			if(isset($dir_check[$user_id]))
				$dc = $dir_check[$user_id];
			else
				$dc = array();
			$for_del = array();
			foreach ($dc as $dcc) {
				if(!in_array($dcc, $u_dir))
					$for_del []= $dcc;
			}
			$for_ins = array();
			foreach($u_dir as $ud)
				if(!in_array($ud, $dc)){
					$for_ins []= array(
						'user_id' => $user_id,
						'dir_id' => $ud
					);
				}
			if(count($for_del))
				$this->delete_batch('user_dir', 'dir_id', $for_del, "user_id=$user_id");
			if(count($for_ins))
				$this->db->insert_batch('user_dir', $for_ins);
		}
	}
	/*
		$users - array of ids
	*/
	public function delete($users){
		$this->delete_batch('user', 'id', $users);
		$this->delete_batch('user_dir', 'user_id', $users);
	}
	public function get($filter, $order = null, $limit = null){
		if(!$filter || !count($filter))
			$filter = null;
		if(!$order)
			$order = 'create_ts ';

		$order = $this->db->escape($order);
		$order = substr($order, 1, strlen($order)-2);
		$limit_sql = '';
		if($limit)
			$limit_sql = "LIMIT $limit[0],$limit[1]";
		
			

		$sql = "SELECT id, count(ud.dir_id) cnt FROM user u ";
		$sql .= "LEFT OUTER JOIN user_dir ud ON u.id=ud.user_id ";
		if($filter){
			$ordir = '';
			$ors = array();
			foreach($filter as $f)
				$ors []= "dir_id=$f";
			$ordir = implode(' OR ', $ors);
			$sql .= "WHERE ";
			$sql .= $ordir;
		}
		$sql .= " GROUP BY u.id";
		if($filter)
			$sql .= " HAVING cnt=".count($filter);
		
		
		
		$r = $this->db->query($sql)->result_array();

		if(!count($r))
			return array();

		$ids = array();
		foreach($r as $u){
			$ids []= 'u.id='.$u['id'];
		}

		$sql = $sql = 'SELECT u.*, GROUP_CONCAT(ud.dir_id) dir FROM user u ';
		$sql .= 'LEFT OUTER JOIN user_dir ud ON u.id=ud.user_id ';
		$sql .= 'WHERE '.implode(' OR ', $ids) . ' ';
		$sql .= " GROUP BY u.id " ;
		$sql .= 'ORDER BY '.$order . ' ' ;
		$sql .= $limit_sql . ' ' ;

		return $this->db->query($sql)->result_array();
	}
	public function get_count($filter){
		if(!$filter || !count($filter))
			$filter = null;


		$sql = "SELECT count(ud.dir_id) cnt FROM user u ";
		$sql .= "LEFT OUTER JOIN user_dir ud ON u.id=ud.user_id ";
		if($filter){
			$ordir = '';
			$ors = array();
			foreach($filter as $f)
				$ors []= "dir_id=$f";
			$ordir = implode(' OR ', $ors);
			$sql .= "WHERE ";
			$sql .= $ordir;
		}
		$sql .= " GROUP BY u.id";
		if($filter)
			$sql .= " HAVING cnt=".count($filter);

		$sql = "SELECT count(*) cnt FROM ($sql) t";

		$r = $this->db->query($sql)->result_array();
		return $r[0]['cnt'];
	}

	public function dirs(){
		return $this->db->query("SELECT * FROM dir")->result_array();
	}

	private function delete_batch($table, $field, $values, $and = null){
		$db = $this->db;
		$sql = "DELETE FROM $table WHERE (".implode(' OR ', array_map(
					function($v) use ($db, $field){
						return "$field=".$db->escape($v);
					}, 
				$values)
			).')' . ($and ?  " AND $and" : '');
		$this->db->query($sql);
	}
}
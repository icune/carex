<?php
$lang['russkii'] = "Русский";
$lang['imja_i_familija'] = "Имя и фамилия";
$lang['telefon'] = "Телефон";
$lang['registracija_'] = "Национальная регистрация товарного знака";
$lang['kak_vas_zovut'] = "Как вас зовут?";
$lang['pakety_i_tarify_'] = "Пакеты и тарифы ";
$lang['registracija_tovarnogo_znaka_v_nacionalnom_vedomstve_kazhdogo_vybrannogo_gosudarstva'] = "Национальная регистрация товарного знака";
$lang['goda'] = "года";
$lang['konsultacija_'] = "Консультация ";
$lang['vash_telefon'] = "Ваш телефон";
$lang['priglashaem_k_nam'] = "Приглашаем к нам";
$lang['vy_urist'] = "Вы юрист?";
$lang['service_page_main_tm_title'] = "РЕГИСТРАЦИЯ ТОВАРНОГО ЗНАКА";
$lang['service_page_main_tm_text'] = "
                            Вы занимаетесь экспортом товара? или только собираетесь выйти на новый рынок с своим товаром? беспокоитесь о безопасности Вашего бренда? Мы поможем защитить Ваши интересы.
                        ";
$lang['service_page_main_btn'] = "Отправить запрос";
$lang['service_page_advantages_title'] = "ПРЕИМУЩЕСТВА НАШИХ УСЛУГ";
$lang['service_page_advantages_sub-title'] = "Планируете зарегистрировать торговую марку?";
$lang['service_page_advantages_adv1-title'] = "Конфиденциальность";
$lang['service_page_advantages_adv1-text'] = "Все Ваши документы хранятся в офисе Legal Space на Кипре";
$lang['service_page_advantages_adv2-title'] = "ОПЛАТА ПО РЕЗУЛЬТАТУ";
$lang['service_page_advantages_adv2-text'] = "Вы осуществляете денежный перевод только когда работа выполнена в полном объеме";
$lang['service_page_advantages_adv3-title'] = "ИНДИВИДУАЛЬНЫЙ ПОДХОД";
$lang['service_page_advantages_adv3-text'] = "Заказывая услуги у нас, вы получаете безупречный сервис, плюс — персонального менеджереа";
$lang['service_page_advantages_adv4-title'] = "ON-LINE СОПРОВОЖДЕНИЕ";
$lang['service_page_advantages_adv4-text'] = "Совершайте все необходимые операции и ведите документооборот прямо на нашем сайте";
$lang['service_page_advantages_adv5-title'] = "ЛУЧШИЕ ЦЕНЫ";
$lang['service_page_advantages_adv5-text'] = "Работа on-line позволяет нам избежать крупных издержек обычных юридических компании";
$lang['service_page_advantages_adv6-title'] = "МЫ РАБОТАЕМ ПО ВЫХОДНЫМ";
$lang['service_page_advantages_adv6-text'] = "Предпочитаете зарабатывать, пока конкуренты отдыхают? Значит мы с Вами за одно!";
$lang['service_page_register_title'] = "Хотите зарегистрировать товарный знак? ";
$lang['service_page_register_sub_title'] = "Получите первую консультацию абсолютно бесплатно";
$lang['service_page_register_btn'] = "Отправить запрос";
$lang['service_page_tarifs_title'] = "Пакеты и тарифы";
$lang['service_page_tarifs_sub-title'] = "Налетай выбирай описание как это работает";
$lang['service_page_tarifs_cost'] = "Стоимость";
$lang['service_page_tarifs_indcleded'] = "Включено";
$lang['service_page_tarifs_patent'] = "Поиск по базе  <br> товарных знаков и заявок<br> и предоставление заключения <br>по итогам поиска";
$lang['service_page_tarifs_reg-tax'] = "Регистрационные сборы*";
$lang['service_page_tarifs_time'] = "Срок";
$lang['service_page_tarifs_month'] = "месяцев";
$lang['service_page_tarifs_btn'] = " Узнать больше";
$lang['service_page_tarifs_p2_title'] = "Регистрация товарного знака Европейского Союза";
$lang['service_page_tarifs_year'] = "года";
$lang['service_page_tarifs_p3_title'] = "Международная регистрация товарного знака";
$lang['service_page_tarifs_bottom-text'] = "*цена указана для трех классов в классификаторе";
$lang['service_page_faq_title'] = "F.A.Q.";
$lang['service_page_faq_sub_title'] = "Ответы на популярные вопросы";
$lang['service_page_faq_q1_title'] = "ЧТО МНЕ НУЖНО СДЕЛАТЬ, ЧТОБЫ ЗАРЕГИСТРИРОВАТЬ ТОВАРНЫЙ ЗНАК?";
$lang['service_page_q1_text'] = "Вам нужно выбрать, каким будет изображение товарного знака: цветным либо черно-белым, затем определиться с классами товаров/услуг - с этим Вам помогут разобраться наши юристы.";
$lang['service_page_faq_q2_title'] = "КАКИЕ ДОКУМЕНТЫ Я ПОЛУЧУ ПРИ РЕГИСТРАЦИИ?";
$lang['service_page_faq_q2_text'] = "
                            Вам будет выслан электронный сертификат, подтверждающий регистрацию товарного знака.
                        ";
$lang['service_page_faq_q3_title'] = " КАК ПРОИЗВОДИТСЯ ОПЛАТА УСЛУГ?";
$lang['service_page_faq_q3_text'] = "
                            Вы оплачиваете компанию через сайт Legal Space. После окончания работ, мы переводим оплату ассоциированному партнеру Legal Space. Если товарный знак не зарегистрирован (хотя таких случаев еще не было), то мы возвращаем Вам всю сумму полностью.
                        ";
$lang['service_page_faq_q4_title'] = "НА КАКОЙ СРОК ПРЕДОСТАВЛЯЕТСЯ ЗАЩИТА ТОВАРНОГО ЗНАКА?";
$lang['service_page_faq_q4_text'] = "
                            Защита предоставляется сроком на 10 лет и может обновляться бесконечно
                        ";
$lang['main_page_offices_h2'] = "Наши офисы по всему миру";
$lang['main_page_offices_subtitle'] = "Вы можете позвонить в любой из наших офисов в рабочее время и мы <br> проконсультируем вас на русском или английском языке";
$lang['main_page_map_adr1_title'] = "Legalspace — Милан";
$lang['main_page_adr1_adress'] = "Виа Фриули, 51 - 20135";
$lang['main_page_adr1_phone'] = "+39 029-475-96-84";
$lang['main_page_map_adr2_title'] = "Legalspace — Москва";
$lang['main_page_adr2_adress'] = "Пресненский вал, 15";
$lang['main_page_adr2_phone'] = "8 (800) 333-67-36";
$lang['main_page_map_adr3_title'] = "Legalspace — Никоссия";
$lang['main_page_adr3_adress'] = "Псарон, 2408";
$lang['main_page_adr3_phone'] = "+357 230-300-16";
$lang['service_page_anyquestion_title'] = "Остались вопросы?";
$lang['service_page_anyquestion_sub_title'] = "Оставьте свой номер и мы Вам перезвоним";
$lang['service_page_anyquestion_btn'] = "ПЕРЕЗВОНИТЕ МНЕ";
$lang['footer_info'] = " <b>Legal Space</b> — это безопасная и надежная виртуальная площадка,
                                                       <br> которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.";
$lang['footer_copyright'] = "© 2015 Legal Space Ltd., Все права защищены";

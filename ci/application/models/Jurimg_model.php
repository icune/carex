<?php 

 class JurImg_model extends CI_model{
    const FOLDER =  '/img/jur', SIZE_FOLDER = 'size';
    private $full_path = '', $size_path, $color_cache = '', $ru_json = '';
    public function __construct(){
    	parent::__construct();
    	$this->full_path = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . self::FOLDER;
    	$this->color_cache = $this->full_path . '/cc.txt';
        $this->ru_json = $this->full_path . '/ru.json';
    	$this->size_path = $this->full_path . '/' . self::SIZE_FOLDER;
    }
    public function listImages($size){
        $files_raw = scandir($this->full_path);
        $files = array();

        foreach($files_raw as $f)
        	if(preg_match('!\.png$!', $f)){
        		$files []= $this->encode($f);
        	}

        $colors = $this->cacheColors($files);
        $sizes = array();
        foreach($files as $f){
        	$sizes[$f] = $this->encode($this->sizeImage($this->decode($f), $size));
        }
        return array('colors' => $colors, 'sizes' => $sizes);
    }
    private function encode($str){
    	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
    		return mb_convert_encoding($str, "UTF-8", "WINDOWS-1251");
    	else
    		return $str;
    }
    private function decode($str){
    	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
    		return mb_convert_encoding($str, "WINDOWS-1251", "UTF-8");
    	else
    		return $str;
    }
    private function cacheColors($files){
    	if(!file_exists($this->color_cache))
    		file_put_contents($this->color_cache, '{}');
    	$colors = json_decode(file_get_contents($this->color_cache), true);

    	$changes = false;
    	foreach($files as $f)
    		if(!isset($colors[$f])){
                $label = explode('_', $f);
                $label = $label[0];
    			$colors[$f] = array(
                    'color' => $this->meanColor($this->decode($f)),
                    'label' => $label
                );
    			$changes = true;
    		}
    	if($changes)
    		file_put_contents($this->color_cache, json_encode($colors));
    	return $this->colorsLang($colors);
    }
    private function colorsLang($colors){
        $curlang = detect_lang();
        if($curlang != 'russian')
            return $colors;
        $ru = json_decode(file_get_contents($this->ru_json), true);
        foreach($colors as $k=>$v)
            if(isset($v['label']) && isset($ru[$v['label']]))
                $colors[$k]['label'] = $ru[$v['label']];
        return $colors;
    }
    private function sizeImage($fn, $size){
    	if(!is_dir($this->size_path))
    		mkdir($this->size_path);
    	// $fn_non_cyr = preg_replace('![^a-zA-Z.]!', '', $fn);
    	$fn_non_cyr = $fn;
    	$fn_size = $this->size_path . '/' . $size . '+++' . $fn_non_cyr;
    	$fn_size_front = self::SIZE_FOLDER . '/' . $size . '+++' . $fn_non_cyr;
    	$fn = $this->full_path . '/' . $fn;
    	if(file_exists($fn_size))
    		return $fn_size_front;

    	$image = imagecreatefrompng($fn);
		$width = imagesx($image);
		$height = imagesy($image);
		$new_img = imagecreatetruecolor($size, $size);
		imagecopyresampled($new_img, $image, 0, 0, 0, 0, $size, $size, $width, $height);
        // imagefilter($new_img, IMG_FILTER_GRAYSCALE);
		imagepng($new_img, $fn_size);
		return $fn_size_front;
    }
    private function meanColor($f){
    	$fn = $this->full_path . "/$f";
    	$image = imagecreatefrompng($fn);
		$width = imagesx($image);
		$height = imagesy($image);
		$pixel = imagecreatetruecolor(1, 1);
		imagecopyresampled($pixel, $image, 0, 0, 0, 0, 1, 1, $width, $height);
		$rgb = imagecolorat($pixel, 0, 0);
		$color = imagecolorsforindex($pixel, $rgb);
		return 'rgb('.$color['red'].', '.$color['green'].', '.$color['blue'].')';
    }
 }
<!DOCTYPE html>
<html data-wf-site="560d391d8d41e6275f317166" data-wf-page="560d391d8d41e6275f317165">
<head>
  <meta charset="utf-8">
  <title>LegalSpace</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/webflow_files/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="/webflow_files/css/webflow.css">
  <link rel="stylesheet" type="text/css" href="/webflow_files/css/ls-lp-main-adofdsfsdf.webflow.css">
  <script type="text/javascript" src="/webflow_files/js/modernizr.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="/webflow_files/images/favicon.ico">
  <link rel="apple-touch-icon" href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png">
  <style type="text/css">
    @media screen and (min-width: 1200px) {
     .w-container {
     max-width: 1170px;
     }
     }
  </style>
  <?php $this->view('analytics'); ?>
</head>
<body class="body">
<?php $this->load->language('header'); ?>
<?php $_ = $this->lang->language; ?>
<?//$this->view('header'); ?>
  <div class="w-nav global-header" data-collapse="medium" data-animation="default" data-duration="400" data-contain="1" data-doc-height="1" data-no-scroll="1">
    <div class="w-container header-container">
      <a class="w-nav-brand header-logo" href="/"><img width="210" src="/webflow_files/images/ls-logo.png">
      </a>
      <nav class="w-nav-menu w-hidden-medium w-hidden-small w-hidden-tiny header-leftarea" role="navigation"><a class="w-nav-link header-link <?php if(Info::$script == 'how_works') echo 'active'; ?>" href="/how"><?php echo $_['how_works']; ?></a><a class="w-nav-link header-link" href="/login"><?if($curlang == 'english') echo 'Login'; else echo 'Войти';?></a>
      </nav>
      <nav class="w-nav-menu header-rightarea" role="navigation"><a class="w-nav-link w-hidden-main header-link" href="#">Как это работает</a><a class="w-nav-link w-hidden-main header-link" href="#"><?if($curlang == 'english') echo 'Login'; else echo 'Войти';?></a><a class="w-nav-link header-link phone" href="tel:+7 (800) 333-67-36">8 (800) 333-67-36</a><a class="w-nav-link w-hidden-main header-link" href="">Запросить юриста</a>
        <div class="w-dropdown lang-block" data-delay="0">
          <div class="w-dropdown-toggle header-lang-toggle">
            <div class="header-lang-text"><?if($curlang == 'russian') echo 'Русский';else echo 'English';?></div>
            <div class="w-icon-dropdown-toggle header-lang-arrow"></div>
          </div>
          <nav class="w-dropdown-list header-lang-list">
			<a class="w-dropdown-link header-lang-link" href="/lang/en/">English</a>
			<a class="w-dropdown-link header-lang-link" href="/lang/ru/">Русский</a>
          </nav>
        </div><a class="w-hidden-medium w-hidden-small w-hidden-tiny w-button header-lawyerbtn" href="/?fs=1"><?php echo $_['req_law']; ?></a>
      </nav>
      <div class="w-nav-button header-hamarea">
        <div class="w-icon-nav-menu header-hamicon"></div>
      </div>
    </div>
  </div>
  <div class="w-section partners-logos-section">
    <div class="w-container">
      <h1 class="partners-h1">Партнёры Legal Space</h1>
      <div class="w-row partners-logos-row">
        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-6 partners-logos-column">
          <a class="w-inline-block partners-row-logo" href="#partner-skolkovo"></a>
          <a class="w-inline-block partners-row-logo meetspeakers" href="#partner-meetspeakers"></a>
        </div>
        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-6 partners-logos-column">
          <a class="w-inline-block partners-row-logo meetingpoint" href="#partner-meetingpoint"></a>
        </div>
        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-6 partners-logos-column">
          <a class="w-inline-block partners-row-logo leanstartup" href="#partner-leanstartup"></a>
        </div>
        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-6 partners-logos-column">
          <a class="w-inline-block partners-row-logo meetpartners" href="#partner-meetpartners"></a>
        </div>
      </div>
    </div>
  </div>
  <div class="w-section partners-partner skolkovo" id="partner-skolkovo">
    <div class="w-container partners-partner-container"><img class="partners-logo" src="/webflow_files/images/pr-skolkovo.png">
      <h1 class="mp-h4">Выбор в пользу бизнес-школы СКОЛКОВО</h1>
      <p class="partners-p1">Московская школа управления СКОЛКОВО – одна из ведущих частных бизнес-школ России и СНГ, основанная в 2006 году по инициативе делового сообщества.</p><a class="partners-link" href="http://school.skolkovo.ru" target="_blank">http://school.skolkovo.ru</a>
    </div>
  </div>
  <div class="w-section partners-partner" id="partner-meetingpoint">
    <div class="w-container partners-partner-container"><img class="partners-logo" src="/webflow_files/images/pr-meetingpoint.png">
      <h1 class="mp-h4">Место для работы,&nbsp;деловых встреч и переговоров в шаге от Кремля</h1>
      <p class="partners-p1">Кто проводит встречи и переговоры на высоком деловом уровне. Meeting Point идеально подходит как нейтральная статусная площадка для встреч с партнерами и произведет на них самое благоприятное впечатление.</p><a class="partners-link" href="http://www.point2meet.ru" target="_blank">http://www.point2meet.ru</a>
    </div>
  </div>
  <div class="w-section partners-partner leanstartup" id="partner-leanstartup">
    <div class="w-container partners-partner-container"><img class="partners-logo" src="/webflow_files/images/pr-leanstartup.png">
      <h1 class="mp-h4">Как избежать ошибок<br>и создать бизнес с нуля</h1>
      <p class="partners-p1">Lean Startup — революционная методология запуска и развития новых проектов в условиях неопределенности, родом из Кремниевой Долины.
      </p><a class="partners-link" href="http://leanstartuprussia.ru" target="_blank">http://leanstartuprussia.ru</a>
    </div>
  </div>
  <div class="w-section partners-partner meetpartners" id="partner-meetpartners">
    <div class="w-container partners-partner-container"><img class="partners-logo" src="/webflow_files/images/pr-meetpartners.png">
      <h1 class="mp-h4">Компания, специализирующаяся на нетворкинг мероприятиях</h1>
      <p class="partners-p1">Международная компания, которая фокусируется на edutainmen и нетворкинг мероприятиях, а также представляет лидирующих мировых спикеров в России - Тони Роббинс, Боб Дорф, Роберт Кийосаки, Харв Екер, Йохан Эрнст Нильсон и другие</p><a class="partners-link" href="http://meetpartners.ru" target="_blank">http://meetpartners.ru</a>
    </div>
  </div>
  <div class="w-section partners-partner meetspeakers" id="partner-meetspeakers">
    <div class="w-container partners-partner-container"><img class="partners-logo" src="/webflow_files/images/pr-meetspeakers.png">
      <h1 class="mp-h4">Ведущее бюро спикеров в России и СНГ</h1>
      <p class="partners-p1">Компания-лидер в международных edutainment &amp; нетворкинг мероприятиях и официальный представитель всемирно известных спикеров в России и СНГ.</p><a class="partners-link" href="http://meetspeakers.com" target="_blank">http://meetspeakers.com</a>
    </div>
  </div>
  <div class="w-section mainfooter-section">
    <div class="w-container mainfooter-1lane">
      <div class="w-row">
        <div class="w-col w-col-5"><img class="mainfooter-paymenticons" width="83" src="/webflow_files/images/footer-visa.png"><img class="mainfooter-paymenticons" width="59" src="/webflow_files/images/footer-mc.png"><img class="mainfooter-paymenticons" width="119" src="/webflow_files/images/footer-pp.png">
        </div>
        <div class="w-col w-col-2 mainfooter-si-col">
          <a class="w-inline-block mainfooter-si" href="https://www.facebook.com/Legal-Space-468707986637000" target="_blank"><img width="12" src="/webflow_files/images/footer-fb.png">
          </a>
          <a class="w-inline-block mainfooter-si" href="https://www.linkedin.com/company/9340358?trk=tyah&amp;trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9340358%2Cidx%3A1-1-1%2CtarId%3A1441991163447%2Ctas%3Alegal%20space" target="_blank"><img width="24" src="/webflow_files/images/footer-in.png">
          </a>
        </div>
        <div class="w-col w-col-5 mainfooter-lawyer-col"><a class="w-button mainfooter-btn-lawyer" href="/lawyer"><?php echo $_['are_you']; ?></a>
          <div class="mainfooter-lawyer-text"><?php echo $_['we_invite']; ?></div>
        </div>
      </div>
    </div>
    <div class="w-container mainfooter-2lane">
      <div class="mainfooter-2lane-text">Партнеры</div>
      <a class="w-inline-block mainfooter-2lane-logo" href="/partners"><img height="80" src="/webflow_files/images/pr-meetingpoint.png">
      </a>
      <a class="w-inline-block mainfooter-2lane-logo" href="/partners"><img height="80" src="/webflow_files/images/pr-skolkovo.png">
      </a>
      <a class="w-inline-block mainfooter-2lane-logo" href="/partners"><img height="80" src="/webflow_files/images/pr-leanstartup.png">
      </a>
      <a class="w-inline-block mainfooter-2lane-logo" href="/partners"><img height="80" src="/webflow_files/images/pr-meetspeakers.png">
      </a>
      <a class="w-inline-block mainfooter-2lane-logo" href="/partners"><img height="80" src="/webflow_files/images/pr-meetpartners.png">
      </a>
    </div>
    <div class="w-container mainfooter-3lane">
      <div class="w-row">
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p1"><?php echo $_['footer_text']; ?></p>
          <p class="mainfooter-3lane-p1"><?php echo $_['cpr']; ?></p>
        </div>
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p2"><?php echo $_['foo_1']; ?></p>
        </div>
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p2"><?php echo $_['foo_2']; ?></p>
        </div>
        <div class="w-col w-col-3">
          <p class="mainfooter-3lane-p2"><?php echo $_['foo_3']; ?></p>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/webflow.js"></script>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
  
  
  
  <script type="text/javascript" src="plug/cs/js/countrySelect.js"></script>
    <script src="/js/navigation.js"></script>
    <script src="/js/common.js"></script>
    <script src="/js/wiggy.js"></script>
    <script src="/js/net.js"></script>
    <script src="/js/jquery.vide.js"></script>
    <script src="/js/video.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="/fcab/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var images = <?php echo json_encode($files, true); ?>
    </script>
    <div class="popup" style="display:none">
        <input type="text" class="cselect-internal" val="<?php echo $_['where_need']; ?>" name="country" />
        <input type="text" name="name" class="wiggy white" placeholder="<?php echo $_['name']; ?>" />
        <input type="text" name="email" class="wiggy white" placeholder="Email" />
        <input type="text" name="phone" class="wiggy white" placeholder="<?php echo $_['phone']; ?>" value="" />
        <textarea class="wiggy white" name="message" placeholder="<?php echo $_["opishite_vashu_zadaczu"]; ?>"></textarea>
        <div class="btn" style="width:100%">
            <?php echo $_['send']; ?> </div>
    </div>




	<div class="modal fade message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="cross"></span>
					</button>
					<br />
				</div>
				<div class="modal-body">

						
						<div class="row">
							
										   
						</div>
						

					</div>
				
			</div>
		</div>
	</div>



	<?php $this->view('jivosite'); ?>
	<?php $this->view('modal'); ?>
  
</body>
</html>
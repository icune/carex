create table user(
  id int not null primary key auto_increment,
  name char(255) not null,
  surname  char(255) not null,
  patronym  char(255) not null,
  email  char(255) not null,
  create_ts timestamp default current_timestamp
 );
 create table dir(
   id int not null primary key auto_increment,
   name char(255) not null
);
create table user_dir(
  user_id int not null,
  dir_id int not null
);


insert into dir(name) values('жилищное право'),
('семейное право'),
('банковское право'),
('транспортное право'),
('нотариат')
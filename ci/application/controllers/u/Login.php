<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index(){
		$this->load->database();
		if(isset($_GET['magic'])){
			$this->by_magic();
			$redirect = '/in/tasks/';
		}else{
			$this->by_email();
			$redirect = '/in/tasks/';
		}
		
		if(isset($_GET['redirect']))
			$redirect = $_GET['redirect'];
		if(isset($_GET['confirm']))
			$redirect = '/in/tasks/';	
		if(isset($_GET['restore']))
			$redirect = '/in/profile?restore';	
		header('Location: ' . $redirect);
	}
	private function by_magic(){
		if(!trim($_GET['magic']))
			return false;
		$r = $this->db->query('SELECT * FROM user WHERE magic=' . $this->db->escape($_GET['magic']))->result_array();
		$cookie = null;
		if(count($r)){
			$cookie = $r[0]['cookie'];
			if(!strlen(trim($cookie)))
				$cookie = User::updateCookie($r[0]['id']);
			setcookie(User::$cookie, $cookie, time()+60*60*24*365, '/');
			User::updateMagic($r[0]['id'], '');
			if(isset($_GET['confirm']))
				User::confirm($r[0]['id']);
			return true;
		}else
			return false;
	}
	private function by_email(){
		$email = $_GET['email'];
		if(!isset($_GET['password']))
			return false;
		$password = $_GET['password'];
		$r = User::get_by_ep($email, $password);
		if(count($r)){
			$cookie = User::updateCookie($r[0]['id']);
			setcookie(User::$cookie, $cookie, time()+60*60*24*365, '/');
			return true;
		}else
			return false;
			
	}
	public function check(){
		$email = $this->postAbsent('email');
		$password = $this->postAbsent('password');

		$r = User::get_by_ep($email);
		if(!count($r))
			$this->err('abs-email');
		$r = User::get_by_ep($email, $password);
		if(!count($r))
			$this->err('wrong-password');
		$this->ok();
	}
	public function restore(){
		if(!isset($_POST['email']))
			$this->err('abs-email');
		$email = $_POST['email'];
		$user = User::get_by_ep($email);
		if(!count($user)) 
			$this->err('no-user');
		$magic = User::updateMagic($user[0]['id']);
		$this->load->helper('mail');
		$host = Info::$host;
		$link = "<a href='http://$host/u/login?restore&magic=$magic'>Мнгновенный вход в личный кабинет</a>";
		Mail::send($email, 'restore', array(
			'lk_link' => $link
		));
		$this->ok();
	}

	
}


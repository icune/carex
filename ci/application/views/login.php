<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Вход</title>
	<script src="/js/jquery-2.1.4.js"></script>
	<script src="/js/net.js"></script>
	<script src="/js/login.js"></script>
	<script src="/js/wiggy.js"></script>
	<link rel="stylesheet" href="/css/wiggy.css">
	<style type="text/css">
		.box{
			width:300px;
			border:1px solid #aaaaaa;
			margin: 0 auto;
		}
		.box .hat{
			padding:20px;
			border-bottom:1px solid #aaaaaa;
		}
		.box .body{
			padding:20px;
		}
		.box .body input{
			padding:5px;
			width: 100%;
			display: block;
			box-sizing:border-box;
		}
		.box .body .btn{
			padding:5px;
			background-color: green;
			color:white;
			cursor: pointer;
		}
		.box .body .btn:hover{
			background-color: red;
		}
	</style>
</head>
<body>
	<div class="box">
		<div class="hat"> Вход </div>
		<div class="body">
			<input type="text" class="email" name="email" />
			<input type="password" class="password" name="password" />
			<div class="btn enter">Войти</div>
		</div>
	</div>
</body>
</html>
$(function(){
	$('#add-specialization').click(function(){
		var name = $('#new-specialization').val();
		var inp = $('<input type="checkbox" checked=checked/>');
		var span = $('<span class="name"></span>').text(name);
		var spanDel = $('<span></span>').addClass('del').text(' Удалить');
		var sp = $('<div>').addClass('spec new').append(inp).append(span)
		.append(spanDel);
		$('#specializations').append(sp);
		spanDel.click(function(){
			sp.remove();
			fillSpec();
		})
		
		inp.change(function(){
			fillSpec();
		})
		$('#specializations').append(
			$('<input type="hidden"  class="new_specializations" name="new_specializations[]">')
			.val(name+'[on]')
		);
		function fillSpec(){
			$('.new_specializations').remove();
			
			$('#specializations .spec.new').each(function(){
				var spp = $('<input type="hidden" class="new_specializations" name="new_specializations[]">')
					.val($(this).find('.name').text() + ($(this).find('[type=checkbox]').is(':checked') ? '[on]' : '') )
				$('#specializations').append(spp);
			});
			
		}
		$('#new-specialization').val('')
	})
})

$(function(){
	$('#add-direction').click(function(){
		var name = $('#new-direction').val();
		var inp = $('<input type="checkbox"  checked=checked/>');
		var span = $('<span class="name"></span>').text(name);
		var spanDel = $('<span></span>').addClass('del').text(' Удалить');
		var sp = $('<div>').addClass('spec new').append(inp).append(span)
		.append(spanDel);
		$('#directions').append(sp);
		spanDel.click(function(){
			sp.remove();
			fillSpec();
		})
		
		inp.change(function(){
			fillSpec();
		})
		$('#directions').append(
			$('<input type="hidden"  class="new_directions" name="new_directions[]">')
			.val(name + '[on]')
		);
		function fillSpec(){
			$('.new_directions').remove();
			
			$('#directions .spec.new').each(function(){
				var spp = $('<input type="hidden" class="new_directions" name="new_directions[]">')
					.val($(this).find('.name').text() + ($(this).find('[type=checkbox]').is(':checked') ? '[on]' : '') );				
				$('#directions').append(spp);
			});
			
		}
		$('#new-direction').val('')
	})
})

$(function(){
	var new_inx = 'new1';
	$('#add-service').click(function(){
		var el = $('<div class="serv">\
					<input type="text" name="service['+new_inx+'][name]" value="">\
					<input type="text" name="service['+new_inx+'][price]" value="">\
					<div class="btn del">Удалить</div>\
				</div>');
		new_inx = new_inx + '1';
		$('#services').append(el);
		bind();
	})
	function bind(){
		$('.serv .del').off('click');
		$('.serv .del').click(function(){
			$(this).closest('.serv').remove();
		})
	}
	bind();
})

$(function(){
	var userSrc = null;
	Chunky({
		fileElem:$('#upl'),
		specialDir:'ava-expert/',
		callbacks:{
			done:function(fn){	
				$('#image').attr('src', fn);

			    var __img = $("<img/>")
			        .attr("src", fn)
			        .load(function() {
			            var real_width = this.width;
			            var real_height = this.height;
						$('#img').load(function(){
							userSrc = fn;
							$('#img').guillotine({
				            	width:150,
				            	height:150
	    					});
	    					for(var i = 0; i < 20; i++)
            					$('#img').guillotine('zoomOut');
						}).attr('src', fn);        

			    		$('.fluid').show();
						__img.remove();
			        });

				
				// Net.post('/u/edit/set', {
				// 	user:{image:fn}
				// });
			}
		}
	})

	$('#plus').click(function(){
        for(var i = 0; i < 2; i++)
            $('#img').guillotine('zoomIn');
    });
    $('#minus').click(function(){
        for(var i = 0; i < 2; i++)
            $('#img').guillotine('zoomOut');
    });

    $('#ok').click(function(){
        var data = $('#img').guillotine('getData');
        Net.post('/u/edit/rescale', {userSrc:userSrc, p:data}, {
        	success:function(){
        		var cFn = userSrc.split('.');
        		cFn[cFn.length-2] = cFn[cFn.length-2] + '-c';
        		cFn = cFn.join('.');
        		$('#image').attr('src', cFn);
        		$('#image-input').val(cFn);
        		$('.fluid').hide();
        		
        	},
        	error:function(){
        		alert('error');
        		$('.fluid').hide();
        	}
        })
    })
})


$(function(){
	window.email_busy = true;
	$('[name="user[email]"]').keyup(busy);
	function busy(done){
		if(user_id && user_email.trim() == $('[name="user[email]"]').val().trim()){
			$('#busy').addClass('free').text('E-mail свободен');
					email_busy = false;
			if(done instanceof Function) done();
			return;
		}
		Net.post('/u/register/check', {email : $('[name="user[email]"]').val(), password : '123'}, {
			success : function(s){
				$('#busy').addClass('free').text('E-mail свободен');
					email_busy = false;
				if(done instanceof Function) done();
			},error : function(s){

				if(s == 'email-busy'){
					$('#busy').removeClass('free').text('E-mail занят');
					email_busy = true;
				}else
					alert('Неизвестная ошибка');

				if(done instanceof Function) done();
			}
		});
	}
	busy();

	$('#main-form').submit(function(){
		if(email_busy){
			setTimeout(function(){
				alert('E-mail занят!');
			}, 0);
			return false;
		}
		return true;
	})
	var del_confirmed = false;
	$('#del-form').submit(function(){
		if(!del_confirmed){
			setTimeout(function(){
				if(confirm('Точно удалить?')){
					del_confirmed = true;
					$('#del-form').submit();
				}
			}, 0);
			return false;
		}
		return true;
	})
})
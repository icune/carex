<?php
final class Info{
	public static $sid = null, $rootDir = null, $printNumber = null;
	public static $uri = null, $query = null;
	public static $isWin = false, $isNix = false;
	public static $script = '';
	public static $host = '';
	public static function init(){

		self::$rootDir = rtrim($_SERVER['DOCUMENT_ROOT'], '/');
		self::$uri = $_SERVER['REQUEST_URI'];
		self::$query = $_SERVER['QUERY_STRING'];
		self::$host = $_SERVER['HTTP_HOST'];
		$parts = explode('/', self::$uri);
		if($parts[count($parts)-1] == self::$host)
			$script = '';
		else{
			$qst = explode('?', $parts[count($parts)-1]);
			
			self::$script = $qst[0];
		}
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		    self::$isWin = true;
		} else {
		    self::$isNix = true;
		}
	}
}

Info::init();
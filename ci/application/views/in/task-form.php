<?php $this->view('in/header'); ?>

<div class="col-sm-9  col-md-10  main">
    <div class="row">
        <p class="pull-left btn-left-sidebar visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
        </p>
    </div>
   
           
                      <div class="row order-edit">
                <?php if(isset($task_id)): ?>
               		<h2>Редактирование Заявки № <?php echo $task_id; ?> </h2>
               	<?php else: ?>
               		<h2>Создание новой заявки</h2>
               	<?php endif; ?>
               	<form method="POST" action="/in/tasks/<?php if(isset($task_id)) echo 'save'; else echo 'insert';?>" >
               <table class="leads-table table table-striped">
                   <tr>
                       <th>Авто</th>
                       <th>От</th>
                       <th>До</th>
                       <th>Статус</th>
                       <th>Действия</th>
                   </tr>
                   <tr>
                       <td>
                       	<?php if(isset($task_id)): ?>
                       		<input type="hidden" name="task_id" value="<?php echo $task_id; ?>" />
               				
               			<?php endif; ?>
                       	<textarea name="task[desc]"><?php echo $task['desc']; ?></textarea>
                       </td>
                       <td class="order-cost">
                       <input type="text" name="task[from]" value="<?php echo $task['from'] ?>" />
                       </td>
                       <td class="order-cost">
                       <input type="text" name="task[to]" value="<?php echo $task['to'] ?>" />
                       </td>
                       <td>
                       	<?php 
	                       	$st = array(
		                   		'new' => 'Новая',
		                   		'go' => 'В процессе...',
		                   		'done' => 'Готово',

		                   	); 
	                       	echo $st[$task['status']];
	                   	?>

                       </td>
                       <td >
                           <button class="lead-canceled"><i class="fa fa-save"></i> Сохранить</button>
                       </td>
                   </tr>
                   
               </table>
               </form>
               
           </div>
           
</div>

<?php $this->view('in/footer'); ?>
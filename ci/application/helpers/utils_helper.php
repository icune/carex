<?php 

final class Utils{
	public static function rStr($cnt, $set = null){
		if(!$set)
			$set = array(array('a', 'z'), array('A', 'Z'), array('0', '9'));
		$chMap = array();
		foreach($set as $s)
			for($code = ord($s[0]); $code <= ord($s[1]); $code++)
				$chMap []= chr($code);
		$r = '';
		for($i = 0; $i < $cnt; $i++)
			$r .= $chMap[rand()%count($chMap)];
		return $r;
	}
	public static function postTo($domain, $post, $add_header = array()){
	//Проверка на правильность URL 
		if(!filter_var($domain, FILTER_VALIDATE_URL)){
			throw new Exception("CURL: Bad $domain domain!");
			return false;
		}

		//Инициализация curl
		$curlInit = curl_init($domain);
		curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
		curl_setopt($curlInit,CURLOPT_HEADER,false);
		curl_setopt($curlInit,CURLOPT_NOBODY,false);
		curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curlInit, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curlInit, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlInit, CURLOPT_URL, $domain);
		
		curl_setopt($curlInit, CURLOPT_POST, true);
		curl_setopt($curlInit, CURLOPT_POSTFIELDS, http_build_query($post));

		//curl_setopt($curlInit, CURLOPT_VERBOSE, true);
		$header = array('Content-Type: application/x-www-form-urlencoded');
		$header = array_merge($header, $add_header);
		curl_setopt($curlInit, CURLOPT_HTTPHEADER, $header);

		//Получаем ответ
		$response = curl_exec($curlInit);

		$errno = curl_errno($curlInit);
		
		curl_close($curlInit);

		if($errno)
			throw new Exception(curl_strerror($errno));

		return $response;
	}
	public static function getTo($domain, $get, $header = array()){
	//Проверка на правильность URL 
		if(!filter_var($domain, FILTER_VALIDATE_URL)){
			throw new Exception("CURL: Bad $domain domain!");
			return false;
		}

		//Инициализация curl
		$curlInit = curl_init($domain . '?' . http_build_query($get));
		curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
		curl_setopt($curlInit,CURLOPT_HEADER,false);
		curl_setopt($curlInit,CURLOPT_NOBODY,false);
		curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curlInit, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curlInit, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlInit, CURLOPT_URL, $domain . '?' . http_build_query($get));

		//curl_setopt($curlInit, CURLOPT_VERBOSE, true);
		if(count($header))
			curl_setopt($curlInit, CURLOPT_HTTPHEADER, $header);

		//Получаем ответ
		$response = curl_exec($curlInit);

		$errno = curl_errno($curlInit);
		
		curl_close($curlInit);

		if($errno)
			throw new Exception(curl_strerror($errno));

		return $response;
	}
	public static function log($str){
		file_put_contents(Info::$rootDir . '/' . 'attractive-log.txt', "[" . date('d.m.Y H:i:s') . "]  " .$str."\n\n", FILE_APPEND);
	}
	public static function assocKey($arr, $key = 'id'){
		$r = array();
		foreach($arr as $a)
			$r[$a[$key]] = $a;
		return $r;
	}
	public static function concar($arr, $fields, $delim = ' '){
		$r = array();
		foreach($arr as $k => $v){
			$s_p = array();
			foreach($fields as $f)
				$s_p []= $v[$f];
			$r[$k] = implode($delim, $s_p);
		}
		return $r;
	}
}
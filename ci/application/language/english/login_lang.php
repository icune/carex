<?php 
$lang['we_invite'] = '';
$lang['are_you'] = 'For lawyers';
$lang['title'] = 'Legal space';
$lang['best_juri_online'] = 'Best lawyers worldwide. Online';
$lang['bla_bla_before_form'] = "Fill in the details and find your lawyer";
$lang['get_free_consult'] = 'Get free advice';
$lang['where_you'] = 'Choose a country';
$lang['desc_problem'] = 'Describe your legal matter';
$lang['send'] = 'Send';
$lang['footer_blabla'] = '© Copyright 2015. Legal Space';
$lang['rights'] = 'All rights reserved';
$lang['moscow'] = 'Moscow';
$lang['moscow_addr'] = '15 Presnensky val';
$lang['milan'] = 'Milan';
$lang['milan_addr'] = 'Via Friuli, 51 - 20135';
$lang['nikosia'] = 'Nikosia';
$lang['nikosia_addr'] = 'Psaron , 2408 ';
$lang['from_where_you'] = 'Where are you from?';
$lang['name'] = 'Your name';
$lang['phone'] = 'Contact phone number';
$lang['success'] = 'Thank you for your request! We’re on your case.';
$lang['error'] = 'Something went wrong. Please contact us and we will help: info@legalspace.world';
$lang['services'] = array(
	'Trademarks and intellectual property',
	'International tax and corporate structuring',
	'Corporate M&A',
	'Private equity',
	'Real estate',
	'Tax',
	'Investments',
	'Company formation ',
	'Investment funds',
	'Restructuring and insolvency',
	'Labour law',
	'Competition and antitrust',
	'Banking and finance',
	'Litigation & dispute resolution',
	'Private wealth and succession planning',
);
$lang['bla_1'] = '<p>LEGAL SPACE is a safe and reliable virtual resource to help you resolve your legal issues online. Whether you are setting up a company in Cyprus, registering a trademark, or applying for residential rights abroad - whatever legal service you need, in whichever part of the world, you can find it right now on our website. </p><p>Every LEGAL SPACE lawyer passes through a highly competitive selection process, which is why we are sure of his or her qualifications and professionalism and can guarantee the quality of his or her services.</p>';
$lang['bla_2'] = '<p>All of our lawyers have a minimum of five years professional experience. Every candidate undergoes a personal interview before they start work, while lawyers may only join the LEGAL SPACE professional pool through recommendation.</p><p> Our partners are outstanding representatives of the legal profession; they are progressive and always abreast of the latest developments in the industry. Together we are changing the world of legal services for the better. </p>';
$lang['we_connect'] = 'We connect you with the best lawyers DIRECTLY';
$lang['spoiled_phone'] = 'Our comprehensive geographical database allows us to solve your issue in any country.';
$lang['with_support'] = 'With support';


$lang['globe_under'] = 'countries';
$lang['hat_under'] = 'lawyers';
$lang['case_under'] = 'clients';

$lang['office_title'] = 'Our offices:';
$lang['office_desc'] = 'Call us and we will help';

$lang['moscow_desc'] = 'Moscow office is located in the very centre of Moscow, 5 minutes walk from the metro Belorusskaya. There is the parking for the clients.';
$lang['milan_desc'] = 'Our office in Milan is located in the very centre of Milan, 10 minutes walk from the metro Lodi.
';
$lang['nikosia_desc'] = 'Our office in Cyprus is conveniently located near the airport.
';

$lang['find_descision'] = 'Find legal solutions right now';
$lang['find_descision_after'] = 'Need help? We will call you very soon';

$lang['bot_button'] = 'Call me';

$lang['enter_lk'] = 'Enter your account';
$lang['password'] = 'Password';




$lang['_main_page_h1_title'] = 'Best lawyers worldwide. Online';
$lang['_main_page_h1_subtitle'] = 'Fill in the details and find your lawyer';
$lang['_main_page_btn_consult'] = 'Get free advice';
$lang['_main_page_trigers_title_1'] = 'FIND LAWYERS EASILY';
$lang['_main_page_trigers_text_1'] = 'More than 2 000 lawyers from more than 150 countries';
$lang['_main_page_trigers_title_2'] = 'BEST LAWYERS';
$lang['_main_page_trigers_text_2'] = '6 years experience, referrences and passed personal interview';
$lang['_main_page_trigers_title_3'] = 'SECURED TRANSACTION';
$lang['_main_page_trigers_text_3'] = 'The lawyer receives payment only after the work is done.';
$lang['_main_page_best_lawyers_h2'] = 'We connect you with the best lawyers directly';
$lang['_main_page_best_lawyers_sub-title'] = '';
$lang['_main_page_best_lawyers_info_text'] = '
<p>LEGAL SPACE is a safe and reliable virtual resource to help you resolve your legal issues online. Whatever legal service you need, in whichever part of the world, you can find it right now on our website. </p>
<p>After you submit a requst you will be redirected to a chat where you can discuss any matter with an expert, as well as check the qualifications of the expert you are talking with. </p>
<p>Every LEGAL SPACE lawyer passes through a highly competitive selection process, which is why we are sure of his or her qualifications and professionalism and can guarantee the quality of his or her services. </p>
                                ';
$lang['_main_page_brands_support'] = 'При поддержке';
$lang['_main_page_pop_service_h2'] = 'Популярные услуги';
$lang['_main_page_pop_service_subtitle'] = 'Вы можете позвонить в любой из наших офисов в рабочее время и мы
                                        <br> проконсультируем вас на русском или английском языке';
$lang['_main_page_pop_service_title_1'] = 'Создать
                                                                <br> компанию
                                                                <br> в России';
$lang['_main_page_pop_service_text_1'] = 'Открыть свою компанию проще, чем вам кажется. Мы поможем сориентироваться в процессе, подготовить документы и начать бизнес';
$lang['_main_page_pop_service_btn'] = 'Бесплатная консультация';
$lang['_main_page_pop_service_btn_more'] = '';
$lang['_main_page_pop_service_title_2'] = 'Создать
                                                                <br> компанию
                                                                <br> на Кипре';
$lang['_main_page_pop_service_text_2'] = 'Компания на Кипре — одно из лучших решений при необходимости выхода на международный рынок';
$lang['_main_page_pop_service_btn'] = 'Бесплатная консультация';
$lang['_main_page_pop_service_btn_more'] = '';
$lang['_main_page_pop_service_title_3'] = 'Зарегистрировать
                                                                <br> товарный
                                                                <br> знак';
$lang['_main_page_pop_service_text_3'] = 'На международном рынке необходимо защищать свой бренд. Мы поможем вам сделать это без лишней бюрократии';
$lang['_main_page_pop_service_btn'] = 'Бесплатная консультация';
$lang['_main_page_pop_service_btn_more'] = 'Узнать подробнее';
$lang['_main_page_solution_h2'] = 'мы Поможем вам <br> решить любые задачи';
$lang['_main_page_solution_subtitle'] = '';
$lang['_main_page_solution_title_startup'] = 'Стартапам';
$lang['_main_page_solution_title_lawyer'] = 'Юристам';
$lang['_main_page_solution_title_business'] = 'Бизнесу';
$lang['_main_page_solution_title_business'] = 'Бизнесу';
$lang['_main_page_solution_text_business'] = '
                            
                                Я понимал, что мне нужен юрист для<br> сопровождения инвестиционного раунда.<br> Legal Space помог мне <br>найти одного из лучших инвестиционных юристов, <br>который решил мою задачу быстро<br> и отстоял важнейшие для меня условия

                        ';
$lang['_main_page_solution_btn_business'] = 'Сократить издержки бизнеса';
$lang['_main_page_solution_business_clinet'] = '<b>Дмитрий Зарюта</b>
                                                         CEO EASY TEN';
$lang['_main_page_solution_title_lawyer'] = 'Юристам';
$lang['_main_page_solution_text_lawyer'] = '
                            С Legal Space мы можем за счет внешних юристов в любой момент <br> масштабировать свой юридический <br>департамент до размера, <br>который нужен для сопровождения <br>крупной сделки или судебного<br> разбирательства
                        ';
$lang['_main_page_solution_btn_lawyer'] = 'Найти юриста';
$lang['_main_page_solution_title_startup'] = 'Стартапам';
$lang['_main_page_solution_text_startup'] = '
                            У нас растущий бизнес и нет <br>времени на поиск высококлассного юриста<br> в рамках имеющегося бюджета. Нам нужен <br>юрист, который хорошо понимает <br>наш бизнес и финансовые возможности. <br>Legal Space помогли нам создать структуру<br> нашего бизнеса, без которой мы бы <br>просто не смогли существовать. <br>Спасибо большое.
                        ';
$lang['_main_page_solution_btn_startup'] = 'Начать свое дело';
$lang['_main_page_solution_startup_clinet'] = 'Команда
                                                        <b>Bash.today</b>';
$lang['_main_page_activity_h2'] = 'Направления деятельности';
$lang['_main_page_activity_list_1'] = 'Товарные знаки и интеллектуальная собственность';
$lang['_main_page_activity_list_2'] = 'Недвижимость';
$lang['_main_page_activity_list_3'] = 'Корпоративное право и M&amp;A';
$lang['_main_page_activity_list_4'] = 'Частный капитал';
$lang['_main_page_activity_list_5'] = 'Международное налоговое и корпоративное структурирование';
$lang['_main_page_activity_list_6'] = 'Налоги';
$lang['_main_page_activity_list_7'] = 'Инвестиции';
$lang['_main_page_activity_list_8'] = 'Учреждение компаний';
$lang['_main_page_activity_list_9'] = 'Инвестиционные фонды';
$lang['_main_page_activity_list_10'] = 'Реструктуризации и банкротство';
$lang['_main_page_activity_list_11'] = 'Трудовое право';
$lang['_main_page_activity_list_12'] = 'Антимонопольное законодательство';
$lang['_main_page_offices_h2'] = 'Наши офисы';
$lang['_main_page_offices_subtitle'] = 'Позвоните нам и мы поможем найти решение, подходящее именно Вам.';
$lang['_main_page_map_adr1_title'] = 'Legalspace — Милан';
$lang['_main_page_adr1_adress'] = 'Виа Фриули, 51 - 20135';
$lang['_main_page_adr1_phone'] = '+39 029-475-96-84';
$lang['_main_page_map_adr2_title'] = 'Legalspace — Москва';
$lang['_main_page_adr2_adress'] = 'Пресненский вал, 15';
$lang['_main_page_adr2_phone'] = '8 (800) 333-67-36';
$lang['_main_page_map_adr3_title'] = 'Legalspace — Никосия';
$lang['_main_page_adr3_adress'] = 'Псарон, 2408';
$lang['_main_page_adr3_phone'] = '+357 230-300-16';
$lang['_main_page_footer_h1'] = 'Начните решение юридических <br> вопросов уже сейчас';
$lang['_main_page_footer_btn'] = 'Получить бесплатную консультацию';

$lang['footer_text'] = "       <b>Legal Space</b> — is an international community of more than 2 000 first-class lawyers.  <br>
                             Our service allows you to get high quality legal services on the transparent basis.
                            <p>
                                © 2015 Legal Space Ltd., All rights reserved. </p>";
$lang['maria'] = 'Мария Нечепа';
$lang['maria_record'] = 'Директор по инвестиционной деятельности банка';
$lang['sk_title'] = 'Для друзей Сколково';
$lang['sk_text'] = 'Мы дарим Вам 9000 рублей на юридические услуги нашей<br> компании в честь дня рождения школы.';
$lang['sk_activate'] = 'Активировать карту';
$lang['where_need'] = 'В какой стране вам нужен юрист?';
$lang['forget'] = 'Restore password by E-mail';
$lang['restore'] = 'Restore';
$lang['wrong_email'] = 'Wrong E-mail';
$lang['restore_send'] = 'Letter with restore instructions sent to E-mail';
$lang['error'] = 'Error. Try later.';
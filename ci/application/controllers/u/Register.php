<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function check(){
		$email = $this->postAbsent('email');	
		$password = $this->postAbsent('password');	
		if(count(User::get_by_ep($email)))
			$this->err('email-busy');
		$this->ok();
	}
	public function index(){
		$id = User::register($_GET, true);
		if($id == 'already'){
			if(isset($_GET['task'])){
				$this->load->helper('task');
				$u = User::get_by_ep($_GET['email']);
				$id = $u[0]['id'];
				Task::add($_GET['task'], $id);
				$this->redir('/?task_added=1&already='.$_GET['email']);	
			};
			$this->redir('/?already='.$_GET['email']);	
		}
		$u = User::getById($id);
		$magic = $u['magic'];
		if(isset($_GET['task'])){
			$this->load->helper('task');
			Task::add($_GET['task'], $id);
		};
		$this->redir('/u/login?magic='.$magic);
	}
	private function redir($redirect_custom = null){
		if(!$redirect_custom)
			$redirect = '/in/hello';
		else
			$redirect = $redirect_custom;
		if(!$redirect_custom && isset($_GET['redirect']))
			$redirect = $_GET['redirect'];
		header('Location: ' . $redirect);
		exit();
	}
}

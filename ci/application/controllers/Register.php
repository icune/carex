<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function index()
	{
		$this->load->view('register', array(
			
		));
	}
	public function expert(){
		$info = $_POST['info'];
		$info['create_ts'] = date('d.m H:i:s');
		file_put_contents(Info::$rootDir . '/exp-reg.txt', json_encode($info), FILE_APPEND);
		echo '{"status":"ok"}';
	}
	public function link(){
		$info = $_POST['info'];
		$info['create_ts'] = date('d.m H:i:s');
		file_put_contents(Info::$rootDir . '/exp-link.txt', json_encode($info), FILE_APPEND);
		echo '{"status":"ok"}';
	}
}

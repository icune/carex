<?php
class Chunky extends CI_Controller {
	private $error = array();

	public function index() {
		if($_SERVER['REQUEST_METHOD'] == 'PUT')
			$this->processChunk();
		if($_SERVER['REQUEST_METHOD'] == 'POST')
			$this->initDownload();
	}

	private function processChunk(){
		$downDir = $this->downloadDir();
		$fName = urldecode(getallheaders()['File-Name']);
		$data = $this->_readBinary();
		$fullFn = $downDir.$fName;
		$fh = fopen($fullFn, 'a');
		fwrite($fh, $data);
		fclose($fh);
		$cFn = str_replace(Info::$rootDir, '', $fullFn);
		$this->j(array('status' => 'success', 'full_filename' => $fullFn, 'client_filename' => $cFn));
	}
	private function _readBinary(){
		$fName = 'php://input';
		$fh = fopen($fName, 'rb');
		$data = '';
		while(!feof($fh))
			$data .= fread($fh, 1024);
		return $data;
	}
	private function initDownload(){
		$downDir = $this->downloadDir();
		$fn = $_POST['file_name'];
		$approved_fn = $fn;
		$inx = 1;
		while(is_file($downDir.$approved_fn)){
			$parts = explode('.', $fn);
			$approved_fn = implode('.', array_slice($parts, 0, count($parts)-1)) . ($inx++) . '.' . $parts[count($parts)-1];
		}
		$this->j(array('file_name' => $approved_fn));
	}
	private function downloadDir(){
		$r = Info::$rootDir . '/upl/';
		$allH = getallheaders();
		if(isset($_POST['special_dir']) || isset($allH['Special-Dir']))
			$spDir = $r . (isset($_POST['special_dir']) ? $_POST['special_dir'] : (isset($allH['Special-Dir']) ? $allH['Special-Dir'] : ''));
		else 
			return $r;
		if(!is_dir($spDir))		
			mkdir($spDir, 0777, true);
		return $spDir;
	}
	private function encode($st){
		
	    // Сначала заменяем "односимвольные" фонемы.
	    $st=strtr($st,"абвгдеёзийклмнопрстуфхъыэ_",
	    "abvgdeeziyklmnoprstufh'iei");
	    $st=strtr($st,"АБВГДЕЁЗИЙКЛМНОПРСТУФХЪЫЭ_",
	    "ABVGDEEZIYKLMNOPRSTUFH'IEI");
	    // Затем - "многосимвольные".
	    $st=strtr($st, 
	                    array(
	                        "ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", 
	                        "щ"=>"shch","ь"=>"", "ю"=>"yu", "я"=>"ya",
	                        "Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH", 
	                        "Щ"=>"SHCH","Ь"=>"", "Ю"=>"YU", "Я"=>"YA",
	                        "ї"=>"i", "Ї"=>"Yi", "є"=>"ie", "Є"=>"Ye"
	                        )
	             );
	    // Возвращаем результат.
	    return $st;
	}
}
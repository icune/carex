<?php
	
	final class Chat{
		private static $db = null;
		public static function init(){
			$loader = new CI_Loader();
			self::$db = $loader->database('', true);
		}
		public static function send($chat_id, $sender_id, $message){

			$text = $message['text'];
			$chat = self::getChat($chat_id);

			$recv_id = $chat['user_id_1'];
			if($recv_id == $sender_id)
				$recv_id = $chat['user_id_2'];

			self::$db->insert('chat_message', array(
				'chat_id' =>$chat_id,
				'text' =>$text,
				'sender_id' => User::$id,
				'receiver_id' => $recv_id
			));

			$chat_message_id = self::$db->insert_id();

			$files = isset($message['files']) ? $message['files'] : array();

			foreach($files as $f){
				self::$db->insert('chat_message_file', array(
					'chat_message_id' =>$chat_message_id,
					'url' => $f
				));			
			}

			return $chat_message_id;
		}
		private static function getChat($chat_id){
			$r = self::$db->query("SELECT * FROM chat WHERE id=$chat_id")->result_array();
			if(!count($r)) throw new Exception("Where fucking chat_id?");
			return $r[0];
		}

		public static function getInter($chat_id){
			$chat = self::getChat($chat_id);
			$inter_id = ($chat['user_id_1'] != User::$id) ? $chat['user_id_1'] : $chat['user_id_2'];
			return User::getById($inter_id);
		}
		public static function docs($sender = null){
			$uid = User::$id;
			$sql =  "SELECT * FROM chat_message_file cmf JOIN chat_message cm ";
			$sql .= "ON cm.id = cmf.chat_message_id ";
			$sql .= "WHERE receiver_id=$uid OR sender_id=$uid ";
			$sql .= "GROUP BY cmf.url ";
			$sql .= "ORDER BY cmf.create_ts DESC";
			$docs = self::$db->query($sql)->result_array();
			if($sender)
				foreach($docs as $k=>$d){
					$docs[$k]['sender'] = User::getById($d['sender_id']);
					$p = explode('/', $docs[$k]['url']);
					$docs[$k]['only'] = $p[count($p) - 1];
					$only = explode('.', $docs[$k]['only']);
					$ee = array('bmp','csv','docx','ico','jpeg','pdf','ppt','svg','cdr','doc','gif','iso','jpg','png','psd','txt');
					$docs[$k]['ext'] = $only[count($only) - 1];
					if(!in_array($docs[$k]['ext'], $ee))
						$docs[$k]['ext'] = 'txt';
				}
			return $docs;
		}
		public static function unread($chat_id){
			$uid = User::$id;
			$msg = self::$db->query("SELECT * FROM chat_message WHERE chat_id=$chat_id AND receiver_id=$uid")->result_array();
			$count = 0;
			foreach($msg as $m)
				if(!intval($m['read']))
					$count ++;
			return $count;
		}
		public static function chatServ($chat_id){
			return self::$db->query('SELECT * FROM services WHERE chat_id=' . $chat_id)->result_array();
		}
	}

	Chat::init();
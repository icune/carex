<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!User::$id || User::$email != 'a@a.a')
			header('Location: /login');
	}
	function index(){
		$this->load->database();

		$this->load->helper('url');

        /* ------------------ */    
 
        $this->load->library('grocery_CRUD'); 
        $this->grocery_crud->set_table('email');
        
        // $this->grocery_crud->callback_before_insert(array($this, 'cb_insert'));

		$output = $this->grocery_crud->render();
		$data = array(
			'grocery' => (array)$output,
			'title' => 'Редактирование писем'
		);

		$this->load->view('imperium/groc', $data);
	}
}

<?php 

final class User{
	private static $salt = 'dsa876adad';
	private static $db = null;
	public static $cookie = 'knosagor';
	public static $id = 0, $is_expert = false, $user = null, $user_info = null;
	public static $agreed = false;
	public static $amount = 0;
	public static $promo = false;
	private static $loader;
	public static $image_c = '';
	public static $email = '';
	public static $display_name = '';
	public static function init(){
		$loader = new CI_Loader();
		$loader->helper('chat');
		self::$db = $loader->database('', true);
		self::$loader = $loader;

		$u = null;
		if(isset($_COOKIE[self::$cookie])){
			$cookie = self::$db->escape($_COOKIE[self::$cookie]);
			$u = self::$db->query('SELECT * FROM user WHERE cookie=' . $cookie)->result_array();
			if(!count($u))
				return;
		}else
			return;

		self::$id = $u[0]['id'];
		self::$email = $u[0]['email'];
		self::$is_expert = intval($u[0]['is_expert']);
		$ui = self::$db->query('SELECT * FROM user_info WHERE user_id="' . self::$id . '"')->result_array();
		
			
		self::$user = $u[0];
		self::$user_info = $ui[0];
		
		if(is_file(Info::$rootDir . User::$user_info['image'])){
			$p = explode('.', User::$user_info['image']);
			$p[count($p)-2] = $p[count($p)-2] . '-c';
			if(is_file(Info::$rootDir . implode('.', $p)))
				self::$image_c = implode('.', $p);
			else
				self::$image_c = User::$user_info['image'];
		}
	}
	public static function passwordHash($password){
		return crypt($password, self::$salt);
	}
	public static function updateMagic($user_id, $magic_custom = null){
		if($magic_custom === null)
			$magic = Utils::rStr(40);
		else
			$magic = $magic_custom;
		self::$db->update('user', array('magic' => $magic), array('id' => $user_id));
		return $magic;
	}
	public static function updateCookie($user_id){
		$cookie = Utils::rStr(40);
		self::$db->update('user', array('cookie' => $cookie), array('id' => $user_id));
		return $cookie;
	}
	public static function register($params, $return_id = false, $ignore_info = false){
		$is_expert = 0;
		if(isset($params['is_expert']))
			$is_expert = 1;
		$email = null;
		if(isset($params['email']))
			$email = $params['email'];
		else
			return '';
		$u = self::get_by_ep($email);
		if(count($u)){
			return 'already';
		};

		if(isset($params['password']))
			$password = User::passwordHash($params['password']);
		else
			$password = User::passwordHash(Utils::rStr(10));
		$phone = '';
		if(isset($params['phone']))
			$phone = $params['phone'];

		$name = '';
		if(isset($params['name']))
			$name = $params['name'];

		$magic = Utils::rStr(40);
		$cookie = Utils::rStr(40);

		self::$db->insert('user', array(
			'email' => $email,
			'password' => $password,
			'phone' => $phone,
			'magic' => $magic,
			'cookie' => $cookie,
			'restoring' => 1,
			'is_expert' => $is_expert
		));

		$id = self::$db->insert_id();
		if(!$ignore_info)
			self::$db->insert('user_info', array(
				'user_id' => $id,
				'image' => '/img/ava.png',
				'name' => $name
			));
		if($return_id) return $id;
		return $magic;
	}
	public static function get_by_ep($email, $password = null){
		if($password)
			$password = User::passwordHash($password);		
		$r = self::$db->query('SELECT * FROM user WHERE email=' .
				 self::$db->escape($email) . 
				 ($password ? ' AND password=' : ''). 
				 ($password ? self::$db->escape($password)  : '')
			)->result_array();
		return $r;
	}
	public static function changePassword($user_id, $password){
		$password = self::passwordHash($password);
		self::$db->update('user', array('password' => $password, 'restoring' => 0), array('id' => $user_id));
	}
	public static function confirm($user_id){
		self::$db->update('user', array('confirmed' => 1), array('id' => $user_id));	
	}
	public static function getAll(){
		$r = self::$db->query('SELECT * FROM user')->result_array();
		return $r;
	}
	public static function getById($id){
		$r = self::$db->query('SELECT * FROM user WHERE id='.intval($id))->result_array();
		if(!count($r))
			throw new Exception("No user_id: $id");
		return $r[0];
	}
	public static function displayName(){
		$name = self::$user_info['surname'] . ' ' . self::$user_info['name'] . ' ' . self::$user_info['patronym'];
		if(!trim($name))
			return self::$email;
		return $name;
	}
}
User::init();
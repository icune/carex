
DROP TABLE IF EXISTS `case`;
CREATE TABLE `case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `expert_id` int(11) NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`)
  /*CONSTRAINT `case_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `case_car`;
CREATE TABLE `case_car` (
  `case_id` int(11) NOT NULL,
  `brand` char(255) NOT NULL,
  `model` char(255) NOT NULL,
  `year` int(11) NOT NULL,
  `color` char(255) NOT NULL,
  `image` char(255) NOT NULL DEFAULT '',
  `mileage` int(11) NOT NULL,
  `transmission` enum('mechanic','auto') NOT NULL,
  `price` int(11) NOT NULL,
  `comment` TEXT
  -- KEY `case_id` (`case_id`)
  /*CONSTRAINT `case_car_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `case` (`id`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `expert`;
CREATE TABLE `expert` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_abroad` tinyint(1) DEFAULT '0',
  `experience` text,
  `work_conditions` text,
  `checked` tinyint(1) DEFAULT '0',
  `rating` int(11) DEFAULT '0',
  `specializations` char(255) DEFAULT '',
  `directions` char(255) DEFAULT '',
  `departure` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `expert_direction`;
CREATE TABLE `expert_direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `expert_service`;
CREATE TABLE `expert_service` (
  `name` char(255) NOT NULL DEFAULT '',
  `price` char(255) NOT NULL DEFAULT '',
  `user_id` int(11)  NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `expert_specialization`;
CREATE TABLE `expert_specialization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `desc` text NOT NULL,
  `from` double NOT NULL DEFAULT '0',
  `to` double NOT NULL DEFAULT '10000',
  `status` enum('new','go','done') NOT NULL,
  `expert_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `task_car`;
CREATE TABLE `task_car` (
  `task_id` int(11) NOT NULL,
  `brand` char(255) NOT NULL,
  `model` char(255) NOT NULL,
  `year` int(11) NOT NULL,
  KEY `task_id` (`task_id`)
  /*CONSTRAINT `task_car_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `magic` char(255) DEFAULT NULL,
  `cookie` char(255) DEFAULT NULL,
  `email` char(255) DEFAULT NULL,
  `phone` char(255) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `is_expert` tinyint(1) DEFAULT '0',
  `restoring` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `user_id` char(255) NOT NULL,
  `name` char(255) NOT NULL DEFAULT '',
  `surname` char(255) NOT NULL DEFAULT '',
  `patronym` char(255) NOT NULL DEFAULT '',
  `country` char(255) NOT NULL DEFAULT '',
  `city` char(255) NOT NULL DEFAULT '',
  `image` char(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `what` CHAR(255) DEFAULT '',
  `short_decription` CHAR(255) DEFAULT '',
  `text` TEXT,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`)
  /*CONSTRAINT `case_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2015-10-24 01:13:00
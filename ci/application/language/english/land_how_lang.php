<?php
$lang['bystro__udobno__bezo'] = "Fast. Convenient. Secure. Yeah!";
$lang['my_pomozhem_naiti_va'] = "We will help you to find the lawyer abroad";
$lang['s_nami_rabotaet_bole'] = "More than 2 000 lawyers from more than 150 countries";
$lang['uristov_po_vsemu_mir'] = "are working with us";
$lang['vy_mozhete_poluczit_'] = "You can get any legal service anywhere in the world right now on our website. Leave the request and we will provide you with the expert with the highest professional qualifications.";
$lang['prosto_vyberite_stra'] = "Find the lawyer easily!";
//$lang['v_kotoroi_vam_nuzhen'] = "where you need a lawyer and describe your matter";
$lang['v_kotoroi_vam_nuzhen'] = "";
$lang['esli_u_nas_vozniknut'] = "Choose the country where you need a lawyer and describe your legal matter. If we need additional information we will call you";
$lang['neobhodimye_dlja_vyb'] = "";
$lang['my_vam_perezvonim'] = "";
$lang['my_naidem_podhodjasc'] = "We will find the right expert for you";
$lang['opytom_reshenija_ime'] = "";
$lang['kazhdyi_urist_'] = "Every Legal Space lawyer";
$lang['prohodit_tsczatelnyi'] = "passes through a highly competitive selection process";
$lang['poetomu_my_polnostu_'] = "which is why we are sure of his or her qualifications and professionalism and can guarantee the quality of his or her services.";
$lang['obsczaites_s_uristom'] = "Fast. Simple. Free ";
$lang['vy_obsuzhdaete_vozni'] = "You can discuss all the matters, send documents and pay through the Legal Space website";
$lang['obsczenie_prohodit_b'] = "Communicate effectively";
$lang['vy_mozhete_perepisyv'] = "You can message, call or video chat with lawyer right on Legal Space website";
$lang['pozvonit_emu_iz_czat'] = "";
$lang['libo_sdelat_video'] = "";
$lang['zvonok'] = "";
$lang['naiti_urista'] = "Find expert";
$lang['bezopasno_provodite_'] = "Effect payments safely";
$lang['dengi_s_vashego_scze'] = "You can pay directly through Legal Space website and the lawyer will receive payment only after the work is done.";
$lang['my_rabotaem_pri_podd'] = "";
$lang['ja_urist'] = "For lawyers";
$lang['priglashaem_k_nam'] = "";
$lang['eto_bezopasnaja_i_na'] = "это безопасная и надежная виртуальная площадка";
$lang['kotoraja_pomozhet_va'] = "которая поможет Вам решить любую юридическую задачу в кратчайшие сроки.";
$lang['vse_prava_zascziscze'] = "Все права защищены";
$lang['tovarnye_znaki_i_int'] = "Trademarks and intellectual property";
$lang['nedvizhimost'] = "Real estate";
$lang['korporativnoe_pravo_'] = "Corporate M&amp;A";
$lang['czastnyi_kapital'] = "Private equity";
$lang['mezhdunarodnoe_nalog'] = "Международное налоговое и корпоративное структурирование";
$lang['nalogi'] = "Tax";
$lang['investicii'] = "Investments";
$lang['uczrezhdenie_kompani'] = "Company formation";
$lang['investicionnye_fondy'] = "Инвестиционные фонды";
$lang['restrukturizacii_i_b'] = "Investment funds";
$lang['trudovoe_pravo'] = "Labour law";
$lang['antimonopolnoe_zakon'] = "Restructuring and insolvency";
$lang['footer_text'] = "       <b>Legal Space</b> — is an international community of more than 2 000 first-class lawyers.  <br>
                             Our service allows you to get high quality legal services on the transparent basis.";
$lang['cpr']='© 2015 Legal Space Ltd., All rights reserved.';
$lang['foo_1'] = "Trademarks and intellectual property
            <br>Real estate
            <br>Corporate M&amp;A
            <br>Private equity";;
$lang['foo_2'] = "International tax and corporate structuring<br/>Tax<br/>Investments<br/>Company formation<br/>Investment funds";
$lang['foo_3'] = "Investment funds<br/>Restructuring and insolvency<br/>Labour law<br/>Competition and antitrust";
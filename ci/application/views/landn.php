<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"></html><!--<![endif]-->
<head>
  <meta charset="UTF-8"/>
  <script type="text/javascript">
    window.lang = <?php echo json_encode($_); ?>;
    window.curlang = '<?php echo $curlang; ?>';
  </script>
  <meta name="format-detection" content="telephone=no"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" href="/css/bootstrap-grid-3.3.1.min.css"/>
  <link rel="stylesheet" href="/css/style.css"/>
  <link rel="stylesheet" href="/css/wiggy.css"/>
  <link rel="stylesheet" href="/plug/cs/css/countrySelect.css">
  <title>LegalSpace</title>
</head>
<body>
  <header>
    <div class="block-area">
      
    </div>
    <div class="gradient"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="select-lang">
            <select id="lang" value="<?php echo ucfirst($curlang); ?>">
              <option value="russian" <?php if($curlang=='russian') echo 'checked'; ?>>
                Russian
              </option>
              <option value="english" <?php if($curlang=='english') echo 'checked'; ?>>
                English
              </option>
            </select>
          </div><a href="/" class="logo"><img src="img/logo.png" alt="" class="img-responsive"/></a>
          <div class="text-top">
            <h1>
              <?php echo $_['best_juri_online']; ?>
            </h1><span>
              <?php echo $_['bla_bla_before_form']; ?>
            </span>
          </div>
          <?php $this->view('land-formn'); ?>
        </div>
      </div>
    </div>
  </header>
  <section class="labels-sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-xs-12">
          <div class="labels-block">
            <div class="icon"><img src="img/icon-sec-1.png" alt="" class="img-responsive"/></div>
            <p>150 +</p><span>
              <?php echo $_['globe_under']; ?>
            </span>
          </div>
        </div>
        <div class="col-sm-4 col-xs-12">
          <div class="labels-block">
            <div class="icon"><img src="img/icon-sec-2.png" alt="" class="img-responsive"/></div>
            <p>2000 +</p><span>
              <?php echo $_['hat_under']; ?>
            </span>
          </div>
        </div>
        <div class="col-sm-4 col-xs-12"> 
          <div class="labels-block">
            <div class="icon"><img src="img/icon-sec-3.png" alt="" class="img-responsive"/></div>
            <p>100 +</p><span>
              <?php echo $_['case_under']; ?>
            </span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="text-info-sec">
    <div class="container">
      <div class="row">
        <div class="col xs-12">
          <p class="small-lozung">
            <?php echo $_['spoiled_phone']; ?>
          </p>
          <div class="small-logo"><i><img src="img/logo-small.png" alt=""/></i><span class="border"></span></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <div class="text-about">
            <?php echo $_['bla_1']; ?>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="text-about">
            <?php echo $_['bla_2']; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="brands-sec">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="h2"><?php echo $_['with_support']; ?></div>
          <ul class="brand-list">
            <li><a href="#"><img src="img/icon-inst-1.png" alt="" class="img-responsive"/></a></li>
            <li><a href="#"><img src="img/icon-inst-2.png" alt="" class="img-responsive"/></a></li>
            <li><a href="#"><img src="img/icon-inst-3.png" alt="" class="img-responsive"/></a></li>
            <li><a href="#"><img src="img/icon-inst-4.png" alt="" class="img-responsive"/></a></li>
            <li><a href="#"><img src="img/icon-inst-5.png" alt="" class="img-responsive"/></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="list-link-sec">
    <div class="container">
      <div class="row">
        <?php 
          $svc = $_['services'];
          $s1 = array_slice($svc, 0, (int)(count($svc)/4));
          $s2 = array_slice($svc, (int)(count($svc)/4), (int)count($svc)/4);
          $s3 = array_slice($svc, 2*(int)(count($svc)/4), (int)count($svc)/4);
          $s4 = array_slice($svc, 3*(int)(count($svc)/4), (int)count($svc)/4);
         ?>
        <div class="col-sm-3 col-xs-6">
          <div class="list-link">
            <ul>
              <?php foreach($s1 as $k => $v): ?>  
                  <li><a><?php echo $v; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-3 col-xs-6">
          <div class="list-link">
            <ul>
              <?php foreach($s2 as $k => $v): ?>  
                  <li><a><?php echo $v; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-3 col-xs-6">
          <div class="list-link">
            <ul>
              <?php foreach($s3 as $k => $v): ?>  
                  <li><a><?php echo $v; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-3 col-xs-6">
          <div class="list-link">
            <ul>
              <?php foreach($s4 as $k => $v): ?>  
                  <li><a><?php echo $v; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="lozung-sec">
            <div class="h2"><?php echo $_['office_title']; ?></div>
            <p>
              <?php echo $_['office_desc']; ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="map-sec">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 wrap-card-map">
          <div class="card-with-mark milan">
            <div class="card">
              <div class="h4"><strong>Legal</strong>space — <?php echo $_['milan']; ?></div>
              <p>
                <?php echo $_['milan_desc']; ?>
              </p>
              <ul>
                <li><i><img src="img/chair.png" alt=""/></i><span><?php echo $_['milan_addr']; ?></span></li>
                <li><i><img src="img/phone.png" alt=""/></i><span>+39 029-475-96-84</span></li>
              </ul>
            </div>
            <div class="mark"><img src="img/mark.png" alt=""/></div>
          </div>
          <div class="card-with-mark moscow">
            <div class="card">
              <div class="h4"><strong>Legal</strong>space — <?php echo $_['moscow']; ?></div>
              <p>
                <?php echo $_['moscow_desc']; ?>
              </p>
              <ul>
                <li><i><img src="img/chair.png" alt=""/></i><span><?php echo $_['moscow_addr']; ?></span></li>
                <li><i><img src="img/phone.png" alt=""/></i><span>8 (800) 333-67-36</span></li>
              </ul>
            </div>
            <div class="mark"><img src="img/mark.png" alt=""/></div>
          </div>
          <div class="card-with-mark nikossia">
            <div class="card">
              <div class="h4"><strong>Legal</strong>space — <?php echo $_['nikosia']; ?></div>
              <p>
                <?php echo $_['nikosia_desc']; ?>
              </p>
              <ul>
                <li><i><img src="img/chair.png" alt=""/></i><span><?php echo $_['nikosia_addr']; ?></span></li>
                <li><i><img src="img/phone.png" alt=""/></i><span>+ 357 230-300-16</span></li>
              </ul>
            </div>
            <div class="mark"><img src="img/mark.png" alt=""/></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-bot-form">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="title-text">
            <div class="h1"><?php echo $_['find_descision']; ?></div>
            <p>
              <?php echo $_['find_descision_after']; ?>
            </p>
          </div>
          <?php $this->view('land-formn'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="brand-bank-block">
      <div class="container">
        <div class="col-xs-12"><a href="#"><img src="img/icon-bank-1.png" alt="" class="img-responsive"/></a><a href="#"><img src="img/icon-bank-2.png" alt="" class="img-responsive"/></a><a href="#"><img src="img/icon-bank-3.png" alt="" class="img-responsive"/></a></div>
      </div>
    </div>
    <div class="footer-bot">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
            <div class="copyright-notation"><span>© 2015 - Legal Space Ltd., <?php echo $_['rights']; ?></span><br/>
              <p>
                <?php echo $_['footer_blabla']; ?>
              </p>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="soc-info"><a href="#" class="link-jurist">
              <?php echo $_['are_you']; ?>
            </a>
              <ul>
                <li><a class="fb"><img src="img/facebook.png" alt=""/></a></li>
                <li><a class="tw"><img src="img/twitter.png" alt=""/></a></li>
                <li><a class="gp"><img src="img/google+.png" alt=""/></a></li>
                <li><a class="ld"><img src="img/linkedIn.png" alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer><!--[if lt IE 9]>
  <script src="js/es5-shim.min.js"></script>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/html5shiv-printshiv.min.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  <script src="/js/jquery-2.1.4.js"></script>
  <script type="text/javascript" src="/plug/cs/js/countrySelect.js"></script>
  <script src="/js/navigation.js"></script>
  <script src="/js/common.js"></script>
  <script src="/js/wiggy.js"></script>
  <script src="/js/net.js"></script>
  <script type="text/javascript">
    var images = <?php echo json_encode($files); ?>;  
  </script>
  <div class="popup" style="display:none">
    <input type="text" class="cselect-internal" val="<?php echo $_['where_you']; ?>" name="country"/>
    <input type="text" name="name" class="wiggy white" placeholder="<?php echo $_['name'];?>"/>
    <input type="text" name="email" class="wiggy white" placeholder="Email"/>
    <input type="text" name="phone" class="wiggy white" placeholder="<?php echo $_['phone'];?>" value=""/>
    <textarea class="wiggy white" placeholder="<?php echo $_['desc_problem']; ?>" name="message"></textarea>
    <div class="btn" style="width:100%">SS</div>
  </div>
</body>
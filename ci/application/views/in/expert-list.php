<?php $this->view('in/header'); ?>
<style type="text/css">
	select.sel{
		padding:7px;
	}
</style>
<div class="col-sm-9  col-md-10  main">
    <div class="row">
        <p class="pull-left btn-left-sidebar visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Меню</button>
        </p>
    </div>
    <div class="row expert-listing">
        <h3>
                    Поиск экспертов
                </h3>
        <form class="form-inline" action="/in/expert">
            <div class="form-group">
                <select class="sel" name="ui.city">
					<option value="">Город</option>
					<?php foreach($cities as $c): ?>
						<option 
							value="<?php echo $c; ?>"
							<?php if(isset($_GET['ui_city']) && $c == $_GET['ui_city']) echo 'selected'; ?>
						><?php echo $c; ?></option>
					<?php endforeach; ?>
				</select>
            </div>
            <div class="form-group">
                <select class="sel" name="e.specializations">
					<option value="">Специализация</option>
					<?php foreach($specializations as $s): ?>
						<option 
							value="<?php echo $s['id']; ?>"
							<?php if(isset($_GET['e_specializations']) && $s['id'] == $_GET['e_specializations']) echo 'selected'; ?>
						><?php echo $s['name']; ?></option>
					<?php endforeach; ?>
				</select>
            </div>
            <button type="submit" class="btn btn-success">Найти эксперта</button>
        </form>
    </div>



	<div class="row expert-list">
		<?php foreach($experts as $e): ?>

			
			<div class="col-sm-4 ">
            	<article class="auto-blog expert-item">


	                <figure class="video-wrap">
	                    <div class="expert-photo" style="background: url(<?php echo $e['image']; ?>) no-repeat center;background-size: cover;">
	                    </div>

	                </figure>
	                <div class="img-description">
	                    <h3><a href="/in/expert/profile?id=<?php echo $e['user_id']; ?>"><?php echo $e['name'] ?>, <?php echo $e['city'] ?></a> 
	                    <?php if($e['checked']): ?>
	                    	<i class="fa fa-check"></i>
	                	<?php endif; ?>
	                    </h3>
	                    <p style="height:40px;"> 
	                    	<?php 
		                    	// $sv = array();
		                    	// $inx = 0;
		                    	// foreach($e['services'] as $s){
		                    	// 	if($inx > 2) break;
		                    	// 	$sv []=  $s['name']; 
		                    	// 	$inx++;
		                    	// }
		                    	// echo implode(', ', $sv);
	                    		$sp = array();
	                    		$sp_i = explode(',', $e['specializations']);
	                    		foreach($sp_i as $i)
	                    			foreach($specializations as $spec)
	                    				if($spec['id'] == $i)
	                    					$sp []= $spec['name'];
	                    		echo implode(', ', $sp);

	                    	?>
	                        <br> 
	                    </p>

	                    <ul class="exp-rate">
	                        <?php 
		                    	for($i = 0; $i < 5; $i++){
		                    		if(intval($e['rating']) > $i)
		                    			echo '<li><i class="fa fa-star"></i></li>';
		                    		else
		                    			echo '<li><i class="fa fa-star fa-star-o"></i></li>';
		                    	}
		                     ?>
	                    </ul>
	                    <ul class="social-share clearfix">

	                        <li>
	                            <a>Посмотерть отзывы <i class="fa fa-comments"></i> 255 </a>
	                        </li>
	                    </ul>

	                    <a href="/in/expert/profile?id=<?php echo $e['user_id']; ?>" class="btn btn-default "> Связаться  </a>
	                </div>
	            </article>
	        </div>
		<?php endforeach; ?>
	</div>
<?php $this->view('in/footer'); ?>